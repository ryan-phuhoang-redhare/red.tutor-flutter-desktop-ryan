import 'dart:convert';

import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/api/api_client.dart';
import 'package:redtutor_flutter_webapp/models/mediaRecourse.dart';
import 'package:redtutor_flutter_webapp/models/topic/topic.dart';
import 'package:redtutor_flutter_webapp/services/topic_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TopicsController extends GetxController {
  static TopicsController instance = Get.find();
  var addNewStatus = false.obs;
  var topicsObs = <Topic>[].obs;
  var updateTopicStatus = false.obs;
  var title = ''.obs;
  var description = ''.obs;
  var assignedExercises = <String>[].obs;
  final topicService = TopicService(dio: ApiClient.dio);

  changeAddNewStatusTo(bool isAddNew) {
    addNewStatus.value = isAddNew;
  }

  changeUpdateTopicStatusTo(bool isAddNew) {
    updateTopicStatus.value = isAddNew;
  }

  void saveDraft(String id, String title, String description,
      List<String> assignedExercises, String imageBase64Data) {
    Topic topicRequest = Topic(
        id: id,
        subjectId: '',
        title: title,
        description: description,
        masteryReward: 0,
        maxRepeatCount: 0,
        coinReward: 0,
        unlockCost: 0,
        assignedExercises: assignedExercises,
        mediaResource: MediaResource(id: '', mediaType: '', url: ''),
        imageBase64Data: imageBase64Data,
        lastEdited: 0,
        status: "");
    if (updateTopicStatus.value) {
      updateTopic(topicRequest);
    } else {
      createTopic(topicRequest);
    }
  }

  Future<List<Topic>?> fetchData() async {
    final List<Topic>? topics = await topicService.getAllTopics();
    if (topics != null) {
      topicsObs.value = topics;
      return topics;
    } else {
      return null;
    }
  }

  Future<void> createTopic(Topic topic) async {
    try {
      Topic? topicRes = await topicService.createTopic(topic);
      if (topicRes != null) {
        _storeTopicSelectedToSharedPreferences(topicRes);
        topicsObs.add(topicRes);
        changeAddNewStatusTo(false);
      }
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<void> updateTopic(Topic topic) async {
    try {
      Topic? topicRes = await topicService.updateCourse(topic);
      if (topicRes != null) {
        int index = topicsObs.indexWhere((ele) => ele.id == topic.id);
        topicsObs[index] = topic;
        changeAddNewStatusTo(false);
      }
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<void> deleteTopic() async {
    try {
      Topic? topic = await loadTopicSelectedFromSharedPreferences();

      topicService.deleteTopic(topic!.id);
      topicsObs.removeWhere((topicEle) => topicEle.id == topic.id);
    } catch (e) {
      print(e);
    }
  }

  Topic findById(String topicId) {
    return topicsObs.firstWhere((topic) => topic.id == topicId);
  }

  Future<void> selectedTopic(Topic topic) async {
    _storeTopicSelectedToSharedPreferences(topic);
  }

  Future<void> _storeTopicSelectedToSharedPreferences(Topic topic) async {
    final prefs = await SharedPreferences.getInstance();
    final jsonData = topic.toJson(); // Convert User object to a map

    prefs.setString('topicSelected',
        jsonEncode(jsonData)); // Save the User data as a JSON string
  }

  Future<Topic?> loadTopicSelectedFromSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    final topicSelected = prefs.getString('topicSelected');

    if (topicSelected != null) {
      final dataMap = jsonDecode(topicSelected);
      return Topic.fromJson(
          dataMap); // Assuming you've implemented a fromJson method in the User class
    }

    return null; // Return null if no user data is found
  }
}
