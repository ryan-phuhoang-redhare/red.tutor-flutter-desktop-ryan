import 'dart:convert';

import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/api/api_client.dart';
import 'package:redtutor_flutter_webapp/models/book/ebook.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../services/ebooks_service.dart';

class EbooksController extends GetxController {
  static EbooksController instance = Get.find();
  var addNewStatus = false.obs;
  var ebooksObs = <Ebook>[].obs;
  var selectedEbookIndex = 0;
  Ebook? selectedEbook;
  var updateEbookStatus = false.obs;
  final ebooksService = EbooksService(dio: ApiClient.dio);
  var isTableScreen = true;
  List<Ebook> listEbookFiltered = [];

  changeAddNewStatusTo(bool isAddNew) {
    addNewStatus.value = isAddNew;
  }

  changeUpdateEbookStatusTo(bool isAddNew) {
    updateEbookStatus.value = isAddNew;
  }

  Future<List<Ebook>?> fetchData() async {
    print('Prepare to fetch ebooks...');
    final List<Ebook>? ebooks = await ebooksService.getAllEbooks();
    if (ebooks != null) {
      ebooksObs.value = ebooks;
      listEbookFiltered = ebooks;
      return ebooks;
    } else {
      return null;
    }
  }

  // void saveDraft(String id, String title, String description,
  //     String worksheetId, String topicId, String noteContent) {
  // void saveDraft(
  //   String ebookTitle,
  //   String ebookUrl,
  //   List<String> authors,
  //   String description,
  //   int totalPages,
  //   List<String> genres,
  //   List<String> languages,
  //   int price,
  //   Publisher publisher,
  //   String learningLevel,
  //   String ebookStatus,
  //   EbookAdvertisement ebookAdvertisement,
  // ) {
  //   Ebook ebookRequest = Ebook(
  //       ebookTitle: ebookTitle,
  //       ebookUrl: ebookUrl,
  //       authors: authors,
  //       description: description,
  //       totalPages: totalPages,
  //       genres: genres,
  //       languages: languages,
  //       price: price,
  //       publisher: publisher,
  //       learningLevel: learningLevel,
  //       ebookStatus: ebookStatus,
  //       ebookAdvertisement: ebookAdvertisement);
  //   if (updateEbookStatus.value) {
  //     //updateEbook(ebookRequest);
  //   } else {
  //     //createEbook(ebookRequest);
  //   }
  // }

  // Future<Ebook?> fetchById(String id) async {
  //   final Ebook? ebook = await ebooksService.getEbookById(id);
  //   if (ebook != null) {
  //     return ebook;
  //   } else {
  //     return null;
  //   }
  // }

  // Future<void> createEbook(Ebook ebook) async {
  //   try {
  //     Ebook? ebookRes = await ebooksService.createEbook(ebook);
  //     if (ebookRes != null) {
  //       _storeEbookSelectedToSharedPreferences(ebookRes);
  //       ebooksObs.add(ebookRes);
  //       changeAddNewStatusTo(false);
  //     }
  //   } on Exception catch (e) {
  //     print(e);
  //   }
  // }

  // Future<void> updateEbook(Ebook ebook) async {
  //   try {
  //     Ebook? ebookRes = await ebookService.updateEbook(ebook);
  //     if (ebookRes != null) {
  //       int index = ebooksObs.indexWhere((ele) => ele.id == ebook.id);
  //       print("Index: " + index.toString());
  //       ebooksObs[index] = ebook;
  //       ebooksObs.refresh();
  //       changeAddNewStatusTo(false);
  //     }
  //   } on Exception catch (e) {
  //     print(e);
  //   }
  // }

  // Future<void> deleteEbook() async {
  //   try {
  //     Ebook? ebook = await loadEbookSelectedFromSharedPreferences();

  //     ebookService.deleteEbook(ebook!.id);
  //     ebooksObs.removeWhere((ebookEle) => ebookEle.id == ebook.id);
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  // Ebook findById(String ebookId) {
  //   return ebooksObs.firstWhere((ebook) => ebook.id == ebookId);
  // }

  Future<void> selectEbook(Ebook ebook) async {
    print('Storing Ebook: ${ebook.toJson()}');
    await _storeEbookSelectedToSharedPreferences(ebook);
  }

  Future<void> _storeEbookSelectedToSharedPreferences(Ebook ebook) async {
    final prefs = await SharedPreferences.getInstance();
    final jsonData = ebook.toJson(); // Convert object to a map
    print('controller 132 $jsonData');

    prefs.setString(
        'ebookSelected', jsonEncode(jsonData)); // Save as a JSON string
  }

  Future<Ebook?> loadEbookSelectedFromSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    final ebookSelected = prefs.getString('ebookSelected');

    if (ebookSelected != null) {
      final dataMap = jsonDecode(ebookSelected);
      return Ebook.fromJson(
          dataMap); // Assuming you've implemented a fromJson method in the User class
    }

    return null; // Return null if no user data is found
  }
}
