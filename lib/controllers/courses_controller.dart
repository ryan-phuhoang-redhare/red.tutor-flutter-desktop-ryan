import 'dart:convert';

import 'package:get/get.dart';
import 'package:objectid/objectid.dart';
import 'package:redtutor_flutter_webapp/api/api_client.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/models/course/course.dart';
import 'package:redtutor_flutter_webapp/models/course/graphNode.dart';
import 'package:redtutor_flutter_webapp/models/course/graphRow.dart';
import 'package:redtutor_flutter_webapp/models/course/learningLevelGraph.dart';
import 'package:redtutor_flutter_webapp/models/course/prerequisiteItem.dart';
import 'package:redtutor_flutter_webapp/models/request/course/graph_request.dart';
import 'package:redtutor_flutter_webapp/models/topic/topic.dart';
import 'package:redtutor_flutter_webapp/services/courses_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CoursesController extends GetxController {
  static CoursesController instance = Get.find();
  var rowSelectedIndex = 0.obs;
  var nodePositionSelected = ''.obs;
  var topicSelected = ''.obs;
  var isDisplayAddedTopicForm = false.obs;

  var addNewStatus = false.obs;
  var updateCourseStatus = false.obs;
  var data = ''.obs;
  var coursesObs = <Course>[].obs;
  var deletedRowIds = <String>[].obs;
  // These topics already exist in graph
  var topicSelectedList = <String>[].obs;
  var rows = <GraphRow>[].obs;
  var graphId = ''.obs;
  final coursesService = CoursesService(dio: ApiClient.dio);

  // START----------------Graph Handler---------------- //
  void initialRows() {
    isDisplayAddedTopicForm.value = false;
    if (updateCourseStatus.value == false) {
      rows = <GraphRow>[].obs;
      var rowDefault = GraphRow(
          id: _getRandomObjectId(),
          levelNode: 0,
          childId: '',
          parentId: '',
          nodes: [
            GraphNode(
                id: _getRandomObjectId(), position: '0', prerequisites: [])
          ]);
      addRow(rowDefault);
    } else {
      loadGraphFromFB();
    }
  }

  setRows(List<GraphRow> newRows) {
    rows.value = newRows;
  }

  saveRow(GraphRow row) {
    int foundIndex = rows.indexWhere((element) => element.id == row.id);
    rows[foundIndex] = row;
  }

  addRow(GraphRow newRow) {
    newRow.id = _getRandomObjectId();
    rows.add(newRow);
    reDrawGraph();
  }

  void addRowBelow(int currentRowIndex) {
    GraphRow previousRow = rows[currentRowIndex];
    GraphRow newRow = GraphRow(
        id: _getRandomObjectId(),
        levelNode: currentRowIndex + 1,
        childId: '',
        parentId: previousRow.id,
        nodes: [
          GraphNode(id: _getRandomObjectId(), position: '0', prerequisites: [])
        ]);
    addRow(newRow);
    reDrawGraph();
  }

  void removeRow(int rowIndex) {
    GraphRow rowRemoved = rows[rowIndex];
    List<GraphNode> nodes = rowRemoved.nodes;
    List<String> topicIds = [];
    nodes.forEach((node) {
      if (node.topicId != null && node.topicId != "") {
        topicIds.add(node.topicId!);
      }
    });
    String rowRemovedId = rowRemoved.id;
    rows.remove(rowRemoved);
    deletedRowIds.add(rowRemovedId);
    topicIds.forEach((topicId) {
      topicSelectedList.remove(topicId);
    });
    reDrawGraph();
  }

  void insertRow(int indexToInsertBelow) {
    List<GraphRow> newRows = [];
    int index = 0;
    rows.forEach((row) {
      newRows.add(row);
      if (index == indexToInsertBelow) {
        GraphRow newRow = GraphRow(
            id: _getRandomObjectId(),
            levelNode: index + 1,
            childId: '',
            parentId: row.id,
            nodes: [
              GraphNode(
                  id: _getRandomObjectId(), position: '0', prerequisites: [])
            ]);
        newRow.id = _getRandomObjectId();
        newRows.add(newRow);
      }

      index++;
    });
    setRows(newRows);
    reDrawGraph();
  }

  void addNode(GraphRow row) {
    int currentNodeIndex = int.parse(row.nodes[row.nodes.length - 1].position);
    if (isRowExisted(row)) {
      GraphNode newNode = GraphNode(
          id: _getRandomObjectId(),
          position: (currentNodeIndex + 1).toString(),
          prerequisites: []);

      row.nodes.add(newNode);
      saveRow(row);
    }
    reDrawGraph();
  }

  void removeNode(int rowIndex, String nodePosition) {
    GraphRow row = rows[rowIndex];
    List<GraphNode> nodes = row.nodes;
    GraphNode? nodeFound =
        nodes.firstWhereOrNull((node) => node.position == nodePosition);
    if (nodeFound != null) {
      removePrerequisiteFrom(rowIndex, nodeFound);
    }
    nodes.remove(nodeFound);

    if (rowIndex == rowSelectedIndex.value) {
      setDisplayAddedTopicForm(false);
    }

    row.nodes = nodes;
    rows[rowIndex] = row;

    reDrawGraph();
  }

  reDrawGraph() {
    var indicateIndex = 1;
    rows.forEach((row) {
      row.nodes.forEach((node) {
        node.position = indicateIndex.toString();
        indicateIndex++;
      });
    });
  }

  void removePrerequisiteFrom(int rowIndex, GraphNode node) {
    String nodeId = node.id;
    int nextRow = rowIndex + 1;
    if (nextRow <= rows.length - 1) {
      GraphRow nextRowObj = rows[nextRow];
      List<GraphNode> nodes = nextRowObj.nodes;
      nodes.forEach((node) {
        if (node.prerequisites.contains(nodeId)) {
          node.prerequisites.remove(nodeId);
        }
      });
      nextRowObj.nodes = nodes;
      rows[nextRow] = nextRowObj;
    }
  }

  void loadGraphFromFB() async {
    LearningLevelGraph graph = await getGraphFromDb();
    graphId.value = graph.id!;
    rows.value = graph.rows;
    topicSelectedList.value = _getTopicSelectedList();
  }

  List<String> _getTopicSelectedList() {
    List<String> result = [];
    rows.forEach((row) {
      row.nodes.forEach((node) {
        if (node.topicId != null) {
          result.add(node.topicId!);
        }
      });
    });
    return result;
  }

  setRowSelectedIndex(int newValue) {
    rowSelectedIndex.value = newValue;
  }

  setNodePositionSelected(String position) {
    nodePositionSelected.value = position;
  }

  setTopicSelected(String newValue) {
    topicSelected.value = newValue;
  }

  setDisplayAddedTopicForm(bool newValue) {
    isDisplayAddedTopicForm.value = newValue;
  }

  Future<void> setPrerequisiteForNode(
      List<PrerequisiteItem> prerequisites, String topicId) async {
    GraphRow row = rows[rowSelectedIndex.value];
    int indexNode = row.nodes
        .indexWhere((node) => node.position == nodePositionSelected.value);
    GraphNode? node = row.nodes[indexNode];
    List<String> nodePositions =
        prerequisites.map((pre) => pre.nodePosition).toList();
    List<String> nodePrerequisitesIds = _convertNodePositionToId(nodePositions);
    if (topicId != "") {
      Topic topicSelected = topicsController.findById(topicId);
      node.topicId = topicId;
      node.topicTitle = topicSelected.title;
    } else {
      node.topicId = "";
      node.topicTitle = "";
    }
    node.prerequisites = nodePrerequisitesIds;
    row.nodes[indexNode] = node;
    rows[rowSelectedIndex.value] = row;
  }

  List<String> _convertNodePositionToId(List<String> positions) {
    List<String> nodeIds = [];
    if (rowSelectedIndex.value - 1 >= 0) {
      GraphRow row = rows[rowSelectedIndex.value - 1];
      List<GraphNode> nodes = row.nodes;
      nodes = nodes.where((node) => positions.contains(node.position)).toList();
      nodeIds = nodes.map((e) => e.id).toList();
      return nodeIds;
    } else {
      return [];
    }
  }

  List<GraphNode> getNodesFromRowIndex(int rowIndex) {
    List<GraphNode> nodes = [];
    if (rowSelectedIndex.value - 1 >= 0) {
      GraphRow row = rows[rowSelectedIndex.value - 1];
      nodes = row.nodes;
      return nodes;
    } else {
      return [];
    }
  }

  bool isRowExisted(GraphRow row) {
    GraphRow? foundRow =
        rows.firstWhereOrNull((element) => element.id == row.id);
    return foundRow != null;
  }

  Future<void> createGraph(
      String name, String subjectId, String creator) async {
    List<GraphRowRequest> graphRowRequests = _getRowRequests();
    AddNewGraphRequest addNewGraphRequest = AddNewGraphRequest(
        name: name,
        subjectId: subjectId,
        creator: creator,
        graphRowRequests: graphRowRequests);
    try {
      coursesService.createGraph(addNewGraphRequest);
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<void> updateGraph() async {
    List<GraphRowRequest> graphRowRequests = _getRowRequests();
    UpdateGraphRequest updateGraphRequest = UpdateGraphRequest(
        id: graphId.value,
        graphRowRequests: graphRowRequests,
        deletedRows: deletedRowIds);

    try {
      // LearningLevelGraph? response =
      await coursesService.updateGraph(updateGraphRequest);
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<dynamic> getGraphFromDb() async {
    dynamic learningLevelGraph;
    Course? course = await loadCourseSelectedFromSharedPreferences();
    try {
      learningLevelGraph = coursesService.getGraphFromDb(course!.id);
      return learningLevelGraph;
    } on Exception catch (e) {
      print(e);
    }
  }

  List<GraphRowRequest> _getRowRequests() {
    List<GraphRowRequest> result = [];
    // Convert GraphRow => GraphRowRequest
    rows.forEach((row) {
      // Convert GraphNode => GraphNodeRequest
      List<GraphNodeRequest> nodeRequests = [];
      row.nodes.forEach((node) {
        GraphNodeRequest graphNodeRequest = GraphNodeRequest(
            nodeId: node.id,
            position: node.position.toString(),
            topicId: node.topicId ?? '',
            prerequisites: node.prerequisites);
        nodeRequests.add(graphNodeRequest);
      });
      GraphRowRequest rowRequest = GraphRowRequest(
          id: row.id,
          levelNode: row.levelNode,
          childId: "",
          parentId: "",
          creator: "",
          nodeRequests: nodeRequests,
          nodePrerequisites: {});
      result.add(rowRequest);
    });
    int index = 0;
    if (result.length > 1) {
      result.forEach((element) {
        if (index == 0) {
          element.childId = result[index + 1].id;
          element.parentId = "";
        } else if (index == result.length - 1) {
          element.childId = "";
          element.parentId = result[result.length - 2].id;
        } else {
          element.childId = result[index + 1].id;
          element.parentId = result[index - 1].id;
        }
        result[index] = element;
        index++;
      });
    }

    return result;
  }

  // END----------------Graph Handler---------------- //

  // START----------------Course Handler---------------- //

  Future<void> selectedCourse(Course course) async {
    _storeCourseSelectedToSharedPreferences(course);
  }

  Future<void> createCourse(Course courseRequest) async {
    try {
      Course? courseRes = await coursesService.createCourse(courseRequest);
      if (courseRes != null) {
        _storeCourseSelectedToSharedPreferences(courseRes);
        coursesObs.add(courseRes);
        await createGraph(
            "Graph for ${courseRes.name}", courseRes.id, courseRes.creatorId);
      }
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<void> updateCourse(Course course) async {
    try {
      Course? courseRes = await coursesService.updateCourse(course);
      if (courseRes != null) {
        await updateGraph();
        int index = coursesObs.indexWhere((ele) => ele.id == course.id);
        coursesObs[index] = course;
        changeAddNewStatusTo(false);
      }
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<void> deleteCourse() async {
    try {
      Course? course = await loadCourseSelectedFromSharedPreferences();
      coursesService.deleteCourse(course!.id);
      coursesObs.removeWhere((courseEle) => courseEle.id == course.id);
    } catch (e) {
      print(e);
    }
  }

  Future<void> _storeCourseSelectedToSharedPreferences(Course course) async {
    final prefs = await SharedPreferences.getInstance();
    final userData = course.toJson(); // Convert User object to a map

    prefs.setString('courseSelected',
        jsonEncode(userData)); // Save the User data as a JSON string
  }

  Future<Course?> loadCourseSelectedFromSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    final courseSelected = prefs.getString('courseSelected');

    if (courseSelected != null) {
      final userMap = jsonDecode(courseSelected);
      return Course.fromJson(
          userMap); // Assuming you've implemented a fromJson method in the User class
    }

    return null; // Return null if no user data is found
  }

  Future<List<Course>?> fetchData() async {
    final List<Course>? courses = await coursesService.getAllCourses();
    if (courses != null) {
      coursesObs.value = courses;
      return courses;
    } else {
      return null;
    }
  }

  changeUpdateCourseStatusTo(bool isAddNew) {
    updateCourseStatus.value = isAddNew;
  }

  String _getRandomObjectId() {
    ObjectId objectId = ObjectId();
    String newObjectId = objectId.toString();
    return newObjectId;
  }

  // END----------------Course Handler---------------- //
  changeAddNewStatusTo(bool isAddNew) {
    addNewStatus.value = isAddNew;
  }
}
