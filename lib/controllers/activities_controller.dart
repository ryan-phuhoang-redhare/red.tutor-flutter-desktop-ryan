// ignore_for_file: invalid_use_of_protected_member

import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/api/api_client.dart';
import 'package:redtutor_flutter_webapp/models/request/activity/bot_request.dart';
import 'package:redtutor_flutter_webapp/models/topic/topic.dart';
import 'package:redtutor_flutter_webapp/services/activities_service.dart';

class ActivitiesController extends GetxController {
  static ActivitiesController instance = Get.find();
  var addNewStatus = false.obs;
  var activitiesObs = <Topic>[].obs;
  var updateStatus = false.obs;
  var botDocument = "".obs;
  var botJson = "".obs;
  final activityService = ActivityService(dio: ApiClient.dio);

  changeAddNewStatusTo(bool isAddNew) {
    addNewStatus.value = isAddNew;
  }

  changeUpdateStatusTo(bool isAddNew) {
    updateStatus.value = isAddNew;
  }

  Future<void> createBot(BotRequest botRequest) async {
    await activityService.createBot(botRequest);
  }
}
