// ignore_for_file: invalid_use_of_protected_member

import 'package:get/get.dart';

class ChatServerController extends GetxController {
  static ChatServerController instance = Get.find();
  var addNewStatus = false.obs;
  var updateStatus = false.obs;

  changeAddNewStatusTo(bool isAddNew) {
    addNewStatus.value = isAddNew;
  }

  changeUpdateStatusTo(bool isAddNew) {
    updateStatus.value = isAddNew;
  }
}
