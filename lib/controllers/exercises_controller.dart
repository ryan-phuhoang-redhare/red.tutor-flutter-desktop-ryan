import 'dart:convert';

import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/api/api_client.dart';
import 'package:redtutor_flutter_webapp/models/exercise/exercise.dart';
import 'package:redtutor_flutter_webapp/services/exerciseService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ExercisesController extends GetxController {
  static ExercisesController instance = Get.find();
  var addNewStatus = false.obs;
  var exercisesObs = <Exercise>[].obs;
  var updateExerciseStatus = false.obs;
  final exerciseService = ExerciseService(dio: ApiClient.dio);

  changeAddNewStatusTo(bool isAddNew) {
    addNewStatus.value = isAddNew;
  }

  changeUpdateExerciseStatusTo(bool isAddNew) {
    updateExerciseStatus.value = isAddNew;
  }

  // void saveDraft(String id, String title, String description,
  //     String worksheetId, String topicId, String noteContent) {
  void saveDraft(
      String id,
      String title,
      String worksheetId,
      String topicId,
      String noteContent,
      int questionLimit,
      int minimumScore,
      int timerDuration) {
    Exercise exerciseRequest = Exercise(
        id: id,
        title: title,
        //description: description,
        worksheetId: worksheetId,
        topicId: topicId,
        noteContent: noteContent,
        questionLimit: questionLimit,
        minimumScore: minimumScore,
        timerDuration: timerDuration);
    if (updateExerciseStatus.value) {
      updateExercise(exerciseRequest);
    } else {
      createExercise(exerciseRequest);
    }
  }

  Future<List<Exercise>?> fetchData() async {
    final List<Exercise>? exercises = await exerciseService.getAllExercises();
    if (exercises != null) {
      exercisesObs.value = exercises;
      return exercises;
    } else {
      return null;
    }
  }

  Future<Exercise?> fetchById(String id) async {
    final Exercise? exercise = await exerciseService.getExerciseById(id);
    if (exercise != null) {
      return exercise;
    } else {
      return null;
    }
  }

  Future<void> createExercise(Exercise exercise) async {
    try {
      Exercise? exerciseRes = await exerciseService.createExercise(exercise);
      if (exerciseRes != null) {
        _storeExerciseSelectedToSharedPreferences(exerciseRes);
        exercisesObs.add(exerciseRes);
        changeAddNewStatusTo(false);
      }
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<void> updateExercise(Exercise exercise) async {
    try {
      Exercise? exerciseRes = await exerciseService.updateExercise(exercise);
      if (exerciseRes != null) {
        int index = exercisesObs.indexWhere((ele) => ele.id == exercise.id);
        print("Index: " + index.toString());
        exercisesObs[index] = exercise;
        exercisesObs.refresh();
        changeAddNewStatusTo(false);
      }
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<void> deleteExercise() async {
    try {
      Exercise? exercise = await loadExerciseSelectedFromSharedPreferences();

      exerciseService.deleteExercise(exercise!.id);
      exercisesObs.removeWhere((exerciseEle) => exerciseEle.id == exercise.id);
    } catch (e) {
      print(e);
    }
  }

  Exercise findById(String exerciseId) {
    return exercisesObs.firstWhere((exercise) => exercise.id == exerciseId);
  }

  Future<void> selectedExercise(Exercise exercise) async {
    _storeExerciseSelectedToSharedPreferences(exercise);
  }

  Future<void> _storeExerciseSelectedToSharedPreferences(
      Exercise exercise) async {
    final prefs = await SharedPreferences.getInstance();
    final jsonData = exercise.toJson(); // Convert object to a map

    prefs.setString(
        'exerciseSelected', jsonEncode(jsonData)); // Save as a JSON string
  }

  Future<Exercise?> loadExerciseSelectedFromSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    final exerciseSelected = prefs.getString('exerciseSelected');

    if (exerciseSelected != null) {
      final dataMap = jsonDecode(exerciseSelected);
      return Exercise.fromJson(
          dataMap); // Assuming you've implemented a fromJson method in the User class
    }

    return null; // Return null if no user data is found
  }
}
