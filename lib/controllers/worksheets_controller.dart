import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:get/get.dart';
import 'package:objectid/objectid.dart';
import 'package:quill_html_converter/quill_html_converter.dart';
import 'package:redtutor_flutter_webapp/api/api_client.dart';
import 'package:redtutor_flutter_webapp/models/request/worksheet/quiz_request.dart';
import 'package:redtutor_flutter_webapp/models/request/worksheet/worksheet_request.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/answer_option.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/quiz.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/worksheet.dart';
import 'package:redtutor_flutter_webapp/services/worksheet_service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:html/parser.dart';
import 'package:vsc_quill_delta_to_html/vsc_quill_delta_to_html.dart'
    as conventer show ConverterOptions, QuillDeltaToHtmlConverter;
import 'package:html2md/html2md.dart' as html2md;
import 'package:delta_markdown_converter/delta_markdown_converter.dart'
    as delta_markdown show markdownToDelta;

class WorksheetsController extends GetxController {
  static WorksheetsController instance = Get.find();
  var addNewStatus = false.obs;
  var updateStatus = false.obs;
  var worksheetsObs = <Worksheet>[].obs;
  var quizPool = <Quiz>[].obs;
  final worksheetService = WorksheetService(dio: ApiClient.dio);

  changeAddNewStatusTo(bool isAddNew) {
    addNewStatus.value = isAddNew;
  }

  changeUpdateStatusTo(bool isAddNew) {
    updateStatus.value = isAddNew;
  }

  initialQuizPool() {
    // initial quizzes
    quizPool.value = [];
    List<AnswerOption> options = [
      AnswerOption(
          quizIndex: -1, index: 0, isRightAnswer: false, answerDescription: ''),
    ];
    Quiz quizDefault = Quiz(
        index: 0,
        questionDescription: '',
        options: options,
        rightAnswer: 0,
        hint: '',
        explanation: '',
        isShuffleAnswer: false,
        type: "MULTIPLE_CHOICE");
    quizDefault.quillEditorController.addListener(() {
      _saveQuestionHtml(quizDefault);
    });
    quizPool.add(quizDefault);
  }

  void addNewQuestion() {
    Quiz lastQuiz = quizPool[quizPool.length - 1];
    List<AnswerOption> newOptions = [
      AnswerOption(
          quizIndex: -1, index: 0, isRightAnswer: false, answerDescription: ''),
    ];
    Quiz newQuiz = Quiz(
        index: lastQuiz.index + 1,
        questionDescription: '',
        options: newOptions,
        rightAnswer: 0,
        hint: '',
        explanation: '',
        isShuffleAnswer: false,
        type: "MULTIPLE_CHOICE");
    newQuiz.quillEditorController.addListener(() {
      _saveQuestionHtml(newQuiz);
    });
    quizPool.add(newQuiz);
  }

  void _saveQuestionHtml(Quiz quiz) {
    final delta = quiz.quillEditorController.document.toDelta();
    final quizDescriptionHtmlText = _toHtml(List.castFrom(delta.toJson()));
    quiz.questionDescription = quizDescriptionHtmlText;
  }

  String _toHtml(List<Map<String, dynamic>> deltaJson) {
    final converterInstance = conventer.QuillDeltaToHtmlConverter(deltaJson);
    return _processHtmlWithAttributes(converterInstance.convert());
  }

  String _processHtmlWithAttributes(String htmlText) {
    // Custom logic to add style attributes to images in the HTML
    final String modifiedHtml = htmlText.replaceAllMapped(
      RegExp(r'<img[^>]*src="?([^"\s]+)"?[^>]*>'),
      (Match match) {
        final imageUrl = match.group(1);
        final styleAttribute =
            'width: 100px; height: 100px'; // Customize this based on your requirements
        return '<img src="$imageUrl" style="$styleAttribute" alt=""/>';
      },
    );

    return modifiedHtml;
  }

  Delta _fromHtml(String html) {
    final markdown = html2md
        .convert(
          html,
        )
        .replaceAll('unsafe:', '');
    final deltaJsonString = delta_markdown.markdownToDelta(markdown);
    final deltaJson = jsonDecode(deltaJsonString);
    if (deltaJson is! List) {
      throw ArgumentError(
        'The delta json string should be of type list when jsonDecode() it',
      );
    }
    return Delta.fromJson(
      deltaJson,
    );
  }

  void _toDelta(Quiz quiz) {
    Delta delta = _fromHtml(quiz.questionDescription);
    Delta modifiedDelta = _modifyImageAttributes(delta);
    quiz.quillEditorController.document = Document.fromDelta(modifiedDelta);
    quiz.quillEditorController.addListener(() {
      _saveQuestionHtml(quiz);
    });
  }

  Delta _modifyImageAttributes(Delta originalDelta) {
    return Delta.fromOperations(
      originalDelta.toList().map((op) {
        final data = op.data;
        if (data is Map && data.containsKey("image")) {
          // Modify attributes of the image operation
          final newAttributes = Map<String, dynamic>.from(op.attributes ?? {});
          newAttributes["style"] = "width: 100; height: 100"; // Update style

          // Create a new operation with modified attributes
          return Operation.insert(data, newAttributes);
        } else {
          return op;
        }
      }).toList(),
    );
  }

//_updateCharacterCount
  Text getValidateCharacterCount(String text, int maxLength) {
    final currentLength = text.length;
    final remaining = maxLength - currentLength;
    return Text(
      'Remaining characters: $remaining/$maxLength',
      style: TextStyle(color: remaining < 0 ? Colors.red : Colors.grey),
    );
  }

  void addAnswerOption(int currentIndex) {
    Quiz selectedQuiz = quizPool[currentIndex];
    int numberOfOption = selectedQuiz.options.length;
    // Just access 4 options
    if (numberOfOption < 4) {
      var newOptions = selectedQuiz.options;
      AnswerOption newOption = AnswerOption(
          quizIndex: quizPool[currentIndex].index,
          index: numberOfOption,
          isRightAnswer: false,
          answerDescription: '');
      AnswerOption? isExistedObj = newOptions
          .firstWhereOrNull((element) => element.index == numberOfOption);
      if (isExistedObj == null) {
        newOptions.add(newOption);
        selectedQuiz.options = newOptions;
        quizPool[currentIndex] = selectedQuiz;
      }
      _resetAnswerOptionIndex(currentIndex);
    }
  }

  void removeAnswerOption(int currentIndex) {
    Quiz selectedQuiz = quizPool[currentIndex];
    if (selectedQuiz.options.length > 1) {
      var optionUpdated = selectedQuiz.options;
      optionUpdated.removeAt(currentIndex);
      quizPool[currentIndex] = selectedQuiz;
      _resetAnswerOptionIndex(currentIndex);
    }
  }

  void reOrderOption(int oldIndex, int newIndex, int quizIndex) {
    Quiz selectedQuiz = quizPool[quizIndex];
    if (oldIndex < newIndex) {
      newIndex -= 1;
    }
    var optionsUpdated = selectedQuiz.options;

    final item = optionsUpdated.removeAt(oldIndex);
    optionsUpdated.insert(newIndex, item);
    quizPool[quizIndex] = selectedQuiz;
    _resetAnswerOptionIndex(quizIndex);
  }

  void reOrderQuizOption(int oldIndex, int newIndex) {
    if (oldIndex < newIndex) {
      newIndex -= 1;
    }
    var quizzesUpdated = quizPool;

    final item = quizzesUpdated.removeAt(oldIndex);
    quizzesUpdated.insert(newIndex, item);
    quizPool = quizzesUpdated;
    _resetQuizOptionIndex();
  }

  handleCheckedAnswerOption(int index, int optionIndex, bool isChecked) {
    var optionsUpdated = quizPool[index].options;
    AnswerOption optionSelected = optionsUpdated[optionIndex];
    optionSelected.isRightAnswer = isChecked;
    optionsUpdated[optionIndex] = optionSelected;
    if (quizPool[index].type == "MULTIPLE_CHOICE") {
      optionsUpdated.forEach((element) {
        if (element.index != optionSelected.index) {
          element.isRightAnswer = false;
        }
      });
    }
    quizPool[index].options = optionsUpdated;
  }

  clearAnswer(int quizIndex) {
    var quizSelected = quizPool[quizIndex];
    List<dynamic> optionsUpdated = [];
    if (quizSelected.type == "MULTIPLE_CHOICE") {
      int index = 0;
      quizPool[quizIndex].options.forEach((element) {
        if (index < 4) {
          optionsUpdated.add(element);
        }
        index++;
      });
    } else if (quizSelected.type == "CHECKBOXES") {
      optionsUpdated = quizPool[quizIndex].options;
    }
    optionsUpdated.forEach((element) {
      element.isRightAnswer = false;
    });
    quizPool[quizIndex].options = optionsUpdated;
  }

  _resetQuizOptionIndex() {
    int index = 0;
    quizPool.forEach((quiz) {
      quiz.index = index;
      index++;
    });
  }

  _resetAnswerOptionIndex(int quizIndex) {
    int index = 0;
    quizPool[quizIndex].options.forEach((element) {
      element.index = index;
      index++;
    });
  }

  // Convert html to plain text
  String htmlToPlainText(String htmlText) {
    final document = parse(htmlText);
    String plainText = parse(document.body!.text).documentElement!.text;
    return plainText;
  }

  String _getRandomObjectId() {
    ObjectId objectId = ObjectId();
    String newObjectId = objectId.toString();
    return newObjectId;
  }

  void saveDraft(WorksheetRequest worksheetRequest) {
    if (updateStatus.value) {
      updateWorksheet(worksheetRequest);
    } else {
      createWorksheet(worksheetRequest);
    }
  }

  Future<bool> _isQuizEdited(String? quizId) async {
    Quiz retrieveQuizFromDb = await _loadQuizFromDB(quizId!);
    Quiz retrieveQuizFromEditor =
        quizPool.firstWhere((element) => element.id == quizId);

    bool checkEquality<T>(T prop1, T prop2) {
      return prop1 != prop2;
    }

    bool checkListAnswerOptionEquality<T>(
        List<dynamic> list1, List<dynamic> list2) {
      if (list1.length != list2.length) {
        return true;
      }

      for (int i = 0; i < list1.length; i++) {
        // Is answer description change?
        if (checkEquality(
            list1[i].answerDescription, list2[i].answerDescription)) {
          return true;
        }
        // Is answer sequence change?
        if (checkEquality(list1[i].index, list2[i].index)) {
          return true;
        }
        // Is right answer change?
        if (checkEquality(list1[i].isRightAnswer, list2[i].isRightAnswer)) {
          return true;
        }
      }

      return false;
    }

    // Is question type change?
    if (checkEquality(retrieveQuizFromDb.type, retrieveQuizFromEditor.type) ||
        // Is question description change?
        checkEquality(retrieveQuizFromDb.questionDescription,
            retrieveQuizFromEditor.questionDescription) ||
        // Is question sequence change?
        checkEquality(retrieveQuizFromDb.index, retrieveQuizFromEditor.index) ||
        // Is shuffle answer change?
        checkEquality(retrieveQuizFromDb.isShuffleAnswer,
            retrieveQuizFromEditor.isShuffleAnswer) ||
        // Is question answers change?
        checkListAnswerOptionEquality(
            retrieveQuizFromDb.options, retrieveQuizFromEditor.options) ||
        // Is hint change?
        checkEquality(retrieveQuizFromDb.hint, retrieveQuizFromEditor.hint) ||
        // Is explanation change?
        checkEquality(retrieveQuizFromDb.explanation,
            retrieveQuizFromEditor.explanation)) {
      return true;
    }

    return false;
  }

  Quiz _convertToQuiz(QuizRequest quizRes) {
    Quiz quiz = Quiz(
        id: quizRes.id,
        index: quizRes.index,
        questionDescription: quizRes.question,
        options: quizRes.options,
        rightAnswer: quizRes.rightAnswer,
        hint: quizRes.hint,
        explanation: quizRes.explanation,
        type: quizRes.type,
        isShuffleAnswer: quizRes.isShuffleAnswer);
    return quiz;
  }

  Future<void> selectedWorksheet(Worksheet worksheet) async {
    _storeWorksheetSelectedToSharedPreferences(worksheet);
  }

  Future<void> _storeWorksheetSelectedToSharedPreferences(
      Worksheet worksheet) async {
    final prefs = await SharedPreferences.getInstance();
    final jsonData = worksheet.toJson(); // Convert User object to a map

    prefs.setString('worksheetSelected',
        jsonEncode(jsonData)); // Save the User data as a JSON string
  }

  Future<Worksheet?> loadWorksheetSelectedFromSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    final worksheetSelected = prefs.getString('worksheetSelected');

    if (worksheetSelected != null) {
      final dataMap = jsonDecode(worksheetSelected);
      return Worksheet.fromJson(
          dataMap); // Assuming you've implemented a fromJson method in the User class
    }

    return null; // Return null if no user data is found
  }

  Future<List<Worksheet>?> fetchData() async {
    final List<Worksheet>? worksheets =
        await worksheetService.getAllWorksheets();
    if (worksheets != null) {
      worksheetsObs.value = worksheets;
      return worksheets;
    } else {
      return null;
    }
  }

  Future<void> createWorksheet(WorksheetRequest worksheetRequest) async {
    try {
      Worksheet? worksheetRes =
          await worksheetService.createWorksheet(worksheetRequest);
      if (worksheetRes != null) {
        _storeWorksheetSelectedToSharedPreferences(worksheetRes);
        worksheetsObs.add(worksheetRes);
        changeAddNewStatusTo(false);
      }
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<void> updateWorksheet(WorksheetRequest worksheetRequest) async {
    List<QuizRequest> quizRequests = worksheetRequest.addNewQuizRequests;
    List<QuizRequest> quizRequestUpdated = [];

    Future<void> processQuizzes() async {
      for (var quizRes in quizRequests) {
        Quiz quizEditor = _convertToQuiz(quizRes);
        if (quizEditor.id != null) {
          bool isQuizEdited = await _isQuizEdited(quizEditor.id);
          if (isQuizEdited == true) {
            quizRes.quizNewId = _getRandomObjectId();
          }
        }
        quizRequestUpdated.add(quizRes);
      }
    }

    await processQuizzes();
    worksheetRequest.addNewQuizRequests = quizRequestUpdated;
    try {
      Worksheet? worksheetRes =
          await worksheetService.updateWorksheet(worksheetRequest);
      if (worksheetRes != null) {
        int index =
            worksheetsObs.indexWhere((ele) => ele.id == worksheetRes.id);
        worksheetsObs[index] = worksheetRes;
        changeAddNewStatusTo(false);
      }
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<void> deleteWorksheet() async {
    try {
      Worksheet? worksheet = await loadWorksheetSelectedFromSharedPreferences();

      worksheetService.deleteWorksheet(worksheet!.id!);
      worksheetsObs
          .removeWhere((worksheetcEle) => worksheetcEle.id == worksheet.id);
    } catch (e) {
      print(e);
    }
  }

  Future<void> loadQuizzesByWorksheetId(String? worksheetId) async {
    final List<Quiz>? quizzes =
        await worksheetService.getQuizzesByWorksheetId(worksheetId!);
    for (var quiz in quizzes!) {
      if (quiz.explanation != "") {
        quiz.explanationController.text = quiz.explanation;
        quiz.characterExplanationText =
            getValidateCharacterCount(quiz.explanation, 200);
      }
      if (quiz.hint != "") {
        quiz.hintController.text = quiz.hint;
        quiz.characterHintText = getValidateCharacterCount(quiz.hint, 200);
      }
      // Convert html to delta and load on Fluter Quill
      _toDelta(quiz);
    }
    quizPool.value = quizzes;
  }

  Future<Quiz> _loadQuizFromDB(String quizId) async {
    final Quiz quiz = await worksheetService.getQuizById(quizId);
    return quiz;
  }
}
