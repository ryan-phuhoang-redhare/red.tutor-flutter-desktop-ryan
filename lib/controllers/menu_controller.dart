import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/routing/routes.dart';

class MenuController extends GetxController {
  static MenuController instance = Get.find();
  var activeItem = overviewPageDisplayName.obs;
  var hoverItem = "".obs;

  changeActiveItemTo(String itemName) {
    activeItem.value = itemName;
  }

  onHover(String itemName) {
    if (!isActive(itemName)) hoverItem.value = itemName;
  }

  isActive(String itemName) => activeItem.value == itemName;

  isHovering(String itemName) => hoverItem.value == itemName;

  Widget returnIconFor(String itemName) {
    switch (itemName) {
      case overviewPageDisplayName:
        return _customIcon(Icons.trending_up, itemName);
      case teachPageDisplayName:
        return _customIcon(Icons.school_sharp, itemName);
      case booksPageDisplayName:
        return _customIcon(Icons.book, itemName);
      case gamesPageDisplayName:
        return _customIcon(Icons.gamepad, itemName);
      case reportsPageDisplayName:
        return _customIcon(Icons.summarize, itemName);
      case chatsPageDisplayName:
        return _customIcon(Icons.chat, itemName);
      case activitiesPageDisplayName:
        return _customIcon(Icons.discord, itemName);
      case authenticationPageDisplayName:
        return _customIcon(Icons.exit_to_app, itemName);
      default:
        return _customIcon(Icons.exit_to_app, itemName);
    }
  }

  Widget _customIcon(IconData icon, String itemName) {
    if (isActive(itemName))
      return Icon(icon, size: 22, color: const Color(0xFF079992));
    return Icon(icon,
        color: isHovering(itemName) ? const Color(0xFF079992) : Colors.white);
  }
}
