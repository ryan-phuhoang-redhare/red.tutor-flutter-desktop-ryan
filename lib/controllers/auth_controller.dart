import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/api/api_client.dart';
import 'package:redtutor_flutter_webapp/models/request/login_request.dart';
import 'package:redtutor_flutter_webapp/services/authentication_service.dart';

class AuthController extends GetxController {
  static AuthController instance = Get.find();
  RxString data = ''.obs;
  final authService = AuthenticationService(dio: ApiClient.dio);
  Future<String> getToken(LoginRequest loginRequest) async {
    final String? token = await authService.login(loginRequest);
    if (token != null) {
      return token;
    } else {
      print('Token is not available');
      return 'Token is not available';
    }
  }
}
