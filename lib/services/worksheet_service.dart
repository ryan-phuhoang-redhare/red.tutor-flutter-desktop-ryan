import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:redtutor_flutter_webapp/api/endpoints.dart';
import 'package:redtutor_flutter_webapp/models/request/worksheet/worksheet_request.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/quiz.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/worksheet.dart';
import 'package:redtutor_flutter_webapp/services/authentication_service.dart';

class WorksheetService {
  final Dio dio;

  WorksheetService({required this.dio});

  Future<List<Worksheet>?> getAllWorksheets() async {
    List<Worksheet> result = [];
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response = await dio.get(
        Endpoints.getAllWorksheets,
      );

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        List<dynamic> worksheets = responseJson["payload"];
        result = worksheets.map((map) => Worksheet.fromJson(map)).toList();
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Get all worksheets got error: $e');
      return null;
    }
  }

  Future<Worksheet?> createWorksheet(WorksheetRequest worksheetRequest) async {
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response = await dio.post(Endpoints.createWorksheet,
          data: worksheetRequest.toJson());

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        dynamic worksheetJson = responseJson["payload"];
        Worksheet worksheet = Worksheet.fromJson(worksheetJson);
        return worksheet; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Create worksheet got error: $e');
      return null;
    }
  }

  Future<Worksheet?> updateWorksheet(WorksheetRequest worksheetRequest) async {
    Worksheet result;
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response = await dio.post(Endpoints.updateWorksheet,
          data: worksheetRequest.toJson());

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        dynamic worksheetJson = responseJson["payload"];
        result = Worksheet.fromJson(worksheetJson);
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Update worksheet got error: $e');
      return null;
    }
  }

  Future<void> deleteWorksheet(String id) async {
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response =
          await dio.post(Endpoints.deleteWorksheet, data: {"id": id});

      if (response.statusCode == 200) {
        print("Delete Successfully");
      } else {
        // Handle login errors (e.g., invalid credentials)
        print("Have some errors when deleting");
      }
    } catch (e) {
      // Handle network or other errors
      print('Delete worksheet got error: $e');
    }
  }

  Future<List<Quiz>?> getQuizzesByWorksheetId(String worksheetId) async {
    List<Quiz> result = [];
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response = await dio.get(Endpoints.getQuizzesByWorksheetId,
          queryParameters: {'worksheetId': worksheetId});

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        List<dynamic> quizzes = responseJson["payload"];
        result = quizzes.map((map) => Quiz.fromJson(map)).toList();
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Get all quizzes by worksheet id got error: $e');
      return null;
    }
  }

  Future<dynamic> getQuizById(String quizId) async {
    dynamic result;
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response = await dio
          .get(Endpoints.getQuizById, queryParameters: {'quizId': quizId});

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        dynamic quiz = responseJson["payload"];
        result = Quiz.fromJson(quiz);
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Get Quiz by quiz id got error: $e');
      return null;
    }
  }
}
