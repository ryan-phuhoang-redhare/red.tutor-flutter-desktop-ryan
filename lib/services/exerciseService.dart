import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:redtutor_flutter_webapp/api/endpoints.dart';
import 'package:redtutor_flutter_webapp/models/exercise/exercise.dart';
import 'package:redtutor_flutter_webapp/services/authentication_service.dart';

class ExerciseService {
  final Dio dio;

  ExerciseService({required this.dio});

  Future<List<Exercise>?> getAllExercises() async {
    List<Exercise> result = [];
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response = await dio.get(
        Endpoints.getAllExercises,
      );

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        List<dynamic> exercises = responseJson["payload"];
        result = exercises.map((map) => Exercise.fromJson(map)).toList();
        print("result: $result");
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Get all exercises got error: $e');
      return null;
    }
  }

  Future<Exercise?> getExerciseById(String id) async {
    Exercise result;
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      // Map bodyData = {"id": id};
      // var bodyJson = json.encode(bodyData);
      final response =
          await dio.get(Endpoints.getExerciseById, queryParameters: {"id": id});
      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        dynamic exerciseJson = responseJson["payload"];
        result = Exercise.fromJson(exerciseJson);
        return result;
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Get exercise by ID got error: $e');
      return null;
    }
  }

  Future<Exercise?> createExercise(Exercise exercise) async {
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token is unavailable');
      return null;
    }

    try {
      final response =
          await dio.post(Endpoints.createExercise, data: exercise.toJson());

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        dynamic exerciseJson = responseJson["payload"];
        Exercise exercise = Exercise.fromJson(exerciseJson);
        return exercise; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Create exercise got error: $e');
      return null;
    }
  }

  Future<Exercise?> updateExercise(Exercise exercise) async {
    Exercise result;
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token is unavailable');
      return null;
    }

    try {
      print("Request data: " + exercise.toJson().toString());
      var requestJson = exercise.toJson();
      requestJson.addAll({"exerciseId": exercise.id});
      final response =
          await dio.post(Endpoints.updateExercise, data: requestJson);

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        dynamic exerciseJson = responseJson["payload"];
        result = Exercise.fromJson(exerciseJson);
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Update exercise got error: $e');
      return null;
    }
  }

  Future<void> deleteExercise(String id) async {
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token is unavailable');
      return null;
    }

    try {
      final response = await dio.post(Endpoints.deleteExercise, data: id);

      if (response.statusCode == 200) {
        print("Delete Successfully");
      } else {
        // Handle login errors (e.g., invalid credentials)
        print("Have some errors when deleting");
      }
    } catch (e) {
      // Handle network or other errors
      print('Delete exercise got error: $e');
    }
  }
}
