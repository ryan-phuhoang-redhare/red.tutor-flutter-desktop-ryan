import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:redtutor_flutter_webapp/api/endpoints.dart';
import 'package:redtutor_flutter_webapp/models/topic/topic.dart';
import 'package:redtutor_flutter_webapp/services/authentication_service.dart';

class TopicService {
  final Dio dio;

  TopicService({required this.dio});

  Future<List<Topic>?> getAllTopics() async {
    List<Topic> result = [];
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response = await dio.get(
        Endpoints.getAllTopics,
      );

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        List<dynamic> topics = responseJson["payload"];
        result = topics.map((map) => Topic.fromJson(map)).toList();
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Get all topics got error: $e');
      return null;
    }
  }

  Future<Topic?> createTopic(Topic topic) async {
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response =
          await dio.post(Endpoints.createTopic, data: topic.toJson());

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        dynamic topicJson = responseJson["payload"];
        Topic topic = Topic.fromJson(topicJson);
        return topic; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Create topic got error: $e');
      return null;
    }
  }

  Future<Topic?> updateCourse(Topic topic) async {
    Topic result;
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response =
          await dio.post(Endpoints.updateTopic, data: topic.toJson());

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        dynamic topicJson = responseJson["payload"];
        result = Topic.fromJson(topicJson);
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Update topic got error: $e');
      return null;
    }
  }

  Future<void> deleteTopic(String id) async {
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response = await dio.post(Endpoints.deleteTopic, data: {"id": id});

      if (response.statusCode == 200) {
        print("Delete Successfully");
      } else {
        // Handle login errors (e.g., invalid credentials)
        print("Have some errors when deleting");
      }
    } catch (e) {
      // Handle network or other errors
      print('Delete topic got error: $e');
    }
  }
}
