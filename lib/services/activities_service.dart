import 'package:dio/dio.dart';
import 'package:redtutor_flutter_webapp/api/endpoints.dart';
import 'package:redtutor_flutter_webapp/models/request/activity/bot_request.dart';
import 'package:redtutor_flutter_webapp/services/authentication_service.dart';

class ActivityService {
  final Dio dio;

  ActivityService({required this.dio});

  Future<void> createBot(BotRequest botRequest) async {
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response =
          await dio.post(Endpoints.createBot, data: botRequest.toJson());

      if (response.statusCode == 200) {
        print('File upload successful'); // Return the token on successful login
      } else {
        print('File upload failed with status: ${response.statusCode}');
      }
    } catch (e) {
      print("Error picking files: $e");
    }
  }
}
