import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:redtutor_flutter_webapp/api/endpoints.dart';
import 'package:redtutor_flutter_webapp/models/request/login_request.dart';
import 'package:redtutor_flutter_webapp/utils/http_decryptor.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthenticationService {
  final Dio dio;

  AuthenticationService({required this.dio});

  Future<String?> login(LoginRequest loginRequest) async {
    try {
      final response = await dio.post(
        Endpoints.login,
        data: {
          'email': 'admin.redtutor@gmail.com',
          'password': '123456',
          'typeLogin': 'Local'
        },
      );

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        Map<String, dynamic> decryptedData =
            HttpDecryptor().decrypt(responseJson["payload"].toString());
        String token = decryptedData['token'] as String;
        Map<String, dynamic> user = decryptedData['user'];
        await _storeUserId(user['id']);
        // Store the token securely (e.g., in SharedPreferences)
        await _storeToken(token);

        return token; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Login error: $e');
      return null;
    }
  }

  static Future<void> _storeUserId(String userId) async {
    final preferences = await SharedPreferences.getInstance();
    await preferences.setString('userId', userId);
  }

  static Future<String?> getUserId() async {
    final preferences = await SharedPreferences.getInstance();
    return preferences.getString('userId');
  }

  static Future<void> _storeToken(String token) async {
    final preferences = await SharedPreferences.getInstance();
    await preferences.setString('token', token);
  }

  static Future<String?> getToken() async {
    final preferences = await SharedPreferences.getInstance();
    return preferences.getString('token');
  }
}
