import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:redtutor_flutter_webapp/api/endpoints.dart';
import 'package:redtutor_flutter_webapp/models/book/ebook.dart';
import 'package:redtutor_flutter_webapp/services/authentication_service.dart';

class EbooksService {
  final Dio dio;

  EbooksService({required this.dio});

  Future<List<Ebook>?> getAllEbooks() async {
    List<Ebook> result = [];
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();
    print('bearerToken: $bearerToken');

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response = await dio.get(
        Endpoints.getAllEbooks,
      );

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        List<dynamic> ebooks = responseJson["payload"];
        print('Fetch Ebooks list ebooks: $ebooks');
        result = ebooks.map((map) => Ebook.fromJson(map)).toList();
        print("result: $result");
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Get all ebooks got error: $e');
      return null;
    }
  }

  // Future<Ebook?> getEbookById(String id) async {
  //   Ebook result;
  //   // Define your Bearer token
  //   String? bearerToken = await AuthenticationService.getToken();

  //   // Add the Bearer token to the headers
  //   if (bearerToken!.isNotEmpty) {
  //     dio.options.headers['Authorization'] = 'Bearer $bearerToken';
  //   } else {
  //     print('Token not unavailable');
  //     return null;
  //   }

  //   try {
  //     // Map bodyData = {"id": id};
  //     // var bodyJson = json.encode(bodyData);
  //     final response =
  //         await dio.get(Endpoints.getEbookById, queryParameters: {"id": id});
  //     if (response.statusCode == 200) {
  //       Response<dynamic> res = response;
  //       Map<String, dynamic> responseJson = json.decode(res.toString());
  //       dynamic ebookJson = responseJson["payload"];
  //       result = Ebook.fromJson(ebookJson);
  //       return result;
  //     } else {
  //       // Handle login errors (e.g., invalid credentials)
  //       return null;
  //     }
  //   } catch (e) {
  //     // Handle network or other errors
  //     print('Get ebook by ID got error: $e');
  //     return null;
  //   }
  // }

  // Future<Ebook?> createEbook(Ebook ebook) async {
  //   // Define your Bearer token
  //   String? bearerToken = await AuthenticationService.getToken();

  //   // Add the Bearer token to the headers
  //   if (bearerToken!.isNotEmpty) {
  //     dio.options.headers['Authorization'] = 'Bearer $bearerToken';
  //   } else {
  //     print('Token is unavailable');
  //     return null;
  //   }

  //   try {
  //     final response =
  //         await dio.post(Endpoints.createEbook, data: ebook.toJson());

  //     if (response.statusCode == 200) {
  //       Response<dynamic> res = response;
  //       Map<String, dynamic> responseJson = json.decode(res.toString());
  //       dynamic ebookJson = responseJson["payload"];
  //       Ebook ebook = Ebook.fromJson(ebookJson);
  //       return ebook; // Return the token on successful login
  //     } else {
  //       // Handle login errors (e.g., invalid credentials)
  //       return null;
  //     }
  //   } catch (e) {
  //     // Handle network or other errors
  //     print('Create ebook got error: $e');
  //     return null;
  //   }
  // }

  // Future<Ebook?> updateEbook(Ebook ebook) async {
  //   Ebook result;
  //   // Define your Bearer token
  //   String? bearerToken = await AuthenticationService.getToken();

  //   // Add the Bearer token to the headers
  //   if (bearerToken!.isNotEmpty) {
  //     dio.options.headers['Authorization'] = 'Bearer $bearerToken';
  //   } else {
  //     print('Token is unavailable');
  //     return null;
  //   }

  //   try {
  //     print("Request data: " + ebook.toJson().toString());
  //     var requestJson = ebook.toJson();
  //     requestJson.addAll({"ebookId": ebook.id});
  //     final response = await dio.post(Endpoints.updateEbook, data: requestJson);

  //     if (response.statusCode == 200) {
  //       Response<dynamic> res = response;
  //       Map<String, dynamic> responseJson = json.decode(res.toString());
  //       dynamic ebookJson = responseJson["payload"];
  //       result = Ebook.fromJson(ebookJson);
  //       return result; // Return the token on successful login
  //     } else {
  //       // Handle login errors (e.g., invalid credentials)
  //       return null;
  //     }
  //   } catch (e) {
  //     // Handle network or other errors
  //     print('Update ebook got error: $e');
  //     return null;
  //   }
  // }

  // Future<void> deleteEbook(String id) async {
  //   // Define your Bearer token
  //   String? bearerToken = await AuthenticationService.getToken();

  //   // Add the Bearer token to the headers
  //   if (bearerToken!.isNotEmpty) {
  //     dio.options.headers['Authorization'] = 'Bearer $bearerToken';
  //   } else {
  //     print('Token is unavailable');
  //     return null;
  //   }

  //   try {
  //     final response = await dio.post(Endpoints.deleteEbook, data: id);

  //     if (response.statusCode == 200) {
  //       print("Delete Successfully");
  //     } else {
  //       // Handle login errors (e.g., invalid credentials)
  //       print("Have some errors when deleting");
  //     }
  //   } catch (e) {
  //     // Handle network or other errors
  //     print('Delete ebook got error: $e');
  //   }
  // }
}
