import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:redtutor_flutter_webapp/api/endpoints.dart';
import 'package:redtutor_flutter_webapp/models/course/course.dart';
import 'package:redtutor_flutter_webapp/models/course/learningLevelGraph.dart';
import 'package:redtutor_flutter_webapp/models/request/course/graph_request.dart';
import 'package:redtutor_flutter_webapp/services/authentication_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CoursesService {
  final Dio dio;

  CoursesService({required this.dio});

  Future<List<Course>?> getAllCourses() async {
    List<Course> result = [];
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response = await dio.get(
        Endpoints.getAllCourses,
      );

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        List<dynamic> courses = responseJson["payload"];
        result = courses.map((map) => Course.fromJson(map)).toList();
        _storeCoursesInSharedPreferences(result);
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Get all courses error: $e');
      return null;
    }
  }

  Future<void> deleteCourse(String id) async {
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response = await dio.post(Endpoints.deleteCourse, data: {"id": id});

      if (response.statusCode == 200) {
        print("Delete Successfully");
      } else {
        // Handle login errors (e.g., invalid credentials)
        print("Have some errors when deleting");
      }
    } catch (e) {
      // Handle network or other errors
      print('Get all courses error: $e');
    }
  }

  Future<Course?> createGraph(AddNewGraphRequest addNewGraphRequest) async {
    dynamic result;
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }
    // var request = addNewGraphRequest.toJson();
    try {
      final response = await dio.post(Endpoints.createGraph,
          data: addNewGraphRequest.toJson());

      if (response.statusCode == 200) {
        // Response<dynamic> res = response;
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      if (e is DioError) {
        // Handle Dio errors
        print('DioError: ${e.message}');
      } else {
        // Handle other exceptions
        print('Creat graph error: $e');
      }
      // Handle network or other errors

      return null;
    }
  }

  Future<LearningLevelGraph?> updateGraph(
      UpdateGraphRequest updateGraphRequest) async {
    LearningLevelGraph result = LearningLevelGraph(rows: []);
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }
    // var request = updateGraphRequest.toJson();
    try {
      final response = await dio.post(Endpoints.updateGraph,
          data: updateGraphRequest.toJson());

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        result = LearningLevelGraph.fromJson(responseJson["payload"]);
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      if (e is DioError) {
        // Handle Dio errors
        print('DioError: ${e.message}');
      } else {
        // Handle other exceptions
        print('Creat graph error: $e');
      }
      // Handle network or other errors

      return null;
    }
  }

  Future<LearningLevelGraph?> getGraphFromDb(String courseId) async {
    LearningLevelGraph result = LearningLevelGraph(rows: []);
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }
    try {
      final response = await dio.get(Endpoints.getGraphByCourseId,
          queryParameters: {"courseId": courseId});

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        result = LearningLevelGraph.fromJson(responseJson["payload"]);
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      if (e is DioError) {
        // Handle Dio errors
        print('DioError: ${e.message}');
      } else {
        // Handle other exceptions
        print('Creat graph error: $e');
      }
      // Handle network or other errors

      return null;
    }
  }

  Future<Course?> createCourse(Course course) async {
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response =
          await dio.post(Endpoints.createCourse, data: course.toJson());

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        dynamic courseJson = responseJson["payload"];
        Course course = Course.fromJson(courseJson);
        return course; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Get all courses error: $e');
      return null;
    }
  }

  Future<Course?> updateCourse(Course course) async {
    Course result;
    // Define your Bearer token
    String? bearerToken = await AuthenticationService.getToken();

    // Add the Bearer token to the headers
    if (bearerToken!.isNotEmpty) {
      dio.options.headers['Authorization'] = 'Bearer $bearerToken';
    } else {
      print('Token not unavailable');
      return null;
    }

    try {
      final response =
          await dio.post(Endpoints.updateCourse, data: course.toJson());

      if (response.statusCode == 200) {
        Response<dynamic> res = response;
        Map<String, dynamic> responseJson = json.decode(res.toString());
        dynamic courseJson = responseJson["payload"];
        result = Course.fromJson(courseJson);
        return result; // Return the token on successful login
      } else {
        // Handle login errors (e.g., invalid credentials)
        return null;
      }
    } catch (e) {
      // Handle network or other errors
      print('Get all courses error: $e');
      return null;
    }
  }

  Future<void> _storeCoursesInSharedPreferences(List<Course> courses) async {
    final preferences = await SharedPreferences.getInstance();
    const key = 'courses';
    // Convert the list of objects to a List of Maps.
    final listJson = courses.map((obj) => obj.toJson()).toList();

    // Convert the List of Maps to a JSON string.
    final jsonString = jsonEncode(listJson);
    // Save the JSON string in SharedPreferences.
    await preferences.setString(key, jsonString);
  }

  static Future<List<Course>> getListCoursesFromSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    const key = 'courses';

    // Get the JSON string from SharedPreferences.
    final jsonString = prefs.getString(key);

    if (jsonString == null) {
      // If the key doesn't exist in SharedPreferences, return an empty list.
      return [];
    }

    // Convert the JSON string back to a List of Maps.
    final listJson = jsonDecode(jsonString) as List;
    // Convert each Map to a MyObject and return the list.
    return listJson.map((map) => Course.fromJson(map)).toList();
  }
}
