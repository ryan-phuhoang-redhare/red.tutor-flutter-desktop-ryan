import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:flutter/material.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/models/request/login_request.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_courses/my_courses.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_exercises/my_exercises.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_topics/my_topics.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_worksheets/my_worksheets.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

class TeachPage extends StatefulWidget {
  const TeachPage({super.key});

  @override
  State<TeachPage> createState() => _TeachPageState();
}

class _TeachPageState extends State<TeachPage> {
  void resetState(var indexTab) {
    if (indexTab == 1) {
      exercisesController.changeAddNewStatusTo(false);
    } else if (indexTab == 2) {
      topicsController.changeAddNewStatusTo(false);
    } else if (indexTab == 3) {
      coursesController.changeAddNewStatusTo(false);
    } else {
      worksheetsController.changeAddNewStatusTo(false);
    }
  }

  @override
  void initState() {
    authController.getToken(LoginRequest("admin.redtutor@gmail.com", "123456"));
    worksheetsController.fetchData();
    exercisesController.fetchData();
    topicsController.fetchData();
    coursesController.fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(50),
      child: DefaultTabController(
        length: 4,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ButtonsTabBar(
              backgroundColor: Colors.lightGreen[400],
              unselectedBackgroundColor: Colors.grey[350],
              unselectedLabelStyle: const TextStyle(color: Colors.black),
              labelStyle: const TextStyle(
                  color: Colors.white, fontWeight: FontWeight.bold),
              buttonMargin: const EdgeInsets.only(right: 50),
              onTap: (p0) {
                resetState(p0);
              },
              tabs: [
                Tab(
                    child: Container(
                        margin: const EdgeInsets.all(10),
                        width: 150,
                        child: const Center(
                            child: CustomText(text: "My Worksheets")))),
                Tab(
                    child: Container(
                        margin: const EdgeInsets.all(10),
                        width: 150,
                        child: const Center(
                            child: CustomText(text: "My Exercises")))),
                Tab(
                    child: Container(
                        margin: const EdgeInsets.all(10),
                        width: 150,
                        child: const Center(
                            child: CustomText(text: "My Topics")))),
                Tab(
                    child: Container(
                        margin: const EdgeInsets.all(10),
                        width: 150,
                        child: const Center(
                            child: CustomText(text: "My Courses")))),
                // Tab(
                //     child: Container(
                //         margin: const EdgeInsets.all(10),
                //         width: 150,
                //         child:
                //             const Center(child: CustomText(text: "Review")))),
              ],
            ),
            const Expanded(
              child: TabBarView(
                children: <Widget>[
                  MyWorksheetsTab(),
                  MyExercises(),
                  MyTopicsTab(),
                  MyCoursesTab()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
