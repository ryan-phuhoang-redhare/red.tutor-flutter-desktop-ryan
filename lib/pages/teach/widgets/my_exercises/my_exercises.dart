import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_exercises/widgets/add_new_exercise.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_exercises/widgets/exercises_table.dart';

class MyExercises extends StatelessWidget {
  const MyExercises({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() => Expanded(
          child: ListView(children: [
            if (exercisesController.addNewStatus.value == false)
              const ExercisesTable()
            else
              const AddNewExercise()
          ]),
        ));
  }
}
