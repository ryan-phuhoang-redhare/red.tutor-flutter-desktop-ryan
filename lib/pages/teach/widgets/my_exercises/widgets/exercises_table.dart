import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/constants/style.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/worksheet.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';
import 'package:redtutor_flutter_webapp/widgets/popup_untils.dart';

class ExercisesTable extends StatefulWidget {
  const ExercisesTable({super.key});

  @override
  State<ExercisesTable> createState() => _ExercisesTableState();
}

class _ExercisesTableState extends State<ExercisesTable> {
  // Future<void> saveSelectedExercise(int index) async {
  //   var exerciseSelected = exercisesController.exercisesObs[index];
  //   await exercisesController.selectedExercise(exerciseSelected);
  // }

  Future<void> saveSelectedExercise(int index) async {
    var exerciseSelected = exercisesController.exercisesObs[index];
    var exerciseId = exerciseSelected.id;
    var exerciseResponse = await exercisesController.fetchById(exerciseId);
    await exercisesController.selectedExercise(exerciseResponse!);
  }

  @override
  void initState() {
    exercisesController.fetchData();
    worksheetsController.fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: active.withOpacity(.4), width: .5),
            boxShadow: [
              BoxShadow(
                  offset: const Offset(0, 6),
                  color: lightGrey.withOpacity(.1),
                  blurRadius: 12)
            ],
            borderRadius: BorderRadius.circular(8),
          ),
          padding: const EdgeInsets.all(16),
          margin: const EdgeInsets.only(bottom: 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  const SizedBox(
                    width: 10,
                  ),
                  CustomText(
                    text:
                        "Published (${exercisesController.exercisesObs.length})",
                    color: dark,
                    weight: FontWeight.bold,
                  ),
                  // Container(
                  //   width: 1,
                  //   height: 22,
                  //   color: dark,
                  //   margin: const EdgeInsets.only(left: 10, right: 10),
                  // ),
                  // CustomText(
                  //   text: "Draft (3)",
                  //   color: lightGrey,
                  //   weight: FontWeight.bold,
                  // ),
                  const SizedBox(
                    width: 50,
                  ),
                  SizedBox(
                    width: 150,
                    height: 50,
                    child: OutlinedButton.icon(
                        onPressed: () {
                          exercisesController.changeAddNewStatusTo(true);
                          //Ryan: To Do:
                          exercisesController
                              .changeUpdateExerciseStatusTo(false);
                        },
                        icon: Icon(
                          Icons.add,
                          color: dark,
                        ),
                        label: const CustomText(text: "Add New")),
                  ),
                  Expanded(child: Container()),
                  // const Expanded(
                  //   child: TextField(
                  //     decoration: InputDecoration(
                  //       hintText: 'Search...',
                  //     ),
                  //   ),
                  // ),
                  // IconButton(
                  //   onPressed: () {},
                  //   icon: const Icon(Icons.search),
                  // ),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              // 56: dataRowHeight
              // 7: number of data each page
              // 40: headingRowHeight
              SizedBox(
                height: (100 * 7) + 40,
                child: DataTable2(
                  columnSpacing: 12,
                  dataRowHeight: 100,
                  headingRowHeight: 40,
                  horizontalMargin: 12,
                  minWidth: 600,
                  showCheckboxColumn: false,
                  //Ryan: To Do: showCheckboxColumn: false,
                  columns: const [
                    DataColumn2(
                        label: CustomText(
                          text: "Exercise Title",
                          weight: FontWeight.bold,
                        ),
                        size: ColumnSize.L),
                    DataColumn2(
                        label: Row(
                      children: [
                        CustomText(
                            text: 'Last Edited', weight: FontWeight.bold),
                        // IconButton(
                        //   hoverColor: Colors.transparent,
                        //   icon: const Icon(Icons.arrow_upward),
                        //   onPressed: () {},
                        // )
                      ],
                    )),
                    DataColumn2(
                      label: CustomText(
                          text: 'Assigned Worksheet', weight: FontWeight.bold),
                      size: ColumnSize.L,
                    ),
                    DataColumn2(
                        label: CustomText(
                          text: "Qns Limit",
                          weight: FontWeight.bold,
                        ),
                        size: ColumnSize.S),
                    DataColumn2(
                        label: CustomText(
                          text: "Pass",
                          weight: FontWeight.bold,
                        ),
                        size: ColumnSize.S),
                    DataColumn2(
                        label: CustomText(
                          text: "Timer",
                          weight: FontWeight.bold,
                        ),
                        size: ColumnSize.S),
                    DataColumn2(
                      label: Row(
                        children: [
                          CustomText(text: 'Status', weight: FontWeight.bold),
                          // IconButton(
                          //   hoverColor: Colors.transparent,
                          //   icon: const Icon(Icons.arrow_upward),
                          //   onPressed: () {},
                          // )
                        ],
                      ),
                      size: ColumnSize.S,
                    ),
                    DataColumn2(
                      label: CustomText(text: ''),
                      size: ColumnSize.M,
                    ),
                  ],
                  rows: List<DataRow>.generate(
                      exercisesController.exercisesObs.length, (index) {
                    var exercise = exercisesController.exercisesObs[index];
                    Worksheet? worksheet = worksheetsController.worksheetsObs
                        .firstWhereOrNull((workSheet) =>
                            workSheet.id == exercise.worksheetId);
                    return DataRow(
                      cells: [
                        // Exercise title
                        DataCell(CustomText(text: exercise.title)),
                        // Last Edited
                        DataCell(CustomText(
                            text: DateFormat('MM/dd/yyyy').format(
                                DateTime.fromMillisecondsSinceEpoch(
                                    exercise.lastEdited!)))),
                        // Assigned Worksheet
                        DataCell(CustomText(
                            text: worksheet == null ? "" : worksheet.title)),
                        // Qns Limit
                        DataCell(CustomText(
                            text: exercise.questionLimit.toString())),
                        // Pass
                        DataCell(
                            CustomText(text: exercise.minimumScore.toString())),
                        // Timer
                        DataCell(CustomText(
                            text: exercise.timerDuration == 0
                                ? "---"
                                : exercise.timerDuration.toString())),
                        // Status
                        const DataCell(CustomText(text: "Published")),
                        // Action button
                        DataCell(Row(
                          children: [
                            Visibility(
                              maintainSize: true,
                              maintainAnimation: true,
                              maintainState: true,
                              visible: (exercise.worksheetId == "") &&
                                      (exercise.topicId == "")
                                  ? false
                                  : true,
                              child: IconButton(
                                hoverColor: Colors.transparent,
                                icon: const Icon(Icons.delete),
                                onPressed: () async {
                                  if ((exercise.worksheetId == "") &&
                                      (exercise.topicId == "")) {
                                    null;
                                  } else {
                                    await saveSelectedExercise(index);
                                    try {
                                      exercisesController.deleteExercise();
                                      showPopupStatus(context, 'success');
                                    } catch (e) {
                                      showPopupStatus(context, 'error');
                                    }
                                  }
                                },
                              ),
                            ),
                            // IconButton(
                            //   hoverColor: Colors.transparent,
                            //   icon: const Icon(Icons.delete),
                            //   onPressed: () async {
                            //     // await saveSelectedExercise(index);
                            //     // try {
                            //     //   exercisesController.deleteExercise();
                            //     //   showPopupStatus(context, 'success');
                            //     // } catch (e) {
                            //     //   showPopupStatus(context, 'error');
                            //     // }
                            //     null;
                            //   },
                            // ),
                            // IconButton(
                            //   hoverColor: Colors.transparent,
                            //   icon: const Icon(Icons.copy),
                            //   onPressed: () {},
                            // ),
                          ],
                        )),
                      ],
                      onSelectChanged: (isSelected) async {
                        // Handle row click action here
                        await saveSelectedExercise(index);
                        setState(() {
                          exercisesController.changeAddNewStatusTo(true);
                          exercisesController
                              .changeUpdateExerciseStatusTo(true);
                        });
                      },
                    );
                  }),
                ),
              ),
            ],
          ),
        ));
  }
}
