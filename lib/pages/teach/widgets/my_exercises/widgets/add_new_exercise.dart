//import 'dart:ffi';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/constants/style.dart';
import 'package:redtutor_flutter_webapp/models/exercise/exercise.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/worksheet.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';
import 'package:quill_html_editor/quill_html_editor.dart';

class AddNewExercise extends StatefulWidget {
  const AddNewExercise({super.key});

  @override
  State<AddNewExercise> createState() => _AddNewExerciseState();
}

class _AddNewExerciseState extends State<AddNewExercise> {
  bool _isBold = false;
  bool _isUnderlined = false;
  bool _isItalic = false;
  bool _isAlignLeft = false;
  bool _isAlignCenter = false;
  bool _isAlignRight = false;
  var _questionLimitQuantity = 1;
  int _questionQuantity = 0;
  int _passLimit = 0;
  int _timer = 0;

  List<Worksheet>? _listWorksheet;
  Worksheet? chosenWorksheet;

  String _topicId = "";

  String _exerciseNoteContent = "";

  //String _attachedTopicTitle = "";
  String _lastEditedString = "---";

  final _noteEditorTextStyle = const TextStyle(
    //fontSize: 18,
    //color: Colors.black,
    //fontWeight: FontWeight.normal,
    fontFamily: 'Montserrat',
  );

  String tempVimeoUrl = "";
  String tempVimeoHeight = "";
  String tempVimeoWidth = "";

  String tempHyperlinkText = "";
  String tempHyperlinkURL = "";

  String tempHyperlinkButtonText = "";
  String tempHyperlinkButtonURL = "";

  final FocusNode _titleTextFieldFocusNode = FocusNode();

  final TextEditingController _titleTextController = TextEditingController();
  final QuillEditorController _noteQuillEditorController =
      QuillEditorController();
  //final TextEditingController _questionLimitQuantityController =
  //    TextEditingController();
  final TextEditingController _questionQuantityTextController =
      TextEditingController(text: "---");

  dynamic exerciseSelectedObject = {};

  final _editorTextStyle = const TextStyle(
    fontSize: 24,
    color: Colors.red,
    fontWeight: FontWeight.normal,
    fontFamily: 'Roboto',
  );

  @override
  void initState() {
    worksheetsController.fetchData().then((list) => _listWorksheet = list).then(
        (value) => exercisesController.updateExerciseStatus.value
            ? _loadRetrievedExerciseSelected()
            : null);
    //_questionLimitQuantityController.text = _questionLimitQuantity.toString();
    super.initState();
  }

  void _saveDraft() async {
    Exercise? retrievedExerciseSelected =
        await exercisesController.loadExerciseSelectedFromSharedPreferences();
    String id = "";
    if (retrievedExerciseSelected != null) {
      id = retrievedExerciseSelected.id;
    }
    exercisesController.saveDraft(
        id,
        _titleTextController.text,
        chosenWorksheet == null ? "" : (chosenWorksheet as Worksheet).id!,
        _topicId,
        _exerciseNoteContent,
        _questionLimitQuantity,
        _passLimit,
        _timer);
  }

  void onChangedExerciseNoteContent(String htmlText) {
    debugPrint("HTML Text Note Content: $htmlText");
    setState(() {
      _exerciseNoteContent = htmlText;
    });
  }

  void onNoteEditorFocusChanged(hasFocus) {
    if (hasFocus) {
      _titleTextFieldFocusNode.unfocus();
    } else {
      setState(() {});
    }
  }

// Actions question quantity
  void _increaseQuestionLimitQuantity() {
    if (_questionLimitQuantity < _questionQuantity) {
      setState(() {
        _questionLimitQuantity++;
      });
    }
  }

  void _decreaseQuestionLimitQuantity() {
    if (_questionLimitQuantity > 1) {
      setState(() {
        _questionLimitQuantity--;
      });
    }
  }

  // Actions pass limit
  void _increasePassLimit() {
    if (_passLimit < 100) {
      setState(() {
        _passLimit++;
      });
    }
  }

  void _decreasePassLimit() {
    if (_passLimit > 0) {
      setState(() {
        _passLimit--;
      });
    }
  }

  // Actions timer
  void _increaseTimer() {
    setState(() {
      _timer++;
    });
  }

  void _decreaseTimer() {
    if (_timer > 0) {
      setState(() {
        _timer--;
      });
    }
  }

  void _loadRetrievedExerciseSelected() async {
    Exercise? retrievedExerciseSelected =
        await exercisesController.loadExerciseSelectedFromSharedPreferences();
    if (retrievedExerciseSelected?.id != null) {
      _titleTextController.text = retrievedExerciseSelected!.title;
      if (_listWorksheet != null) {
        chosenWorksheet = _listWorksheet!.firstWhere((worksheet) =>
            worksheet.id == retrievedExerciseSelected.worksheetId);
      }
      _questionQuantity = (chosenWorksheet as Worksheet).quizzes.length;
      _questionQuantityTextController.text = _questionQuantity.toString();
      //setState(() {
      exerciseSelectedObject = retrievedExerciseSelected;
      _topicId = retrievedExerciseSelected.topicId!;
      _exerciseNoteContent = retrievedExerciseSelected.noteContent;
      _questionLimitQuantity = retrievedExerciseSelected.questionLimit;
      //_attachedTopicTitle = retrievedExerciseSelected.topicId == ""
      //   ? "---"
      //  : topicsController.findById(retrievedExerciseSelected.topicId!).title;
      int lastEdited = retrievedExerciseSelected.lastEdited!;
      _lastEditedString = lastEdited == 0
          ? "---"
          : DateFormat('MM/dd/yyyy')
              .format(DateTime.fromMillisecondsSinceEpoch(lastEdited));
      _passLimit = retrievedExerciseSelected.minimumScore;
      _timer = retrievedExerciseSelected.timerDuration;
      //});
      setState(() {});
    }
  }

  initialQuillEditor() async {
    if (exercisesController.updateExerciseStatus.value) {
      await _noteQuillEditorController
          .setText(exerciseSelectedObject.noteContent)
          .then((value) => print("quillEditorController: $value"))
          .catchError((error) {
        print("Error setting Quill editor content: $error");
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                flex: 4,
                child: Container(
                  margin: const EdgeInsets.only(right: 50),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // Exercise Title
                      Container(
                        margin: const EdgeInsets.only(top: 10, bottom: 10),
                        child: Column(
                          children: [
                            const Align(
                                alignment: Alignment.topLeft,
                                child: CustomText(
                                  text: "Exercise Title",
                                  weight: FontWeight.bold,
                                )),
                            const SizedBox(
                              height: 10,
                            ),
                            TextFormField(
                              controller: _titleTextController,
                              focusNode: _titleTextFieldFocusNode,
                              decoration: const InputDecoration(
                                hintText: 'Exercise Title Text...',
                                border: OutlineInputBorder(),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 30),
                      // // Notes Description
                      // Column(
                      //   children: [
                      //     const Align(
                      //         alignment: Alignment.topLeft,
                      //         child: CustomText(
                      //           text: "Notes",
                      //           weight: FontWeight.bold,
                      //         )),
                      //     const SizedBox(
                      //       height: 10,
                      //     ),
                      //     Align(
                      //         alignment: Alignment.topLeft,
                      //         child: _buildToolbar()),
                      //     Stack(
                      //       alignment: Alignment.topRight,
                      //       children: [
                      //         TextFormField(
                      //           controller:
                      //               notesDescriptionTextEditingController,
                      //           onChanged: (text) {
                      //             _detectVimeoUrl(
                      //                 notesDescriptionTextEditingController);
                      //           },
                      //           maxLines: 4,
                      //           decoration: const InputDecoration(
                      //             hintText: 'Notes Description Text...',
                      //             border: OutlineInputBorder(),
                      //           ),
                      //         ),
                      //         IconButton(
                      //           hoverColor: Colors.transparent,
                      //           icon: const Icon(Icons.info),
                      //           onPressed: () {},
                      //         )
                      //       ],
                      //     )
                      //   ],
                      // ),
                      // const SizedBox(height: 50),

                      // Notes Description Quill
                      Container(
                        margin: const EdgeInsets.only(right: 50),
                        child: Column(
                          children: [
                            const Align(
                                alignment: Alignment.topLeft,
                                child: CustomText(
                                  text: "Notes",
                                  weight: FontWeight.bold,
                                )),
                            const SizedBox(
                              height: 10,
                            ),
                            Align(
                                alignment: Alignment.topLeft,
                                child: SizedBox(
                                  height: MediaQuery.of(context).size.height,
                                  child: Scaffold(
                                    body: Column(
                                      children: [
                                        ToolBar(
                                          toolBarConfig: const [
                                            ToolBarStyle.size,
                                            ToolBarStyle.bold,
                                            ToolBarStyle.italic,
                                            ToolBarStyle.underline,
                                            ToolBarStyle.undo,
                                            ToolBarStyle.redo,
                                            ToolBarStyle.listBullet,
                                            ToolBarStyle.video,
                                            //ToolBarStyle.link
                                          ],
                                          toolBarColor: Colors.cyan.shade50,
                                          activeIconColor: Colors.green,
                                          padding: const EdgeInsets.all(12),
                                          iconSize: 20,
                                          controller:
                                              _noteQuillEditorController,
                                          customButtons: [
                                            InkWell(
                                                onTap: () {
                                                  // _noteQuillEditorController
                                                  //     .unFocus();
                                                  // _noteQuillEditorController
                                                  //     .enableEditor(false);
                                                  tempHyperlinkText = "";
                                                  showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                          context) {
                                                        return AlertDialog(
                                                          alignment: Alignment
                                                              .topCenter,
                                                          scrollable: false,
                                                          title: const Text(
                                                              'Insert Hyperlink'),
                                                          content: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(8.0),
                                                            child: Form(
                                                              child: Column(
                                                                mainAxisSize:
                                                                    MainAxisSize
                                                                        .min,
                                                                children: <Widget>[
                                                                  TextFormField(
                                                                    decoration:
                                                                        const InputDecoration(
                                                                      labelText:
                                                                          'Hyperlink URL',
                                                                      // icon: Icon(Icons
                                                                      //     .account_box),
                                                                    ),
                                                                    onChanged: (value) =>
                                                                        tempHyperlinkURL =
                                                                            value,
                                                                  ),
                                                                  TextFormField(
                                                                    decoration:
                                                                        const InputDecoration(
                                                                      labelText:
                                                                          'Hyperlink Text (leave out to show as hyperlink URL)',
                                                                      // icon: Icon(Icons
                                                                      //     .account_box),
                                                                    ),
                                                                    onChanged: (value) =>
                                                                        tempHyperlinkText =
                                                                            value,
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                          actions: [
                                                            ElevatedButton(
                                                                child:
                                                                    const Text(
                                                                        "Done"),
                                                                onPressed: () {
                                                                  if (tempHyperlinkText ==
                                                                      "") {
                                                                    tempHyperlinkText =
                                                                        tempHyperlinkURL;
                                                                  }
                                                                  String
                                                                      hyperlinkTag =
                                                                      '''<a href="$tempHyperlinkURL">$tempHyperlinkText</a>''';
                                                                  _noteQuillEditorController
                                                                      .insertText(
                                                                          hyperlinkTag);
                                                                  Navigator.pop(
                                                                      context);
                                                                })
                                                          ],
                                                        );
                                                      });
                                                },
                                                child: const Icon(Icons.link)),

                                            InkWell(
                                                onTap: () {
                                                  tempHyperlinkButtonText = "";
                                                  showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                          context) {
                                                        return AlertDialog(
                                                          alignment: Alignment
                                                              .topCenter,
                                                          scrollable: false,
                                                          title: const Text(
                                                              'Insert Hyperlink as a Button'),
                                                          content: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(8.0),
                                                            child: Form(
                                                              child: Column(
                                                                mainAxisSize:
                                                                    MainAxisSize
                                                                        .min,
                                                                children: <Widget>[
                                                                  TextFormField(
                                                                    decoration:
                                                                        const InputDecoration(
                                                                      labelText:
                                                                          'Hyperlink URL',
                                                                      // icon: Icon(Icons
                                                                      //     .account_box),
                                                                    ),
                                                                    onChanged: (value) =>
                                                                        tempHyperlinkButtonURL =
                                                                            value,
                                                                  ),
                                                                  TextFormField(
                                                                    decoration:
                                                                        const InputDecoration(
                                                                      labelText:
                                                                          'Hyperlink Text (leave out to show as hyperlink URL)',
                                                                      // icon: Icon(Icons
                                                                      //     .account_box),
                                                                    ),
                                                                    onChanged: (value) =>
                                                                        tempHyperlinkButtonText =
                                                                            value,
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                          actions: [
                                                            ElevatedButton(
                                                                child:
                                                                    const Text(
                                                                        "Done"),
                                                                onPressed: () {
                                                                  if (tempHyperlinkButtonText ==
                                                                      "") {
                                                                    tempHyperlinkButtonText =
                                                                        tempHyperlinkButtonURL;
                                                                  }
                                                                  String
                                                                      hyperlinkTag =
                                                                      '''<a style="color:White;background-color:MediumSeaGreen" href="$tempHyperlinkButtonURL">$tempHyperlinkButtonText</a>''';
                                                                  _noteQuillEditorController
                                                                      .insertText(
                                                                          hyperlinkTag);
                                                                  Navigator.pop(
                                                                      context);
                                                                })
                                                          ],
                                                        );
                                                      });
                                                },
                                                child: const Icon(Icons
                                                    .dataset_linked_outlined)),
                                            // InkWell(
                                            //     onTap: () {
                                            //       // _noteQuillEditorController
                                            //       //     .unFocus();
                                            //       // _noteQuillEditorController
                                            //       //     .enableEditor(false);
                                            //       showDialog(
                                            //           context: context,
                                            //           builder: (BuildContext
                                            //               context) {
                                            //             return AlertDialog(
                                            //               alignment: Alignment
                                            //                   .topCenter,
                                            //               scrollable: false,
                                            //               title: const Text(
                                            //                   'Insert Vimeo Video'),
                                            //               content: Padding(
                                            //                 padding:
                                            //                     const EdgeInsets
                                            //                         .all(8.0),
                                            //                 child: Form(
                                            //                   child: Column(
                                            //                     mainAxisSize:
                                            //                         MainAxisSize
                                            //                             .min,
                                            //                     children: <Widget>[
                                            //                       TextFormField(
                                            //                         decoration:
                                            //                             const InputDecoration(
                                            //                           labelText:
                                            //                               'Video URL',
                                            //                           // icon: Icon(Icons
                                            //                           //     .account_box),
                                            //                         ),
                                            //                         onChanged: (value) =>
                                            //                             tempVimeoUrl =
                                            //                                 value,
                                            //                       ),
                                            //                       TextFormField(
                                            //                         decoration:
                                            //                             const InputDecoration(
                                            //                           labelText:
                                            //                               'Video width',
                                            //                           // icon: Icon(
                                            //                           //     Icons.message),
                                            //                         ),
                                            //                         onChanged: (value) =>
                                            //                             tempVimeoWidth =
                                            //                                 value,
                                            //                       ),
                                            //                       TextFormField(
                                            //                         decoration:
                                            //                             const InputDecoration(
                                            //                           labelText:
                                            //                               'Video height',
                                            //                           // icon: Icon(
                                            //                           //     Icons.email),
                                            //                         ),
                                            //                         onChanged: (value) =>
                                            //                             tempVimeoHeight =
                                            //                                 value,
                                            //                       ),
                                            //                     ],
                                            //                   ),
                                            //                 ),
                                            //               ),
                                            //               actions: [
                                            //                 ElevatedButton(
                                            //                     child:
                                            //                         const Text(
                                            //                             "Done"),
                                            //                     onPressed: () {
                                            //                       String
                                            //                           videoIframe =
                                            //                           "<iframe class=\"ql-video\" width=\"$tempVimeoWidth\" height=\"$tempVimeoHeight\" frameborder=\"0\" allowfullscreen=\"true\" src=\"$tempVimeoUrl\"></iframe>";
                                            //                       _noteQuillEditorController
                                            //                           .insertText(
                                            //                               videoIframe);
                                            //                       Navigator.pop(
                                            //                           context);
                                            //                     })
                                            //               ],
                                            //             );
                                            //           });
                                            //     },
                                            //     child: const Icon(
                                            //         Icons.ondemand_video)),
                                          ],
                                        ),
                                        Expanded(
                                            child: QuillHtmlEditor(
                                          text: _exerciseNoteContent,
                                          // textStyle: const TextStyle(
                                          //     fontSize: 18,
                                          //     color: Colors.black,
                                          //     fontWeight: FontWeight.normal,
                                          //     fontFamily: 'Roboto'),
                                          textStyle: _noteEditorTextStyle,
                                          hintText: '',
                                          controller:
                                              _noteQuillEditorController,
                                          isEnabled: true,
                                          minHeight: 300,
                                          padding: const EdgeInsets.only(
                                              left: 10, top: 5),
                                          //hintTextPadding: EdgeInsets.zero,
                                          //backgroundColor: _backgroundColor,
                                          onFocusChanged: (hasFocus) =>
                                              onNoteEditorFocusChanged(
                                            hasFocus,
                                          ),
                                          onEditorCreated: () {
                                            // initialQuillEditor();
                                          },
                                          //     debugPrint('has focus $hasFocus'),

                                          onTextChanged: (value) {
                                            _exerciseNoteContent = value;
                                            debugPrint(value);
                                            print("Html Text is: $value");
                                          },
                                          onSelectionChanged: (sel) {},

                                          onEditingComplete: (value) {
                                            setState(() {
                                              FocusScope.of(context).unfocus();
                                            });
                                          },
                                          // onEditorCreated: () =>
                                          //     quillEditorController.embedImage(
                                          //         "https://upload.wikimedia.org/wikipedia/en/thumb/1/1a/King_logo.svg/440px-King_logo.svg.png"),
                                          // onEditingComplete: (s) =>
                                          //     debugPrint('Editing completed $s'),
                                          // onEditorResized: (height) =>
                                          //     debugPrint('Editor resized $height'),
                                          // onSelectionChanged: (sel) =>
                                          //     debugPrint('${sel.index},${sel.length}'),
                                          loadingBuilder: (context) {
                                            return const Center(
                                                child:
                                                    CircularProgressIndicator(
                                              strokeWidth: 0.4,
                                            ));
                                          },
                                        ))
                                      ],
                                    ),
                                  ),
                                )),
                          ],
                        ),
                      ),

                      const SizedBox(height: 50),
                      // Assign Worksheet
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const CustomText(
                            text: "Assign Worksheet",
                            weight: FontWeight.bold,
                          ),
                          const SizedBox(width: 175),
                          Expanded(
                            child: Container(
                              //margin: const EdgeInsets.only(top: 30),
                              child: (DropdownButton<String>(
                                value: chosenWorksheet == null
                                    ? null
                                    : (chosenWorksheet as Worksheet).id,
                                isExpanded: true,
                                onChanged: (String? worksheetId) {
                                  if (worksheetId != null) {
                                    chosenWorksheet = _listWorksheet!
                                        .firstWhere((worksheet) =>
                                            worksheet.id == worksheetId);
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                    _questionQuantity =
                                        (chosenWorksheet as Worksheet)
                                            .quizzes
                                            .length;
                                    _questionQuantityTextController.text =
                                        _questionQuantity.toString();
                                    _questionLimitQuantity = 1;
                                    setState(() {});
                                  }
                                },
                                hint:
                                    const CustomText(text: "Please Select One"),
                                items: worksheetsController.worksheetsObs
                                    .map<DropdownMenuItem<String>>(
                                      (Worksheet worksheet) =>
                                          DropdownMenuItem<String>(
                                        value: worksheet.id,
                                        child: Text(worksheet.title),
                                      ),
                                    )
                                    .toList(),
                              )),
                            ),
                          ),
                          Tooltip(
                            message: "This is an instruction",
                            child: IconButton(
                              hoverColor: Colors.transparent,
                              icon: const Icon(Icons.info),
                              onPressed: () {},
                            ),
                          )
                        ],
                      ),
                      // No. of Questions
                      // const Row(
                      //   children: [
                      //     CustomText(
                      //       text: "No. of Questions",
                      //       weight: FontWeight.bold,
                      //     ),
                      //     SizedBox(
                      //       width: 185,
                      //     ),
                      //     CustomText(text: "10"),
                      //   ],
                      // ),
                      const SizedBox(height: 30),
                      // Question Limit
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const CustomText(
                            text: "Question Limit",
                            weight: FontWeight.bold,
                          ),
                          const SizedBox(
                            width: 190,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              IconButton(
                                icon: const Icon(Icons.remove),
                                onPressed: _decreaseQuestionLimitQuantity,
                              ),
                              SizedBox(
                                width: 80,
                                child: TextField(
                                  textAlign: TextAlign.center,
                                  decoration: const InputDecoration(
                                    border: OutlineInputBorder(),
                                  ),
                                  keyboardType: TextInputType.number,
                                  // controller: TextEditingController(
                                  //     text:
                                  //         "$_questionLimitQuantity/$limitOfQuestion"),
                                  controller: TextEditingController(
                                      text: _questionLimitQuantity.toString()),
                                  onTap: () {},
                                  onChanged: (value) {
                                    // setState(() {
                                    _questionLimitQuantity =
                                        int.tryParse(value) ?? 0;
                                    if (_questionLimitQuantity >
                                        _questionQuantity) {
                                      _questionLimitQuantity =
                                          _questionQuantity;
                                    }
                                    // });
                                  },
                                  onEditingComplete: () {
                                    setState(() {
                                      FocusScope.of(context).unfocus();
                                    });
                                  },
                                  onTapOutside: (event) {
                                    setState(() {
                                      FocusScope.of(context).unfocus();
                                    });
                                  },
                                ),
                              ),
                              IconButton(
                                icon: const Icon(Icons.add),
                                onPressed: _increaseQuestionLimitQuantity,
                              ),
                            ],
                          ),
                          Tooltip(
                            message: "This is a instruction",
                            child: IconButton(
                              hoverColor: Colors.transparent,
                              icon: const Icon(Icons.info),
                              onPressed: () {},
                            ),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      // Number of Questions
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const CustomText(
                            text: "Number of Questions",
                            weight: FontWeight.bold,
                          ),
                          const SizedBox(
                            width: 130,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              // IconButton(
                              //   icon: const Icon(Icons.remove),
                              //   onPressed: _decreaseQuestionQuantity,
                              // ),
                              SizedBox(
                                width: 80,
                                child: TextField(
                                  textAlign: TextAlign.center,
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                  ),
                                  // keyboardType: TextInputType.number,
                                  controller: _questionQuantityTextController,
                                  readOnly: true,
                                  // onChanged: (value) {
                                  //   setState(() {
                                  //     // _questionQuantity =
                                  //     //     int.tryParse(value) ?? 1;
                                  //     _questionQuantity =
                                  //         (chosenWorksheet as Worksheet)
                                  //             .quizzes
                                  //             .length;
                                  //   });
                                  // },
                                ),
                              ),
                              // IconButton(
                              //   icon: const Icon(Icons.add),
                              //   onPressed: _increaseQuestionQuantity,
                              // ),
                            ],
                          ),
                          // IconButton(
                          //   hoverColor: Colors.transparent,
                          //   icon: const Icon(Icons.info),
                          //   onPressed: () {},
                          // )
                        ],
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      // Last Edited
                      Row(
                        children: [
                          const CustomText(
                            text: "Last Edited",
                            weight: FontWeight.bold,
                          ),
                          const SizedBox(
                            width: 225,
                          ),
                          CustomText(text: _lastEditedString),
                        ],
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.start,
                      //   children: [
                      //     const CustomText(
                      //       text: "Attached to Topic",
                      //       weight: FontWeight.bold,
                      //     ),
                      //     const SizedBox(
                      //       width: 180,
                      //     ),
                      //     Expanded(
                      //       child: CustomText(
                      //         // text:
                      //         //     "This is a title for a Topic name (uneditable)",
                      //         text: _attachedTopicTitle,
                      //         color: Colors.grey,
                      //       ),
                      //     ),
                      //   ],
                      // )
                    ],
                  ),
                )),
            Expanded(
                flex: 1,
                child: Column(
                  children: [
                    Row(
                      children: [
                        CustomText(
                          text: exercisesController.updateExerciseStatus.value
                              ? "Status: PUBLISHED"
                              : "Status: DRAFT",
                          weight: FontWeight.bold,
                        ),
                        Expanded(child: Container()),
                        Tooltip(
                          message: "This is a instruction",
                          child: IconButton(
                            hoverColor: Colors.transparent,
                            icon: const Icon(Icons.info),
                            onPressed: () {},
                          ),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 150,
                          height: 50,
                          child: OutlinedButton(
                            onPressed: () {
                              // Add your button action here
                              _saveDraft();
                            },
                            child: const CustomText(text: 'Save Draft'),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        SizedBox(
                          width: 120,
                          height: 50,
                          child: OutlinedButton(
                            onPressed: () {
                              // Add your button action here
                              _saveDraft();
                            },
                            child: const CustomText(text: 'Confirm'),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 40,
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const CustomText(
                          text: "Pass Limit %",
                          weight: FontWeight.bold,
                        ),
                        Expanded(child: Container()),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            IconButton(
                              icon: const Icon(Icons.remove),
                              onPressed: _decreasePassLimit,
                            ),
                            SizedBox(
                              width: 60,
                              child: TextField(
                                textAlign: TextAlign.center,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                ),
                                keyboardType: TextInputType.number,
                                controller: TextEditingController(
                                    text: _passLimit.toString()),
                                onChanged: (value) {
                                  _passLimit = int.tryParse(value) ?? 0;
                                  if (_passLimit > 100) {
                                    _passLimit = 100;
                                  }
                                },
                                onEditingComplete: () {
                                  setState(() {
                                    FocusScope.of(context).unfocus();
                                  });
                                },
                                onTapOutside: (event) {
                                  setState(() {
                                    FocusScope.of(context).unfocus();
                                  });
                                },
                              ),
                            ),
                            IconButton(
                              icon: const Icon(Icons.add),
                              onPressed: _increasePassLimit,
                            ),
                          ],
                        ),
                        Tooltip(
                          message: "This is a instruction",
                          child: IconButton(
                            hoverColor: Colors.transparent,
                            icon: const Icon(Icons.info),
                            onPressed: () {},
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const CustomText(
                          text: "Timer (minute)",
                          weight: FontWeight.bold,
                        ),
                        Expanded(child: Container()),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            IconButton(
                              icon: const Icon(Icons.remove),
                              onPressed: _decreaseTimer,
                            ),
                            SizedBox(
                              width: 60,
                              child: TextField(
                                textAlign: TextAlign.center,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                ),
                                keyboardType: TextInputType.number,
                                controller: TextEditingController(
                                    text:
                                        _timer > 0 ? _timer.toString() : "---"),
                                onChanged: (value) {
                                  _timer = int.tryParse(value) ?? 0;
                                },
                                onEditingComplete: () {
                                  setState(() {
                                    FocusScope.of(context).unfocus();
                                  });
                                },
                                onTapOutside: (event) {
                                  setState(() {
                                    FocusScope.of(context).unfocus();
                                  });
                                },
                              ),
                            ),
                            IconButton(
                              icon: const Icon(Icons.add),
                              onPressed: _increaseTimer,
                            ),
                          ],
                        ),
                        Tooltip(
                          message: "This is a instruction",
                          child: IconButton(
                            hoverColor: Colors.transparent,
                            icon: const Icon(Icons.info),
                            onPressed: () {},
                          ),
                        )
                      ],
                    ),

                    const SizedBox(
                      height: 40,
                    ),
                    // Action Needed
                    DottedBorder(
                      borderType: BorderType.Rect,
                      color: Colors.black,
                      strokeWidth: 2,
                      dashPattern: const [5, 5],
                      child: SizedBox(
                        width: 400,
                        height: 400,
                        child: ListView(
                          children: const [
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: CustomText(
                                text: "Action needed",
                                weight: FontWeight.bold,
                              ),
                            ),
                            ListTile(
                              leading: Icon(Icons.circle, size: 20),
                              title: Text(
                                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."),
                            ),
                            ListTile(
                              leading: Icon(Icons.circle, size: 20),
                              title: Text('Item 2'),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                )),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        const Divider(color: Colors.grey, thickness: 2),
        Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              OutlinedButton(
                onPressed: () {
                  exercisesController.changeAddNewStatusTo(false);
                },
                child: Container(
                    padding: const EdgeInsets.all(8.0),
                    width: 100,
                    height: 40,
                    child: const Align(
                        alignment: Alignment.center,
                        child: CustomText(text: "Back"))),
              )
            ])
      ],
    );
  }

  Widget _buildToolbar() {
    return Container(
      padding: const EdgeInsets.all(8.0),
      color: light,
      width: 800,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
            hoverColor: Colors.transparent,
            icon: const Icon(Icons.format_bold),
            onPressed: () {
              setState(() {
                _isBold = !_isBold;
              });
            },
            color: _isBold ? Colors.blue : Colors.black,
          ),
          IconButton(
            hoverColor: Colors.transparent,
            icon: const Icon(Icons.format_underlined),
            onPressed: () {
              setState(() {
                _isUnderlined = !_isUnderlined;
              });
            },
            color: _isUnderlined ? Colors.blue : Colors.black,
          ),
          IconButton(
            hoverColor: Colors.transparent,
            icon: const Icon(Icons.format_italic),
            onPressed: () {
              setState(() {
                _isItalic = !_isItalic;
              });
            },
            color: _isItalic ? Colors.blue : Colors.black,
          ),
          IconButton(
            hoverColor: Colors.transparent,
            icon: const Icon(Icons.attach_file_outlined),
            onPressed: () {},
            color: Colors.black,
          ),
          IconButton(
            hoverColor: Colors.transparent,
            icon: const Icon(Icons.format_list_numbered),
            onPressed: () {},
            color: Colors.black,
          ),
          IconButton(
            hoverColor: Colors.transparent,
            icon: const Icon(Icons.format_list_bulleted),
            onPressed: () {},
            color: Colors.black,
          ),
          IconButton(
            hoverColor: Colors.transparent,
            icon: const Icon(Icons.image),
            onPressed: () {},
            color: Colors.black,
          ),
          IconButton(
            hoverColor: Colors.transparent,
            icon: const Icon(Icons.format_align_left),
            onPressed: () {
              setState(() {
                _isAlignLeft = !_isAlignLeft;
              });
            },
            color: _isAlignLeft ? Colors.blue : Colors.black,
          ),
          IconButton(
            hoverColor: Colors.transparent,
            icon: const Icon(Icons.format_align_center),
            onPressed: () {
              setState(() {
                _isAlignCenter = !_isAlignCenter;
              });
            },
            color: _isAlignCenter ? Colors.blue : Colors.black,
          ),
          IconButton(
            hoverColor: Colors.transparent,
            icon: const Icon(Icons.format_align_right),
            onPressed: () {
              setState(() {
                _isAlignRight = !_isAlignRight;
              });
            },
            color: _isAlignRight ? Colors.blue : Colors.black,
          ),
        ],
      ),
    );
  }
}
