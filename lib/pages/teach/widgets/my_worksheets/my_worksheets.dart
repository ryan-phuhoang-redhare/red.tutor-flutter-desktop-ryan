import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_worksheets/widgets/add_new_worksheet.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_worksheets/widgets/worksheet_table.dart';

class MyWorksheetsTab extends StatelessWidget {
  const MyWorksheetsTab({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() => Expanded(
          child: ListView(children: [
            if (worksheetsController.addNewStatus.value == false)
              const WorksheetsTable()
            else
              const AddNewWorksheetPage()
          ]),
        ));
  }
}
