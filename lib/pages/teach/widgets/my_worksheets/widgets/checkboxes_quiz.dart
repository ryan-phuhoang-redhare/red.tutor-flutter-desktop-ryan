import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/answer_option.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/quiz.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

class CheckboxesQuiz extends StatefulWidget {
  final Quiz quiz;
  const CheckboxesQuiz({super.key, required this.quiz});

  @override
  State<CheckboxesQuiz> createState() => _CheckboxesQuizState();
}

class _CheckboxesQuizState extends State<CheckboxesQuiz> {
  handleCheckedAnswerOption(int index, int optionIndex, bool isChecked) {
    var optionsUpdated = widget.quiz.options;
    AnswerOption optionSelected = optionsUpdated[optionIndex];
    optionSelected.isRightAnswer = isChecked;
    optionsUpdated[optionIndex] = optionSelected;
    setState(() {
      widget.quiz.options = optionsUpdated;
    });
  }

  Text _updateCharacterCount(String text, int maxLength) {
    final currentLength = text.length;
    final remaining = maxLength - currentLength;
    return Text(
      'Remaining characters: $remaining/$maxLength',
      style: TextStyle(color: remaining < 0 ? Colors.red : Colors.grey),
    );
  }

  void removeAnswerOption(int currentIndex) {
    if (widget.quiz.options.length > 1) {
      var optionUpdated = widget.quiz.options;
      optionUpdated.removeAt(currentIndex);
      resetAnswerOptionIndex(currentIndex);
    }
  }

  resetAnswerOptionIndex(int quizIndex) {
    setState(() {
      int index = 0;
      widget.quiz.options.forEach((element) {
        element.index = index;
        index++;
      });
    });
  }

  void addAnswerOption(int currentIndex) {
    Quiz selectedQuiz = widget.quiz;
    var newOptions = selectedQuiz.options;
    AnswerOption newOption = AnswerOption(
        quizIndex: newOptions.length,
        index: newOptions.length,
        isRightAnswer: false,
        answerDescription: '');
    AnswerOption? isExistedObj = newOptions
        .firstWhereOrNull((element) => element.index == newOption.index);
    if (isExistedObj == null) {
      newOptions.add(newOption);
      selectedQuiz.options = newOptions;
    }
    resetAnswerOptionIndex(currentIndex);
  }

  void reOrderOption(int oldIndex, int newIndex, int quizIndex) {
    Quiz selectedQuiz = widget.quiz;
    if (oldIndex < newIndex) {
      newIndex -= 1;
    }
    var optionsUpdated = selectedQuiz.options;

    final item = optionsUpdated.removeAt(oldIndex);
    optionsUpdated.insert(newIndex, item);
    // setState(() {
    //   quizzes[quizIndex] = selectedQuiz;
    // });
    resetAnswerOptionIndex(quizIndex);
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      // Calculate the desired height based on the number of options
      double desiredHeight = 0;
      if (widget.quiz.options.length <= 4) {
        desiredHeight = 180 + widget.quiz.options.length * 70.0;
      } else {
        desiredHeight = 180 + 4 * 50.0;
      }
      return // Multiple-choice options
          Container(
        padding: const EdgeInsets.all(16.0),
        height: desiredHeight,
        child: ReorderableListView(
          buildDefaultDragHandles: false,
          children: widget.quiz.options.asMap().entries.map((entry) {
            final int optionIndex = entry.key;
            // final AnswerOption answerOption = entry.value;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              key: ValueKey(optionIndex),
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ReorderableDragStartListener(
                      index: optionIndex,
                      child: const Icon(Icons
                          .drag_indicator_outlined), // Reorder icon on the left
                    ),
                    Checkbox(
                      value: widget.quiz.options[optionIndex]
                          .isRightAnswer, // Update this with your logic
                      onChanged: (value) {
                        handleCheckedAnswerOption(
                            widget.quiz.index, optionIndex, value!);
                      },
                    ),
                    Expanded(
                      child: ListTile(
                          title: TextFormField(
                            controller:
                                widget.quiz.options[optionIndex].textController,
                            onChanged: (text) {
                              setState(() {
                                widget.quiz.options[optionIndex]
                                    .answerDescription = text;
                                widget.quiz.options[optionIndex]
                                        .characterAnswerText =
                                    _updateCharacterCount(text, 100);
                              });
                            },
                            decoration: const InputDecoration(
                              hintText: 'Question Answer Text...',
                              border: OutlineInputBorder(),
                            ),
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(
                                  100), // Limit to 100 characters
                            ],
                          ),
                          subtitle: Align(
                            alignment: Alignment.bottomRight,
                            child: widget
                                .quiz.options[optionIndex].characterAnswerText,
                          )),
                    ),
                    Tooltip(
                      message: "Remove",
                      child: IconButton(
                        onPressed: () {
                          removeAnswerOption(optionIndex);
                        },
                        icon: const Icon(Icons.remove_circle),
                        iconSize: 30,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                if (optionIndex == widget.quiz.options.length - 1)
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Tooltip(
                        message: "Add more option",
                        child: OutlinedButton(
                          onPressed: () {
                            addAnswerOption(widget.quiz.index);
                          },
                          child: const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Icon(Icons.add_circle),
                                SizedBox(
                                  width: 10,
                                ),
                                CustomText(text: "Add option"),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
              ],
            );
          }).toList(),
          onReorder: (oldIndex, newIndex) {
            reOrderOption(oldIndex, newIndex, widget.quiz.index);
          },
        ),
      );
    });
  }
}
