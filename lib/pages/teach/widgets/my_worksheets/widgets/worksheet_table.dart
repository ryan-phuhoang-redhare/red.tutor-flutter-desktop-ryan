import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/constants/style.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

class WorksheetsTable extends StatefulWidget {
  const WorksheetsTable({super.key});

  @override
  State<WorksheetsTable> createState() => _WorksheetsTableState();
}

class _WorksheetsTableState extends State<WorksheetsTable> {
  @override
  void initState() {
    super.initState();
  }

  Future<void> saveSelectedWorksheet(int index) async {
    var worksheetSelected = worksheetsController.worksheetsObs[index];
    await worksheetsController.selectedWorksheet(worksheetSelected);
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: active.withOpacity(.4), width: .5),
            boxShadow: [
              BoxShadow(
                  offset: const Offset(0, 6),
                  color: lightGrey.withOpacity(.1),
                  blurRadius: 12)
            ],
            borderRadius: BorderRadius.circular(8),
          ),
          padding: const EdgeInsets.all(16),
          margin: const EdgeInsets.only(bottom: 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  const SizedBox(
                    width: 10,
                  ),
                  CustomText(
                    text:
                        "Published (${worksheetsController.worksheetsObs.length})",
                    color: dark,
                    weight: FontWeight.bold,
                  ),
                  // Container(
                  //   width: 1,
                  //   height: 22,
                  //   color: dark,
                  //   margin: const EdgeInsets.only(left: 10, right: 10),
                  // ),
                  // CustomText(
                  //   text: "Draft (3)",
                  //   color: lightGrey,
                  //   weight: FontWeight.bold,
                  // ),
                  const SizedBox(
                    width: 50,
                  ),
                  SizedBox(
                    width: 150,
                    height: 50,
                    child: OutlinedButton.icon(
                        onPressed: () {
                          worksheetsController.changeAddNewStatusTo(true);
                          // worksheetsController.changeUpdateWorksheetStatusTo(false);
                        },
                        icon: Icon(
                          Icons.add,
                          color: dark,
                        ),
                        label: const CustomText(text: "Add New")),
                  ),
                  Expanded(child: Container()),
                  // const Expanded(
                  //   child: TextField(
                  //     decoration: InputDecoration(
                  //       hintText: 'Search...',
                  //     ),
                  //   ),
                  // ),
                  // IconButton(
                  //   onPressed: () {},
                  //   icon: const Icon(Icons.search),
                  // ),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              // 56: dataRowHeight
              // 7: number of data each page
              // 40: headingRowHeight
              SizedBox(
                height: (100 * 7) + 40,
                child: DataTable2(
                  columnSpacing: 12,
                  dataRowHeight: 100,
                  headingRowHeight: 40,
                  horizontalMargin: 12,
                  minWidth: 600,
                  showCheckboxColumn: false,
                  columns: [
                    const DataColumn2(
                        label: CustomText(
                          text: "Worksheet Title",
                          weight: FontWeight.bold,
                        ),
                        size: ColumnSize.L),
                    DataColumn2(
                      label: Row(
                        children: [
                          const CustomText(
                              text: 'Last Edited', weight: FontWeight.bold),
                          // IconButton(
                          //   hoverColor: Colors.transparent,
                          //   icon: const Icon(Icons.arrow_upward),
                          //   onPressed: () {},
                          // )
                        ],
                      ),
                      size: ColumnSize.M,
                    ),
                    const DataColumn2(
                      label: CustomText(
                        text: 'Qns',
                        weight: FontWeight.bold,
                      ),
                      size: ColumnSize.S,
                    ),
                    const DataColumn2(
                      label: CustomText(
                          text: 'Average Review %', weight: FontWeight.bold),
                      size: ColumnSize.M,
                    ),
                    const DataColumn2(
                        label: CustomText(
                          text: "New Comments",
                          weight: FontWeight.bold,
                        ),
                        size: ColumnSize.S),
                    DataColumn2(
                      label: Row(
                        children: [
                          const CustomText(
                              text: 'Status', weight: FontWeight.bold),
                          // IconButton(
                          //   hoverColor: Colors.transparent,
                          //   icon: const Icon(Icons.arrow_upward),
                          //   onPressed: () {},
                          // )
                        ],
                      ),
                      size: ColumnSize.M,
                    ),
                    const DataColumn2(
                      label: CustomText(text: ''),
                      size: ColumnSize.M,
                    ),
                  ],
                  rows: List<DataRow>.generate(
                      worksheetsController.worksheetsObs.length, (index) {
                    var worksheet = worksheetsController.worksheetsObs[index];
                    var timemiliseconds = DateTime.fromMillisecondsSinceEpoch(
                        worksheet.lastEdited);
                    String lastEdtiedformattedDate =
                        DateFormat('yyyy-MM-dd').format(timemiliseconds);
                    return DataRow(
                      cells: [
                        // Worksheet title
                        DataCell(CustomText(text: worksheet.title)),
                        // Last Edited
                        DataCell(CustomText(text: lastEdtiedformattedDate)),
                        // Qns
                        DataCell(CustomText(
                            text: worksheet.quizzes.length.toString())),
                        // Average Review %
                        const DataCell(CustomText(text: "--, --, --, --")),
                        // New Comments
                        const DataCell(CustomText(text: "--")),
                        // Status
                        DataCell(CustomText(text: worksheet.status)),
                        // Action button
                        DataCell(Row(
                          children: [
                            // IconButton(
                            //   hoverColor: Colors.transparent,
                            //   icon: const Icon(Icons.copy),
                            //   onPressed: () {},
                            // ),
                          ],
                        )),
                      ],
                      onSelectChanged: (isSelected) {
                        // Handle row click action here
                        saveSelectedWorksheet(index);
                        setState(() {
                          worksheetsController.changeAddNewStatusTo(true);
                          worksheetsController.changeUpdateStatusTo(true);
                        });
                      },
                    );
                  }),
                ),
              ),
            ],
          ),
        ));
  }
}
