import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:flutter_quill_extensions/flutter_quill_extensions.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/models/request/worksheet/option_request.dart';
import 'package:redtutor_flutter_webapp/models/request/worksheet/quiz_request.dart';
import 'package:redtutor_flutter_webapp/models/request/worksheet/worksheet_request.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/answer_option.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/quiz.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/quiz_type.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/worksheet.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_worksheets/widgets/checkboxes_quiz.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_worksheets/widgets/multiple_choice_quiz.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

class AddNewWorksheetPage extends StatefulWidget {
  const AddNewWorksheetPage({super.key});

  @override
  State<AddNewWorksheetPage> createState() => _AddNewWorksheetPageState();
}

class _AddNewWorksheetPageState extends State<AddNewWorksheetPage> {
  final TextEditingController _titleTextController = TextEditingController();
  Text _characterCountTitleText = const Text(
    'Remaining characters: 100/100',
    style: TextStyle(color: Colors.grey),
  );
  bool isShuffleQues = false;

  // List of dropdown items
  List<QuizType> typeOfQuiz = [
    QuizType(name: 'Multiple Choice', value: "MULTIPLE_CHOICE"),
    QuizType(name: 'Checkboxes', value: "CHECKBOXES"),
  ]; // Initial selected option

  dynamic worksheetSelected;

  @override
  void initState() {
    if (worksheetsController.updateStatus.value) {
      loadWorksheetSelected();
    } else {
      worksheetsController.initialQuizPool();
      setState(() {
        isShuffleQues = false;
        _titleTextController.text = '';
      });
    }
    super.initState();
  }

  void loadWorksheetSelected() async {
    Worksheet? retrievedWorksheetSelected =
        await worksheetsController.loadWorksheetSelectedFromSharedPreferences();
    await worksheetsController
        .loadQuizzesByWorksheetId(retrievedWorksheetSelected!.id);
    // List<Quiz> quizzes = [];
    if (retrievedWorksheetSelected.id != null) {
      _updateTitleCharacterCount(retrievedWorksheetSelected.title);
      setState(() {
        isShuffleQues = retrievedWorksheetSelected.isShuffleEnabled;
        _titleTextController.text = retrievedWorksheetSelected.title;
        worksheetSelected = retrievedWorksheetSelected;
      });
    }
  }

  void onChangedQuestionDesription(String htmlText, int quizIndex) {
    setState(() {
      String questionDescription =
          worksheetsController.quizPool[quizIndex].questionDescription;
      bool showHintQuestionText =
          worksheetsController.quizPool[quizIndex].showHintQuestionText;
      if (htmlText != "") {
        questionDescription = htmlText;
      }
      String plainText =
          worksheetsController.htmlToPlainText(questionDescription);
      if (htmlText != "" && showHintQuestionText) {
        worksheetsController.quizPool[quizIndex].showHintQuestionText = false;
      } else if (htmlText == "") {
        worksheetsController.quizPool[quizIndex].showHintQuestionText = true;
      }
      worksheetsController.quizPool[quizIndex].questionDescription =
          questionDescription;
      worksheetsController
              .quizPool[quizIndex].characterQuestionDescriptionText =
          worksheetsController.getValidateCharacterCount(plainText, 200);
    });
  }

  void _saveDraft() {
    List<QuizRequest> quizResquests = [];
    int index = 0;
    worksheetsController.quizPool.forEach((quiz) {
      List<OptionRequest> optionRequests = [];
      quiz.options.forEach((option) {
        OptionRequest optRes = OptionRequest(
            index: option.index,
            description: option.answerDescription,
            isChecked: option.isRightAnswer);
        optionRequests.add(optRes);
      });
      QuizRequest quizRequest = QuizRequest(
          id: quiz.id,
          question: quiz.questionDescription,
          type: quiz.type,
          options: optionRequests,
          rightAnswer: quiz.rightAnswer,
          explanation: quiz.explanationController.text,
          hint: quiz.hintController.text,
          index: index,
          creatorId: "",
          isShuffleAnswer: quiz.isShuffleAnswer);
      if (quiz.type == "MULTIPLE_CHOICE") {
        int optionChecked = optionRequests
            .firstWhere((option) => option.isChecked == true)
            .index;
        quizRequest.rightAnswer = optionChecked;
      } else if (quiz.type == "CHECKBOXES") {
        List<OptionRequest> optionsChecked =
            optionRequests.where((option) => option.isChecked == true).toList();
        List<int> convertToIndexes =
            optionsChecked.map((e) => e.index).toList();
        quizRequest.rightAnswer = convertToIndexes;
      }
      quizResquests.add(quizRequest);
      index++;
    });

    WorksheetRequest worksheetRequest = WorksheetRequest(
        title: _titleTextController.text,
        isShuffleEnabled: isShuffleQues,
        addNewQuizRequests: quizResquests);
    if (!worksheetsController.updateStatus.value) {
      worksheetsController.createWorksheet(worksheetRequest);
    } else {
      worksheetRequest.id = worksheetSelected.id;
      worksheetsController.updateWorksheet(worksheetRequest);
    }
  }

  _updateTitleCharacterCount(String titleText) {
    setState(() {
      _characterCountTitleText =
          worksheetsController.getValidateCharacterCount(titleText, 100);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                flex: 4,
                child: Container(
                  margin: const EdgeInsets.only(right: 50),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // Worksheet Title
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const CustomText(
                            text: "Worksheet Title",
                            weight: FontWeight.bold,
                          ),
                          Row(
                            children: [
                              const CustomText(text: "Shuffle Questions"),
                              Switch(
                                  // This bool value toggles the switch.
                                  value: isShuffleQues,
                                  activeColor: Colors.blueAccent,
                                  onChanged: (bool value) {
                                    // This is called when the user toggles the switch.
                                    setState(() {
                                      isShuffleQues = value;
                                    });
                                  }),
                            ],
                          )
                        ],
                      ),
                      TextFormField(
                        controller: _titleTextController,
                        onChanged: (text) {
                          // Update character count
                          _updateTitleCharacterCount(text);
                        },
                        decoration: const InputDecoration(
                          hintText: 'Worksheet Title Text...',
                          border: OutlineInputBorder(),
                        ),
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(
                              100), // Limit to 100 characters
                        ],
                      ),
                      Align(
                          alignment: Alignment.centerRight,
                          child: _characterCountTitleText),
                      const SizedBox(height: 30),
                      // Questions
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const CustomText(
                            text: "Questions",
                            weight: FontWeight.bold,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          // Obx(() {
                          //   return SizedBox(
                          //     width: MediaQuery.of(context).size.width,
                          //     child: ListView.builder(
                          //         shrinkWrap: true,
                          //         itemCount:
                          //             worksheetsController.quizPool.length,
                          //         itemBuilder:
                          //             (BuildContext context, int index) {
                          //           return _questionItemWidget(index);
                          //         }),
                          //   );
                          // }),
                          Obx(() {
                            return SizedBox(
                                width: MediaQuery.of(context).size.width,
                                height:
                                    worksheetsController.quizPool.length * 850,
                                child: LayoutBuilder(
                                    builder: (context, constraints) {
                                  double containerWidth = constraints
                                      .maxWidth; // Get the available width
                                  double desiredWidth =
                                      630.0; // Your desired width

                                  // Ensure the container width is not smaller than the desired width
                                  if (containerWidth < desiredWidth) {
                                    containerWidth = desiredWidth;
                                  }
                                  return Obx(() {
                                    return Expanded(
                                      child: Container(
                                        padding: const EdgeInsets.all(16.0),
                                        width: containerWidth,
                                        child: ConstrainedBox(
                                          constraints: BoxConstraints(
                                              minHeight: worksheetsController
                                                      .quizPool.length *
                                                  850),
                                          child: ReorderableListView(
                                            physics:
                                                const ClampingScrollPhysics(),
                                            buildDefaultDragHandles: false,
                                            children: worksheetsController
                                                .quizPool
                                                .map((quiz) {
                                              final int optionIndex =
                                                  quiz.index;
                                              return ReorderableDragStartListener(
                                                key: ValueKey(optionIndex),
                                                index: optionIndex,
                                                child: Container(
                                                  margin: const EdgeInsets.only(
                                                      top: 20),
                                                  decoration: BoxDecoration(
                                                    color: Colors.amber,
                                                    border: Border.all(
                                                      color: Colors.grey,
                                                      width: 2.0,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10.0),
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      Transform.rotate(
                                                        angle: 1.5708,
                                                        child: const Icon(Icons
                                                            .drag_indicator),
                                                      ),
                                                      _questionItemWidget(
                                                          optionIndex)
                                                    ],
                                                  ),
                                                ),
                                              );
                                            }).toList(),
                                            onReorder: (oldIndex, newIndex) {
                                              worksheetsController
                                                  .reOrderQuizOption(
                                                      oldIndex, newIndex);
                                            },
                                          ),
                                        ),
                                      ),
                                    );
                                  });
                                }));
                          }),

                          const SizedBox(
                            height: 20,
                          ),
                          TextButton(
                              onPressed: () {
                                worksheetsController.addNewQuestion();
                              },
                              child: const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.add_circle),
                                    SizedBox(width: 10),
                                    CustomText(text: "Add new question"),
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ],
                  ),
                )),
            Expanded(
                flex: 1,
                child: Column(
                  children: [
                    Row(
                      children: [
                        CustomText(
                          text: worksheetsController.updateStatus.value
                              ? "Status: Published"
                              : "Status: DRAFT",
                          weight: FontWeight.bold,
                        ),
                        Expanded(child: Container()),
                        Tooltip(
                          message: "This is a instruction",
                          child: IconButton(
                            hoverColor: Colors.transparent,
                            icon: const Icon(Icons.info),
                            onPressed: () {},
                          ),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 150,
                          height: 50,
                          child: OutlinedButton(
                            onPressed: () {
                              // Add your button action here
                              _saveDraft();
                            },
                            child: const CustomText(text: 'Save Draft'),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        SizedBox(
                          width: 120,
                          height: 50,
                          child: OutlinedButton(
                            onPressed: () {
                              // Add your button action here
                              _saveDraft();
                            },
                            child: const CustomText(text: 'Confirm'),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    // Avg Review
                    const Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              CustomText(text: 'Avg. Proper Writing'),
                              CustomText(text: '---'),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              CustomText(text: 'Avg. Relevant Content'),
                              CustomText(text: '---'),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              CustomText(text: 'Avg. Accurate Answer'),
                              CustomText(text: '---'),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              CustomText(text: 'Avg. Suitable Difficulty'),
                              CustomText(text: '---'),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    // Action Nedded
                    DottedBorder(
                      borderType: BorderType.Rect,
                      color: Colors.black,
                      strokeWidth: 2,
                      dashPattern: const [5, 5],
                      child: SizedBox(
                        width: 400,
                        height: 400,
                        child: ListView(
                          children: const [
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: CustomText(
                                text: "Action needed",
                                weight: FontWeight.bold,
                              ),
                            ),
                            ListTile(
                              leading: Icon(Icons.circle, size: 20),
                              title: Text(
                                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."),
                            ),
                            ListTile(
                              leading: Icon(Icons.circle, size: 20),
                              title: Text('Item 2'),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                )),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        const Divider(color: Colors.grey, thickness: 2),
        Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              OutlinedButton(
                onPressed: () {
                  worksheetsController.changeAddNewStatusTo(false);
                  worksheetsController.changeUpdateStatusTo(false);
                },
                child: Container(
                    padding: const EdgeInsets.all(8.0),
                    width: 100,
                    height: 40,
                    child: const Align(
                        alignment: Alignment.center,
                        child: CustomText(text: "Back"))),
              )
            ]),
      ],
    );
  }

  Widget _questionItemWidget(int quizIndex) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: Colors.grey,
          width: 2.0,
        ),
        borderRadius: BorderRadius.circular(10.0),
      ),
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        key: ValueKey(worksheetsController.quizPool[quizIndex]),
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Spacer(),
              Padding(
                padding: const EdgeInsets.only(right: 50),
                child: Row(
                  children: [
                    const CustomText(text: "Shuffle Answers"),
                    Switch(
                        // This bool value toggles the switch.
                        value: worksheetsController
                            .quizPool[quizIndex].isShuffleAnswer,
                        activeColor: Colors.blueAccent,
                        onChanged: (bool value) {
                          // This is called when the user toggles the switch.
                          setState(() {
                            worksheetsController
                                .quizPool[quizIndex].isShuffleAnswer = value;
                          });
                        }),
                    // onChanged: null),
                  ],
                ),
              ),
              DropdownButton<String>(
                value: worksheetsController.quizPool[quizIndex].type,
                items: typeOfQuiz.map((QuizType quizType) {
                  return DropdownMenuItem<String>(
                    value: quizType.value,
                    child: Text(quizType.name),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    worksheetsController.quizPool[quizIndex].type = value!;
                  });
                  worksheetsController.clearAnswer(quizIndex);
                },
              ),
              const SizedBox(
                width: 20,
              ),
              Row(
                children: [
                  Tooltip(
                    message: "Delete question",
                    child: IconButton(
                      icon: const Icon(Icons.delete),
                      onPressed: () {
                        setState(() {
                          if (worksheetsController.quizPool.length > 1) {
                            worksheetsController.quizPool.removeAt(quizIndex);
                          }
                        });
                      },
                    ),
                  ),
                  Tooltip(
                    message: "Duplicate question",
                    child: IconButton(
                        icon: const Icon(Icons.copy),
                        onPressed: () {
                          setState(() {
                            Quiz quizSelected =
                                worksheetsController.quizPool[quizIndex];
                            List<AnswerOption> options = [];
                            quizSelected.options.forEach((option) {
                              AnswerOption dupAns = AnswerOption(
                                  quizIndex: option.quizIndex,
                                  index: option.index,
                                  answerDescription: option.answerDescription,
                                  isRightAnswer: option.isRightAnswer);
                              dupAns.textController = TextEditingController(
                                  text: option.answerDescription);
                              options.add(dupAns);
                            });
                            Quiz quizDuplicated = Quiz(
                                index: quizSelected.index + 1,
                                questionDescription:
                                    quizSelected.questionDescription,
                                options: options,
                                rightAnswer: 0,
                                hint: quizSelected.hint,
                                explanation: quizSelected.explanation,
                                type: quizSelected.type,
                                isShuffleAnswer: quizSelected.isShuffleAnswer);
                            quizDuplicated.explanationController.text =
                                quizDuplicated.explanation;
                            quizDuplicated.hintController.text =
                                quizDuplicated.hint;
                            worksheetsController.quizPool.add(quizDuplicated);
                          });
                        }),
                    // onPressed: null),
                  ),
                ],
              )
            ],
          ),
          QuillProvider(
            configurations: QuillConfigurations(
              controller: worksheetsController
                  .quizPool[quizIndex].quillEditorController,
              sharedConfigurations: const QuillSharedConfigurations(
                locale: Locale('en'),
              ),
            ),
            child: IntrinsicHeight(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  QuillBaseToolbar(
                      configurations: QuillBaseToolbarConfigurations(
                          toolbarIconCrossAlignment: WrapCrossAlignment.start,
                          toolbarIconAlignment: WrapAlignment.start,
                          childrenBuilder: (context) {
                            final controller = context.requireQuillController;
                            return [
                              QuillToolbarFontSizeButton(
                                  options:
                                      const QuillToolbarFontSizeButtonOptions(
                                          rawItemsMap: {"10": "10"}),
                                  controller: controller,
                                  defaultDisplayText: 'A'),
                              QuillToolbarToggleStyleButton(
                                attribute: Attribute.bold,
                                controller: controller,
                                options:
                                    const QuillToolbarToggleStyleButtonOptions(
                                  iconData: Icons.format_bold,
                                  iconSize: 20,
                                ),
                              ),
                              QuillToolbarToggleStyleButton(
                                attribute: Attribute.italic,
                                controller: controller,
                                options:
                                    const QuillToolbarToggleStyleButtonOptions(
                                  iconData: Icons.format_italic,
                                  iconSize: 20,
                                ),
                              ),
                              QuillToolbarToggleStyleButton(
                                attribute: Attribute.underline,
                                controller: controller,
                                options:
                                    const QuillToolbarToggleStyleButtonOptions(
                                  iconData: Icons.format_underline,
                                  iconSize: 20,
                                ),
                              ),
                              QuillToolbarToggleStyleButton(
                                attribute: Attribute.ol,
                                controller: controller,
                                options:
                                    const QuillToolbarToggleStyleButtonOptions(
                                  iconData: Icons.format_list_numbered,
                                  iconSize: 20,
                                ),
                              ),
                              QuillToolbarToggleStyleButton(
                                attribute: Attribute.ul,
                                controller: controller,
                                options:
                                    const QuillToolbarToggleStyleButtonOptions(
                                  iconData: Icons.format_list_bulleted,
                                  iconSize: 20,
                                ),
                              ),
                              QuillToolbarImageButton(
                                controller: controller,
                                options: QuillToolbarImageButtonOptions(
                                    imageButtonConfigurations:
                                        QuillToolbarImageConfigurations(
                                  onImageInsertCallback:
                                      (image, controller) async {
                                    // Create a map with the necessary structure
                                    final Map<String, dynamic> jsonMap = {
                                      "insert": {"image": image},
                                      "attributes": {
                                        "style": "width: 100; height: 100"
                                      }
                                    };
                                    // Extract image URL and attributes
                                    final String imageUrl =
                                        jsonMap['insert']['image'];
                                    final Map<String, dynamic> attributes =
                                        jsonMap['attributes'];
                                    // Create a Quill Delta
                                    final Delta delta = Delta()
                                      ..insert(
                                        {'image': imageUrl},
                                        attributes,
                                      );
                                    final TextSelection currentSelection =
                                        controller.selection;
                                    // Get the starting and ending offsets
                                    final int baseOffset =
                                        currentSelection.baseOffset;
                                    controller.compose(
                                        delta,
                                        TextSelection.collapsed(
                                            offset: baseOffset + 1),
                                        ChangeSource.local);
                                  },
                                )),
                              )
                            ];
                          })),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey, // Border color
                            width: 1.0, // Border thickness
                          ),
                          borderRadius: BorderRadius.circular(5.0)),
                      child: QuillEditor.basic(
                        configurations: QuillEditorConfigurations(
                          padding: const EdgeInsets.all(16),
                          readOnly: false,
                          embedBuilders:
                              FlutterQuillEmbeds.defaultEditorBuilders(),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          // ToolBar(
          //   toolBarConfig: const [
          //     ToolBarStyle.bold,
          //     ToolBarStyle.italic,
          //     ToolBarStyle.underline,
          //     ToolBarStyle.link,
          //     ToolBarStyle.listBullet,
          //     ToolBarStyle.listOrdered,
          //     ToolBarStyle.image,
          //   ],
          //   toolBarColor: Colors.cyan.shade50,
          //   activeIconColor: Colors.green,
          //   padding: const EdgeInsets.all(12),
          //   iconSize: 20,
          //   controller:
          //       worksheetsController.quizPool[quizIndex].quillEditorController,
          // ),
          // Container(
          //   decoration: const BoxDecoration(
          //     border: Border(
          //       left: BorderSide(
          //         color: Color(0xFFE0E0E0),
          //         width: 2,
          //       ),
          //       right: BorderSide(
          //         color: Color(0xFFE0E0E0),
          //         width: 2,
          //       ),
          //       bottom: BorderSide(
          //         color: Color(0xFFE0E0E0),
          //         width: 2,
          //       ),
          //     ),
          //   ),
          //   child: Column(
          //     children: [
          //       Stack(
          //         alignment: Alignment.center,
          //         children: [
          //           QuillHtmlEditor(
          //             hintText: 'Question Description Text',
          //             controller: worksheetsController
          //                 .quizPool[quizIndex].quillEditorController,
          //             isEnabled: true,
          //             minHeight: 50,
          //             autoFocus: false,
          //             padding: const EdgeInsets.only(left: 10, top: 5),
          //             onEditorCreated: () {
          //               worksheetsController
          //                   .quizPool[quizIndex].quillEditorController
          //                   .unFocus();
          //               if (worksheetsController
          //                       .quizPool[quizIndex].questionDescription !=
          //                   "") {
          //                 worksheetsController
          //                     .quizPool[quizIndex].quillEditorController
          //                     .setText(worksheetsController
          //                         .quizPool[quizIndex].questionDescription);
          //               }
          //             },
          //             // onTextChanged: onChangedQuestionDesription,
          //             onTextChanged: (htmlText) =>
          //                 {onChangedQuestionDesription(htmlText, quizIndex)},
          //             loadingBuilder: (context) {
          //               return const Center(
          //                   child: CircularProgressIndicator(
          //                 strokeWidth: 0.4,
          //               ));
          //             },
          //           ),
          //           if (worksheetsController
          //               .quizPool[quizIndex].showHintQuestionText)
          //             const Text(
          //               'Question Description Text...',
          //               style: TextStyle(color: Colors.grey, fontSize: 16),
          //             ),
          //         ],
          //       ),
          //       Align(
          //           alignment: Alignment.bottomRight,
          //           child: worksheetsController
          //               .quizPool[quizIndex].characterQuestionDescriptionText),
          //     ],
          //   ),
          // ),
          if (worksheetsController.quizPool[quizIndex].type == "CHECKBOXES")
            CheckboxesQuiz(quiz: worksheetsController.quizPool[quizIndex]),
          if (worksheetsController.quizPool[quizIndex].type ==
              "MULTIPLE_CHOICE")
            MultipleChoiceQuiz(quiz: worksheetsController.quizPool[quizIndex]),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Expanded(
                  flex: 1, child: CustomText(text: "Hint Description")),
              Expanded(
                flex: 7,
                child: Column(
                  children: [
                    TextFormField(
                      maxLines: null,
                      controller: worksheetsController
                          .quizPool[quizIndex].hintController,
                      onChanged: (text) {
                        setState(() {
                          worksheetsController.quizPool[quizIndex].hint = text;
                          worksheetsController
                                  .quizPool[quizIndex].characterHintText =
                              worksheetsController.getValidateCharacterCount(
                                  text, 200);
                        });
                      },
                      decoration: const InputDecoration(
                        hintText: 'Hint Description Text...',
                        border: OutlineInputBorder(),
                      ),
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(200),
                      ],
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: worksheetsController
                          .quizPool[quizIndex].characterHintText,
                    )
                  ],
                ),
              )
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Expanded(flex: 1, child: CustomText(text: "Explanation")),
              Expanded(
                  flex: 7,
                  child: Column(
                    children: [
                      TextFormField(
                        maxLines: null,
                        controller: worksheetsController
                            .quizPool[quizIndex].explanationController,
                        onChanged: (text) {
                          setState(() {
                            worksheetsController
                                .quizPool[quizIndex].explanation = text;
                            worksheetsController.quizPool[quizIndex]
                                    .characterExplanationText =
                                worksheetsController.getValidateCharacterCount(
                                    text, 200);
                          });
                        },
                        decoration: const InputDecoration(
                          hintText: 'Explain Description Text...',
                          border: OutlineInputBorder(),
                        ),
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(200),
                        ],
                      ),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: worksheetsController
                            .quizPool[quizIndex].characterExplanationText,
                      )
                    ],
                  ))
            ],
          )
        ],
      ),
    );
  }
}
