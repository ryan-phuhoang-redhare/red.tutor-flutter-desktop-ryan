import 'dart:async';
import 'dart:convert';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quill_html_editor/quill_html_editor.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/models/exercise/exercise.dart';
import 'package:redtutor_flutter_webapp/models/topic/topic.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

import 'dart:html' as html;

class AddNewTopicPage extends StatefulWidget {
  const AddNewTopicPage({super.key});

  @override
  State<AddNewTopicPage> createState() => _AddNewTopicPageState();
}

class _AddNewTopicPageState extends State<AddNewTopicPage> {
  final TextEditingController _titleTextController = TextEditingController();
  final QuillEditorController quillEditorController = QuillEditorController();
  Text _characterCountTitleText = const Text(
      'Remaining characters: 100/100'); // Initialize with the maximum limit
  List<String> exercisesOptions = [""];
  List<String> exerciseSelectedList = [""];
  Timer? timer;
  var errorMessage = '';
  dynamic topicSelectedObject = {};

  var isEnabledQuillHtmlEditor = false;
  var _topicDescription = "";
  List<int> uint8Image = [];
  String base64Data = '';
  String imageUrl = 'https://i.ibb.co/q1Kmvw2/default-image.png';
  @override
  void initState() {
    loadTopicSelected();
    super.initState();
  }

  initialQuillEditor() async {
    if (topicsController.updateTopicStatus.value) {
      await quillEditorController
          .setText(topicSelectedObject.description)
          .then((value) => print("quillEditorController: $value"))
          .catchError((error) {
        print("Error setting Quill editor content: $error");
      });
    }
  }

  void loadTopicSelected() async {
    if (topicsController.updateTopicStatus.value) {
      Topic? retrievedTopicSelected =
          await topicsController.loadTopicSelectedFromSharedPreferences();
      _titleTextController.text = retrievedTopicSelected!.title;
      _updateCharacterCount(retrievedTopicSelected.title);
      setState(() {
        topicSelectedObject = retrievedTopicSelected;
        _topicDescription = retrievedTopicSelected.description;
        exerciseSelectedList = retrievedTopicSelected.assignedExercises;
        exercisesOptions = retrievedTopicSelected.assignedExercises;
        imageUrl = retrievedTopicSelected.mediaResource.url;
      });
    } else {
      _titleTextController.text = "";
      setState(() {
        _topicDescription = "";
      });
    }
  }

  void _addExerciseOption(int number) {
    if (exercisesOptions.length < exercisesController.exercisesObs.length) {
      if (exerciseSelectedList[number] != '') {
        var exercisesOptionsUpdated = List<String>.from(exercisesOptions);
        var exerciseSelectedListUpdated =
            List<String>.from(exerciseSelectedList);
        exercisesOptionsUpdated.add("Exercise Option $number");
        exerciseSelectedListUpdated.add('');
        setState(() {
          exercisesOptions = exercisesOptionsUpdated;
          exerciseSelectedList = exerciseSelectedListUpdated;
        });
      } else {
        showError(
            'Please select the exercise first before wanting more options',
            const Duration(seconds: 3));
      }
    } else {
      // warning to user have run out of exercises to choose from
      showError('You have run out of exercises to choose from',
          const Duration(seconds: 3));
    }
  }

  void _removeExerciseOption(int exerciseIndex) {
    var exerciseSelectedListUpdated = exerciseSelectedList;
    if (exerciseSelectedListUpdated.length > exerciseIndex) {
      exerciseSelectedListUpdated.removeAt(exerciseIndex);
    }
    setState(() {
      exerciseSelectedList = exerciseSelectedListUpdated;
    });
  }

  void _updateCharacterCount(String text) {
    const maxLength = 100; // Change this to your desired character limit
    final currentLength = text.length;
    final remaining = maxLength - currentLength;

    setState(() {
      _characterCountTitleText =
          Text('Remaining characters: $remaining/$maxLength');
    });
  }

  void _saveDraft() async {
    Topic? retrievedTopicSelected =
        await topicsController.loadTopicSelectedFromSharedPreferences();
    String id = '';
    if (retrievedTopicSelected != null) {
      id = retrievedTopicSelected.id;
    }
    topicsController.saveDraft(id, _titleTextController.text, _topicDescription,
        exerciseSelectedList, base64Data);
  }

  void showError(String message, Duration duration) {
    setState(() {
      errorMessage = message;
    });

    timer = Timer(duration, () {
      setState(() {
        errorMessage = '';
      });
    });
  }

  void onChangedTopicDesription(String htmlText) {
    print("HTML Text: $htmlText");
    setState(() {
      _topicDescription = htmlText;
    });
  }

  void uploadImage() {
    final html.FileUploadInputElement input = html.FileUploadInputElement()
      ..accept = 'image/*'; // Limit file types to images

    input
        .click(); // Simulate a click on the input element to open the file picker

    input.onChange.listen((e) {
      final html.File file = input.files!.first;
      final html.FileReader reader = html.FileReader();

      reader.onLoadEnd.listen((e) {
        // Handle the uploaded image, e.g., send it to a server or display it

        Uint8List data = reader.result as Uint8List;
        String imageBase64Data = base64Encode(data);
        setState(() {
          uint8Image = data;
          base64Data = imageBase64Data;
        });
      });

      reader.readAsArrayBuffer(file);
    });
  }

  @override
  void dispose() {
    _titleTextController.dispose();
    timer?.cancel();
    quillEditorController.dispose();
    super.dispose();
  }

  void reOrderOption(int oldIndex, int newIndex) {
    var exerciseSelectedListUpdated = exerciseSelectedList;
    if (oldIndex < newIndex) {
      newIndex -= 1;
    }

    final item = exerciseSelectedListUpdated.removeAt(oldIndex);
    exerciseSelectedListUpdated.insert(newIndex, item);
    setState(() {
      exerciseSelectedList = exerciseSelectedListUpdated;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                flex: 4,
                child: Container(
                  width: double.infinity,
                  margin: const EdgeInsets.only(right: 50),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // Topic Title
                      Container(
                        margin: const EdgeInsets.only(top: 10, bottom: 10),
                        child: Column(
                          children: [
                            const Align(
                                alignment: Alignment.topLeft,
                                child: CustomText(
                                  text: "Topic Title",
                                  weight: FontWeight.bold,
                                )),
                            const SizedBox(
                              height: 10,
                            ),
                            TextFormField(
                              controller: _titleTextController,
                              onChanged: (text) {
                                // Update character count
                                _updateCharacterCount(text);
                              },
                              decoration: const InputDecoration(
                                hintText: 'Topic Title Text...',
                                border: OutlineInputBorder(),
                              ),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(
                                    100), // Limit to 100 characters
                              ],
                            ),
                            Align(
                                alignment: Alignment.centerRight,
                                child: _characterCountTitleText)
                          ],
                        ),
                      ),
                      const SizedBox(height: 30),
                      // Topic Description
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const CustomText(
                            text: "Topic Description",
                            weight: FontWeight.bold,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          ToolBar(
                            toolBarConfig: const [
                              ToolBarStyle.bold,
                              ToolBarStyle.italic,
                              ToolBarStyle.underline
                            ],
                            toolBarColor: Colors.cyan.shade50,
                            activeIconColor: Colors.green,
                            padding: const EdgeInsets.all(12),
                            iconSize: 20,
                            controller: quillEditorController,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                border:
                                    Border.all(color: Colors.white10, width: 2),
                                color: Colors.grey[100]),
                            child: QuillHtmlEditor(
                              hintText: 'Topic Description Text',
                              controller: quillEditorController,
                              isEnabled: true,
                              minHeight: 300,
                              autoFocus: false,
                              padding: const EdgeInsets.only(left: 10, top: 5),
                              onEditorCreated: () {
                                initialQuillEditor();
                              },
                              onTextChanged: onChangedTopicDesription,
                              loadingBuilder: (context) {
                                return const Center(
                                    child: CircularProgressIndicator(
                                  strokeWidth: 0.4,
                                ));
                              },
                            ),
                          )
                        ],
                      ),
                      const SizedBox(height: 30),
                      // Topic Image
                      Container(
                        margin: const EdgeInsets.only(top: 10, bottom: 10),
                        child: Row(
                          children: [
                            const CustomText(
                              text: "Topic Image",
                              weight: FontWeight.bold,
                            ),
                            const SizedBox(width: 200),
                            Container(
                              margin: const EdgeInsets.only(right: 50),
                              width: 200,
                              height: 200,
                              child: uint8Image.isEmpty
                                  ? Image.network(imageUrl)
                                  : Image.memory(
                                      Uint8List.fromList(uint8Image)),
                            ),
                            const SizedBox(width: 10),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ElevatedButton(
                                  onPressed: () {
                                    // Add your button action here
                                    uploadImage();
                                  },
                                  child: const CustomText(text: 'Choose'),
                                ),
                                const SizedBox(height: 10),
                                const CustomText(
                                  text: "Best size: ",
                                  size: 14,
                                ),
                                const CustomText(
                                  text: "256px by 256px",
                                  size: 14,
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 30),
                      // Assign Exercise
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const CustomText(
                            text: "Assign Exercise",
                            weight: FontWeight.bold,
                          ),
                          const SizedBox(width: 150),
                          Expanded(
                            child:
                                LayoutBuilder(builder: (context, constraints) {
                              double maxWidth = constraints.maxWidth;
                              double maxHeight = exercisesOptions.length * 90;
                              maxHeight = maxHeight + 70;
                              return Container(
                                padding: const EdgeInsets.all(16.0),
                                height: maxHeight,
                                width: maxWidth,
                                child: ReorderableListView(
                                  buildDefaultDragHandles: false,
                                  children: exercisesOptions
                                      .asMap()
                                      .entries
                                      .map((entry) {
                                    final int optionIndex = entry.key;
                                    return Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      key: ValueKey(optionIndex),
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            ReorderableDragStartListener(
                                              index: optionIndex,
                                              child: const Icon(Icons
                                                  .drag_indicator_outlined), // Reorder icon on the left
                                            ),
                                            const SizedBox(
                                              width: 10,
                                            ),
                                            _exerciseOption(optionIndex)
                                          ],
                                        ),
                                        if (optionIndex ==
                                            exercisesOptions.length - 1)
                                          Container(
                                            margin: EdgeInsets.only(top: 20),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Container(
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    border: Border.all(
                                                      color: Colors.grey,
                                                      width: 1.0,
                                                    ),
                                                  ),
                                                  child: IconButton(
                                                    icon: const Icon(Icons.add),
                                                    onPressed: () {
                                                      _addExerciseOption(
                                                          optionIndex);
                                                    },
                                                  ),
                                                ),
                                                const SizedBox(width: 30),
                                                const CustomText(
                                                    text:
                                                        "Add another Exercise")
                                              ],
                                            ),
                                          )
                                      ],
                                    );
                                  }).toList(),
                                  onReorder: (oldIndex, newIndex) {
                                    reOrderOption(oldIndex, newIndex);
                                  },
                                ),
                              );
                            }),
                          ),
                        ],
                      ),
                      if (errorMessage.isNotEmpty)
                        Text(
                          errorMessage,
                          style: const TextStyle(
                            color: Colors.red,
                            fontSize: 16,
                          ),
                        ),
                    ],
                  ),
                )),
            Expanded(
                flex: 1,
                child: Column(
                  children: [
                    Row(
                      children: [
                        CustomText(
                          text: topicsController.updateTopicStatus.value
                              ? "Status: Published"
                              : "Status: Draft",
                          weight: FontWeight.bold,
                        ),
                        Expanded(child: Container()),
                        Tooltip(
                          message: "This is a instruction",
                          child: IconButton(
                            hoverColor: Colors.transparent,
                            icon: const Icon(Icons.info),
                            onPressed: () {},
                          ),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 150,
                          height: 50,
                          child: OutlinedButton(
                            onPressed: () {
                              // Add your button action here
                              _saveDraft();
                            },
                            child: const CustomText(text: 'Save Draft'),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        SizedBox(
                          width: 120,
                          height: 50,
                          child: OutlinedButton(
                            onPressed: () {
                              // Add your button action here
                              _saveDraft();
                            },
                            child: const CustomText(text: 'Confirm'),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    // Action Needed
                    DottedBorder(
                      borderType: BorderType.Rect,
                      color: Colors.black,
                      strokeWidth: 2,
                      dashPattern: const [5, 5],
                      child: SizedBox(
                        width: 400,
                        height: 400,
                        child: ListView(
                          children: const [
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: CustomText(
                                text: "Action needed",
                                weight: FontWeight.bold,
                              ),
                            ),
                            ListTile(
                              leading: Icon(Icons.circle, size: 20),
                              title: Text(
                                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."),
                            ),
                            ListTile(
                              leading: Icon(Icons.circle, size: 20),
                              title: Text('Item 2'),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                )),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        const Divider(color: Colors.grey, thickness: 2),
        Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              OutlinedButton(
                onPressed: () {
                  topicsController.changeAddNewStatusTo(false);
                },
                child: Container(
                    padding: const EdgeInsets.all(8.0),
                    width: 100,
                    height: 40,
                    child: const Align(
                        alignment: Alignment.center,
                        child: CustomText(text: "Back"))),
              )
            ]),
      ],
    );
  }

  Widget _exerciseOption(int exerciseIndex) {
    var value = exerciseSelectedList[exerciseIndex] == ''
        ? null
        : exerciseSelectedList[exerciseIndex];
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: 50,
            width: 550,
            child: DropdownButton<String>(
              value: value,
              isExpanded: true,
              onChanged: (String? exerciseId) {
                if (exerciseId != null) {
                  if (!exerciseSelectedList.contains(exerciseId)) {
                    setState(() {
                      exercisesOptions[exerciseIndex] = exerciseId;
                      exerciseSelectedList[exerciseIndex] = exerciseId;
                    });
                  } else {
                    // warning to user have this exercise already
                    showError('You have already chosen this exercise',
                        const Duration(seconds: 2));
                  }
                }
              },
              hint: const CustomText(text: "Please Select One"),
              items: exercisesController.exercisesObs
                  .map<DropdownMenuItem<String>>(
                (Exercise exercise) {
                  var isEnabled = !exerciseSelectedList.contains(exercise.id);
                  return DropdownMenuItem<String>(
                    enabled: isEnabled,
                    value: exercise.id,
                    child: Text(exercise.title),
                  );
                },
              ).toList(),
            ),
          ),
          if (exercisesOptions.length > 1)
            Container(
              margin: EdgeInsets.only(right: exerciseIndex != 0 ? 35 : 0),
              child: IconButton(
                icon: const Icon(Icons.delete),
                hoverColor: Colors.transparent,
                onPressed: () {
                  _removeExerciseOption(exerciseIndex);
                },
              ),
            ),
          if (exerciseIndex == 0)
            Tooltip(
              message: "This is a instruction",
              child: IconButton(
                hoverColor: Colors.transparent,
                icon: const Icon(Icons.info),
                onPressed: () {},
              ),
            )
        ],
      ),
    );
  }
}
