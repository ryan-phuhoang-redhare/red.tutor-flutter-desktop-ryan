import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:html/parser.dart';
import 'package:intl/intl.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/constants/style.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';
import 'package:redtutor_flutter_webapp/widgets/popup_untils.dart';

class TopicsTable extends StatefulWidget {
  const TopicsTable({super.key});

  @override
  State<TopicsTable> createState() => _TopicsTableState();
}

class _TopicsTableState extends State<TopicsTable> {
  Future<void> saveSelectedTopic(int index) async {
    var topicSelected = topicsController.topicsObs[index];
    await topicsController.selectedTopic(topicSelected);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: active.withOpacity(.4), width: .5),
            boxShadow: [
              BoxShadow(
                  offset: const Offset(0, 6),
                  color: lightGrey.withOpacity(.1),
                  blurRadius: 12)
            ],
            borderRadius: BorderRadius.circular(8),
          ),
          padding: const EdgeInsets.all(16),
          margin: const EdgeInsets.only(bottom: 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  const SizedBox(
                    width: 10,
                  ),
                  CustomText(
                    text: "Published (${topicsController.topicsObs.length})",
                    color: dark,
                    weight: FontWeight.bold,
                  ),
                  // Container(
                  //   width: 1,
                  //   height: 22,
                  //   color: dark,
                  //   margin: const EdgeInsets.only(left: 10, right: 10),
                  // ),
                  // CustomText(
                  //   text: "Draft (3)",
                  //   color: lightGrey,
                  //   weight: FontWeight.bold,
                  // ),
                  const SizedBox(
                    width: 50,
                  ),
                  SizedBox(
                    width: 150,
                    height: 50,
                    child: OutlinedButton.icon(
                        onPressed: () {
                          topicsController.changeAddNewStatusTo(true);
                          topicsController.changeUpdateTopicStatusTo(false);
                        },
                        icon: Icon(
                          Icons.add,
                          color: dark,
                        ),
                        label: const CustomText(text: "Add New")),
                  ),
                  Expanded(child: Container()),
                  // const Expanded(
                  //   child: TextField(
                  //     decoration: InputDecoration(
                  //       hintText: 'Search...',
                  //     ),
                  //   ),
                  // ),
                  // IconButton(
                  //   onPressed: () {},
                  //   icon: const Icon(Icons.search),
                  // ),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              // 56: dataRowHeight
              // 7: number of data each page
              // 40: headingRowHeight
              SizedBox(
                height: (100 * 7) + 40,
                child: DataTable2(
                  columnSpacing: 12,
                  dataRowHeight: 100,
                  headingRowHeight: 40,
                  horizontalMargin: 12,
                  minWidth: 600,
                  showCheckboxColumn: false,
                  columns: [
                    const DataColumn2(
                        label: CustomText(
                          text: "Topic Title",
                          weight: FontWeight.bold,
                        ),
                        size: ColumnSize.L),
                    DataColumn2(
                        label: Row(
                      children: [
                        const CustomText(
                            text: 'Last Edited', weight: FontWeight.bold),
                        // IconButton(
                        //   hoverColor: Colors.transparent,
                        //   icon: const Icon(Icons.arrow_upward),
                        //   onPressed: () {},
                        // )
                      ],
                    )),
                    const DataColumn(
                      label: CustomText(
                        text: 'Image',
                        weight: FontWeight.bold,
                      ),
                    ),
                    const DataColumn2(
                      label: CustomText(
                          text: 'Exercises', weight: FontWeight.bold),
                      size: ColumnSize.S,
                    ),
                    const DataColumn2(
                        label: CustomText(
                          text: "Topic Description",
                          weight: FontWeight.bold,
                        ),
                        size: ColumnSize.L),
                    DataColumn2(
                      label: Row(
                        children: [
                          const CustomText(
                              text: 'Status', weight: FontWeight.bold),
                          // IconButton(
                          //   hoverColor: Colors.transparent,
                          //   icon: const Icon(Icons.arrow_upward),
                          //   onPressed: () {},
                          // )
                        ],
                      ),
                      size: ColumnSize.S,
                    ),
                    const DataColumn2(
                      label: CustomText(text: ''),
                      size: ColumnSize.M,
                    ),
                  ],
                  rows: List<DataRow>.generate(
                      topicsController.topicsObs.length, (index) {
                    var topic = topicsController.topicsObs[index];
                    String imageUrl =
                        'https://i.ibb.co/q1Kmvw2/default-image.png';
                    if (topic.mediaResource.url != "") {
                      imageUrl = topic.mediaResource.url;
                    }
                    var timemiliseconds =
                        DateTime.fromMillisecondsSinceEpoch(topic.lastEdited);
                    String lastEdtiedformattedDate =
                        DateFormat('yyyy-MM-dd').format(timemiliseconds);
                    final document = parse(topic.description);
                    String descriptionPlainText =
                        parse(document.body!.text).documentElement!.text;
                    return DataRow(
                      cells: [
                        // Topic title
                        DataCell(CustomText(text: topic.title)),
                        // Last Edited
                        DataCell(CustomText(text: lastEdtiedformattedDate)),
                        // Image
                        DataCell(Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.network(
                            imageUrl,
                            width: 100,
                          ),
                        )),
                        // Exercises
                        DataCell(CustomText(
                            text: topic.assignedExercises.length.toString())),
                        // Topic description
                        DataCell(Tooltip(
                          message: descriptionPlainText,
                          child: CustomText(
                            text: descriptionPlainText,
                            overflow: TextOverflow.ellipsis,
                          ),
                        )),
                        // Status
                        DataCell(CustomText(text: topic.status)),
                        // Action button
                        DataCell(Row(
                          children: [
                            IconButton(
                              hoverColor: Colors.transparent,
                              icon: const Icon(Icons.delete),
                              onPressed: () async {
                                await saveSelectedTopic(index);
                                try {
                                  topicsController.deleteTopic();
                                  showPopupStatus(context, 'success');
                                } catch (e) {
                                  showPopupStatus(context, 'error');
                                }
                              },
                            ),
                            // IconButton(
                            //   hoverColor: Colors.transparent,
                            //   icon: const Icon(Icons.copy),
                            //   onPressed: () {},
                            // ),
                          ],
                        )),
                      ],
                      onSelectChanged: (isSelected) {
                        // Handle row click action here
                        saveSelectedTopic(index);
                        setState(() {
                          topicsController.changeAddNewStatusTo(true);
                          topicsController.changeUpdateTopicStatusTo(true);
                        });
                      },
                    );
                  }),
                ),
              ),
            ],
          ),
        ));
  }
}
