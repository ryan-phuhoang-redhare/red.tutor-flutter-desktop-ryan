import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_topics/widgets/add_new_topic.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_topics/widgets/topics_table.dart';

class MyTopicsTab extends StatelessWidget {
  const MyTopicsTab({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() => Expanded(
          child: ListView(children: [
            if (topicsController.addNewStatus.value == false)
              const TopicsTable()
            else
              const AddNewTopicPage()
          ]),
        ));
  }
}
