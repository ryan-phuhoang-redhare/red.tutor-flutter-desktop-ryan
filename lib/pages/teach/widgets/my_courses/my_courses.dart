import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_courses/widgets/add_new_course.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_courses/widgets/courses_table.dart';

class MyCoursesTab extends StatelessWidget {
  const MyCoursesTab({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() => Expanded(
          child: ListView(children: [
            if (coursesController.addNewStatus.value == false)
              const CoursesTable()
            else
              const AddNewCourse()
          ]),
        ));
  }
}
