import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/constants/style.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';
import 'package:redtutor_flutter_webapp/widgets/popup_untils.dart';

class CoursesTable extends StatefulWidget {
  const CoursesTable({super.key});

  @override
  State<CoursesTable> createState() => _CoursesTableState();
}

class _CoursesTableState extends State<CoursesTable> {
  @override
  void initState() {
    super.initState();
  }

  Future<void> saveSelectedCourse(int index) async {
    var courseSelected = coursesController.coursesObs[index];
    await coursesController.selectedCourse(courseSelected);
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: active.withOpacity(.4), width: .5),
            boxShadow: [
              BoxShadow(
                  offset: const Offset(0, 6),
                  color: lightGrey.withOpacity(.1),
                  blurRadius: 12)
            ],
            borderRadius: BorderRadius.circular(8),
          ),
          padding: const EdgeInsets.all(16),
          margin: const EdgeInsets.only(bottom: 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  const SizedBox(
                    width: 10,
                  ),
                  CustomText(
                    text: "Published (${coursesController.coursesObs.length})",
                    color: dark,
                    weight: FontWeight.bold,
                  ),
                  // Container(
                  //   width: 1,
                  //   height: 22,
                  //   color: dark,
                  //   margin: const EdgeInsets.only(left: 10, right: 10),
                  // ),
                  // CustomText(
                  //   text: "Draft (3)",
                  //   color: lightGrey,
                  //   weight: FontWeight.bold,
                  // ),
                  const SizedBox(
                    width: 50,
                  ),
                  SizedBox(
                    width: 150,
                    height: 50,
                    child: OutlinedButton.icon(
                        onPressed: () {
                          coursesController.changeAddNewStatusTo(true);
                          coursesController.changeUpdateCourseStatusTo(false);
                        },
                        icon: Icon(
                          Icons.add,
                          color: dark,
                        ),
                        label: const CustomText(text: "Add New")),
                  ),
                  Expanded(child: Container()),
                  // const Expanded(
                  //   child: TextField(
                  //     decoration: InputDecoration(
                  //       hintText: 'Search...',
                  //     ),
                  //   ),
                  // ),
                  // IconButton(
                  //   onPressed: () {},
                  //   icon: const Icon(Icons.search),
                  // ),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              // 56: dataRowHeight
              // 7: number of data each page
              // 40: headingRowHeight
              SizedBox(
                height: (100 * 7) + 40,
                child: DataTable2(
                  columnSpacing: 12,
                  dataRowHeight: 100,
                  headingRowHeight: 40,
                  horizontalMargin: 12,
                  minWidth: 600,
                  showCheckboxColumn: false,
                  columns: const [
                    DataColumn2(
                        label: CustomText(
                          text: "Course Title",
                          weight: FontWeight.bold,
                        ),
                        size: ColumnSize.L),
                    DataColumn2(
                        label: Row(
                      children: [
                        CustomText(
                            text: 'Last Edited', weight: FontWeight.bold),
                        // IconButton(
                        //   hoverColor: Colors.transparent,
                        //   icon: const Icon(Icons.arrow_upward),
                        //   onPressed: () {},
                        // )
                      ],
                    )),
                    DataColumn(
                      label: CustomText(
                        text: 'Image',
                        weight: FontWeight.bold,
                      ),
                    ),
                    DataColumn2(
                      label: CustomText(text: 'Price', weight: FontWeight.bold),
                      size: ColumnSize.S,
                    ),
                    DataColumn2(
                      label: CustomText(
                          text: 'Categories', weight: FontWeight.bold),
                      size: ColumnSize.L,
                    ),
                    DataColumn2(
                      label:
                          CustomText(text: 'Alerts', weight: FontWeight.bold),
                      size: ColumnSize.S,
                    ),
                    DataColumn2(
                      label: Row(
                        children: [
                          CustomText(text: 'Status', weight: FontWeight.bold),
                          // IconButton(
                          //   hoverColor: Colors.transparent,
                          //   icon: const Icon(Icons.arrow_upward),
                          //   onPressed: () {},
                          // )
                        ],
                      ),
                      size: ColumnSize.S,
                    ),
                    DataColumn2(
                      label: CustomText(text: ''),
                      size: ColumnSize.M,
                    ),
                  ],
                  // Obx(() =>)
                  rows: List<DataRow>.generate(
                      coursesController.coursesObs.length, (index) {
                    var course = coursesController.coursesObs[index];
                    String imageUrl =
                        'https://i.ibb.co/q1Kmvw2/default-image.png';
                    if (course.mediaResource.url != "") {
                      imageUrl = course.mediaResource.url;
                    }
                    var timemiliseconds =
                        DateTime.fromMillisecondsSinceEpoch(course.lastEdited);
                    String lastEdtiedformattedDate =
                        DateFormat('yyyy-MM-dd').format(timemiliseconds);
                    List<String> categoriesRow1 = [
                      course.difficulty,
                      course.subjectLanguage
                    ];
                    List<String> categoriesRow2 = [
                      course.instructionLanguage[0],
                      course.theme
                    ];
                    return DataRow(
                      cells: [
                        // Name (title)
                        DataCell(CustomText(
                            text: coursesController.coursesObs[index].name)),
                        // Last Edited
                        DataCell(CustomText(text: lastEdtiedformattedDate)),
                        // Image
                        DataCell(Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.network(
                            imageUrl,
                            width: 100,
                          ),
                        )),
                        // Price
                        DataCell(Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            CustomText(
                                text: coursesController
                                            .coursesObs[index].unlockCost ==
                                        0
                                    ? "Free"
                                    : coursesController
                                        .coursesObs[index].unlockCost
                                        .toString()),
                            // const Icon(Icons.lock)
                          ], // Lock icon means this is only for subscription
                        )),
                        // Categories
                        DataCell(
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Wrap(
                                spacing:
                                    8.0, // Space between items horizontally
                                runSpacing:
                                    8.0, // Space between items vertically
                                children: List.generate(
                                  categoriesRow1.length,
                                  (index) => Chip(
                                    label: Text(categoriesRow1[index]),
                                    backgroundColor: Colors.blue,
                                  ),
                                ),
                              ),
                              Wrap(
                                spacing:
                                    8.0, // Space between items horizontally
                                runSpacing:
                                    8.0, // Space between items vertically
                                children: List.generate(
                                  categoriesRow2.length,
                                  (index) => Chip(
                                    label: Text(categoriesRow2[index]),
                                    backgroundColor: Colors.blue,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        // Alerts
                        const DataCell(CustomText(text: "--")),
                        // Status
                        DataCell(CustomText(text: course.status)),
                        // Actions button
                        DataCell(Row(
                          children: [
                            IconButton(
                              hoverColor: Colors.transparent,
                              icon: const Icon(Icons.delete),
                              onPressed: () async {
                                await saveSelectedCourse(index);
                                try {
                                  coursesController.deleteCourse();
                                  showPopupStatus(context, 'success');
                                } catch (e) {
                                  showPopupStatus(context, 'error');
                                }
                              },
                            ),
                            // IconButton(
                            //   hoverColor: Colors.transparent,
                            //   icon: const Icon(Icons.copy),
                            //   onPressed: () {
                            //     saveSelectedCourse(index);
                            //   },
                            // ),
                          ],
                        )),
                      ],
                      // Wrap the DataRow with InkWell to handle row-level click
                      onSelectChanged: (isSelected) {
                        // Handle row click action here
                        saveSelectedCourse(index);
                        setState(() {
                          coursesController.changeAddNewStatusTo(true);
                          coursesController.changeUpdateCourseStatusTo(true);
                        });
                      },
                    );
                  }),
                ),
              ),
            ],
          ),
        ));
  }
}
