import 'package:flutter/material.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_courses/widgets/basic_information_course.dart';
import 'package:redtutor_flutter_webapp/pages/teach/widgets/my_courses/widgets/topic_tree.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

class AddNewCourse extends StatefulWidget {
  const AddNewCourse({super.key});

  @override
  State<AddNewCourse> createState() => _AddNewCourseState();
}

class _AddNewCourseState extends State<AddNewCourse> {
  var courseAccessValue = '';

  updateCourseAccessValue(var newValue) {
    setState(() {
      courseAccessValue = newValue;
    });
    print('Course Access Value: $newValue');
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // Information of Course
        BasicInformationCourse(
            updateCourseAccessValue: updateCourseAccessValue),
        const SizedBox(
          height: 20,
        ),
        const Divider(color: Colors.grey, thickness: 2),
        const SizedBox(
          height: 20,
        ),
        // Topic Tree
        const TopicTree(),
        // Expanded(child: Container()),
        const SizedBox(
          height: 20,
        ),
        const Divider(color: Colors.grey, thickness: 2),
        Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              OutlinedButton(
                onPressed: () {
                  coursesController.changeAddNewStatusTo(false);
                },
                child: Container(
                    padding: const EdgeInsets.all(8.0),
                    width: 100,
                    height: 40,
                    child: const Align(
                        alignment: Alignment.center,
                        child: CustomText(text: "Back"))),
              )
            ])
      ],
    );
  }
}
