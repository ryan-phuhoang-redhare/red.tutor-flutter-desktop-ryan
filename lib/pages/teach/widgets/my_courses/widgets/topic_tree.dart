import 'dart:async';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/models/course/graphNode.dart';
import 'package:redtutor_flutter_webapp/models/course/graphRow.dart';
import 'package:redtutor_flutter_webapp/models/course/prerequisiteItem.dart';
import 'package:redtutor_flutter_webapp/models/topic/topic.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

class TopicTree extends StatefulWidget {
  const TopicTree({super.key});

  @override
  State<TopicTree> createState() => _TopicTreeState();
}

class _TopicTreeState extends State<TopicTree> {
  List<PrerequisiteItem> checkboxPrerequisiteItems = [];

  String? topicSelected;
  var errorMessage = '';
  Timer? timer;
  @override
  void initState() {
    coursesController.initialRows();
    super.initState();
  }

  _loadPrerequisiteItems() {
    checkboxPrerequisiteItems = [];
    int rowSelectedIndex = coursesController.rowSelectedIndex.value;
    String nodePositionSelected = coursesController.nodePositionSelected.value;
    int indexNodeSelected = coursesController.rows[rowSelectedIndex].nodes
        .indexWhere((element) => element.position == nodePositionSelected);
    GraphNode nodeSelected =
        coursesController.rows[rowSelectedIndex].nodes[indexNodeSelected];
    List<String> prerequisitesNodeSelected = nodeSelected.prerequisites;
    List<GraphNode> nodes =
        coursesController.getNodesFromRowIndex(rowSelectedIndex - 1);
    nodes.forEach((node) {
      PrerequisiteItem item = PrerequisiteItem(
          nodePosition: node.position,
          text: "Topic ${node.position}",
          isChecked: false);
      if (prerequisitesNodeSelected.contains(node.id)) {
        item.isChecked = true;
      } else {
        item.isChecked = false;
      }

      checkboxPrerequisiteItems.add(item);
    });
    if (nodeSelected.topicId != "") {
      setState(() {
        topicSelected = nodeSelected.topicId;
      });
    } else {
      setState(() {
        topicSelected = null;
      });
    }
  }

  void showError(String message, Duration duration) {
    setState(() {
      errorMessage = message;
    });

    timer = Timer(duration, () {
      setState(() {
        errorMessage = '';
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      alignment: Alignment.centerLeft,
                      child: const Padding(
                        padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                        child: CustomText(
                            text: "Topic tree", weight: FontWeight.bold),
                      )),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                          flex: 4,
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width,
                            child: ListView.builder(
                                shrinkWrap: true,
                                itemCount: coursesController.rows.length,
                                itemBuilder: (BuildContext context, int index) {
                                  var rows = coursesController.rows;
                                  return Container(
                                      margin: const EdgeInsets.only(bottom: 20),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          _buildRow(rows[index], index),
                                          Stack(
                                            alignment: Alignment.centerRight,
                                            children: [
                                              const Divider(),
                                              if (rows.length > 1 &&
                                                  index < rows.length - 1)
                                                GestureDetector(
                                                  onTap: () {
                                                    coursesController
                                                        .insertRow(index);
                                                  },
                                                  child: MouseRegion(
                                                    cursor: SystemMouseCursors
                                                        .click,
                                                    child: Tooltip(
                                                      verticalOffset: 15,
                                                      margin:
                                                          const EdgeInsets.only(
                                                              right: 20),
                                                      message:
                                                          "Insert a row here",
                                                      child: Container(
                                                          margin:
                                                              const EdgeInsets
                                                                  .only(
                                                                  right: 190),
                                                          child: const Icon(
                                                              Icons
                                                                  .add_circle)),
                                                    ),
                                                  ),
                                                ),
                                            ],
                                          )
                                        ],
                                      ));
                                }),
                          ))
                    ],
                  )
                ],
              ),
            ),
          ],
        ));
  }

  Widget _buildRow(GraphRow row, int rowIndex) {
    var rows = coursesController.rows;
    var foundRow = rows.firstWhereOrNull((rowEle) => rowEle.id == row.id);
    return Obx(() => Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
                flex: 3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    if (foundRow != null)
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: List.generate(
                            rows[rowIndex].nodes.length,
                            (nodeIndex) {
                              var nodes = rows[rowIndex].nodes;
                              return Container(
                                  margin: const EdgeInsets.only(right: 50),
                                  child: _buildNode(rowIndex,
                                      nodes[nodeIndex].position, nodeIndex));
                            },
                          ),
                        ),
                      ),
                    // Button Add New Topic Slot
                    if (rows[rowIndex].nodes.length < 3)
                      SizedBox(
                        width: 150,
                        height: 150,
                        child: Center(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            GestureDetector(
                              onTap: () {
                                coursesController.addNode(row);
                              },
                              child: MouseRegion(
                                cursor: SystemMouseCursors.click,
                                onEnter: (_) {
                                  setState(() {});
                                },
                                onExit: (_) {
                                  setState(() {});
                                },
                                child: Container(
                                  width: 50,
                                  height: 50,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                      color: Colors.grey,
                                      width: 1,
                                    ),
                                    color: Colors.transparent,
                                  ),
                                  child: const Icon(Icons.add),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            const CustomText(text: "New Slot"),
                          ],
                        )),
                      ),
                  ],
                )),
            Expanded(
                flex: 3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (rowIndex > 0)
                          TextButton.icon(
                              onPressed: () {
                                coursesController.removeRow(rowIndex);
                              },
                              icon: const Icon(Icons.remove),
                              label: const CustomText(text: "Remove Row")),
                        const SizedBox(height: 10),
                        if (rowIndex == rows.length - 1)
                          TextButton.icon(
                              onPressed: () {
                                coursesController.addRowBelow(rowIndex);
                              },
                              icon: const Icon(Icons.add),
                              label: const CustomText(text: "New Row"))
                      ],
                    ),
                    if (coursesController.isDisplayAddedTopicForm.value &&
                        rowIndex == coursesController.rowSelectedIndex.value)
                      addedTopicForm(),
                  ],
                ))
          ],
        ));
  }

  Widget _buildNode(int rowIndex, String nodePosition, int nodeIndex) {
    var nodes = coursesController.rows[rowIndex].nodes;
    String centerNodeText = "Add Topic";
    if (nodes[nodeIndex].topicId != null && nodes[nodeIndex].topicId != "") {
      centerNodeText = nodes[nodeIndex].topicTitle!;
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {
                coursesController.setDisplayAddedTopicForm(true);
                coursesController.setNodePositionSelected(nodePosition);
                coursesController.setRowSelectedIndex(rowIndex);
                _loadPrerequisiteItems();
              },
              child: MouseRegion(
                cursor: SystemMouseCursors.click,
                child: Tooltip(
                  message:
                      nodes[nodeIndex].topicId != null ? centerNodeText : '',
                  child: DottedBorder(
                    borderType: BorderType.Circle,
                    color: Colors.black,
                    strokeWidth: 2,
                    dashPattern: const [5, 5],
                    child: Container(
                      width: 150,
                      height: 150,
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle, color: Colors.white),
                      child: Center(
                        child: Text(
                          centerNodeText.length <= 10
                              ? centerNodeText
                              : '${centerNodeText.substring(0, 10)}...',
                          style: const TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            if (coursesController.rows[rowIndex].nodes.length > 1)
              MouseRegion(
                cursor: SystemMouseCursors.click,
                child: GestureDetector(
                    onTap: () {
                      coursesController.removeNode(rowIndex, nodePosition);
                    },
                    child: const Icon(Icons.delete,
                        size: 40, color: Colors.black54)),
              )
          ],
        ),
        const SizedBox(height: 20),
        CustomText(
          text: "Node $nodePosition",
          weight: FontWeight.bold,
        ),
      ],
    );
  }

  Widget addedTopicForm() {
    if (coursesController.isDisplayAddedTopicForm.value == true) {
      return Expanded(
        flex: 1,
        child: Container(
          margin: const EdgeInsets.only(right: 20),
          padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
          width: 500,
          height: 400,
          decoration: BoxDecoration(
            color: Colors.white, // Container background color
            borderRadius: BorderRadius.circular(10), // Border radius (optional)
            boxShadow: const [
              BoxShadow(
                color: Colors.grey, // Shadow color
                offset: Offset(0, 3), // Offset of the shadow (X, Y)
                blurRadius: 6, // Blur radius
                spreadRadius: 0, // Spread radius
              ),
            ],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                children: [
                  CustomText(
                      text:
                          "Assign Node ${coursesController.nodePositionSelected}",
                      weight: FontWeight.bold),
                  const SizedBox(
                    width: 20,
                  ),
                  Tooltip(
                    message: "This is a instruction",
                    child: IconButton(
                      hoverColor: Colors.transparent,
                      icon: const Icon(Icons.info),
                      onPressed: () {},
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Row(
                    // alignment: Alignment.centerRight,
                    children: [
                      Expanded(
                        flex: 4,
                        child: DropdownButton<String>(
                          hint: const Text("Please Select One"),
                          value: topicSelected,
                          isExpanded: true,
                          onChanged: (String? newValue) {
                            if (coursesController.topicSelectedList
                                .contains(newValue)) {
                              showError('You have already chosen this topic',
                                  const Duration(seconds: 3));
                            } else {
                              setState(() {
                                topicSelected = newValue;
                              });
                              coursesController.topicSelectedList
                                  .add(newValue!);
                            }
                          },
                          items: topicsController.topicsObs
                              .map<DropdownMenuItem<String>>(
                                (Topic topic) => DropdownMenuItem<String>(
                                  value: topic.id,
                                  child: Text(topic.title),
                                ),
                              )
                              .toList(),
                        ),
                      ),
                      if (topicSelected != null)
                        Expanded(
                            flex: 1,
                            child: OutlinedButton(
                                onPressed: () {
                                  coursesController.topicSelectedList
                                      .remove(topicSelected);
                                  setState(() {
                                    topicSelected = null;
                                  });
                                },
                                child: const CustomText(text: "Clear")))
                    ],
                  ),
                  if (errorMessage.isNotEmpty)
                    Text(
                      errorMessage,
                      style: const TextStyle(
                        color: Colors.red,
                        fontSize: 16,
                      ),
                    ),
                ],
              ),
              if (coursesController.rowSelectedIndex.value != 0)
                const SizedBox(
                  height: 30,
                ),
              if (coursesController.rowSelectedIndex.value != 0)
                Row(
                  children: [
                    const CustomText(
                        text: "Unlock Prerequisite", weight: FontWeight.bold),
                    const SizedBox(
                      width: 20,
                    ),
                    Tooltip(
                      message: "This is a instruction",
                      child: IconButton(
                        hoverColor: Colors.transparent,
                        icon: const Icon(Icons.info),
                        onPressed: () {},
                      ),
                    )
                  ],
                ),
              if (coursesController.rowSelectedIndex.value != 0)
                SizedBox(
                  width: 600,
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: checkboxPrerequisiteItems.length,
                    itemBuilder: (context, index) {
                      PrerequisiteItem item = checkboxPrerequisiteItems[index];
                      return ListTile(
                        title: Text("Node ${item.nodePosition}"),
                        leading: Checkbox(
                          value: item.isChecked,
                          onChanged: (value) {
                            setState(() {
                              checkboxPrerequisiteItems[index].isChecked =
                                  value!;
                            });
                          },
                        ),
                      );
                    },
                  ),
                ),
              const SizedBox(height: 20),
              Expanded(child: Container()),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  OutlinedButton(
                    onPressed: () {
                      coursesController.setDisplayAddedTopicForm(false);
                    },
                    child: Container(
                        padding: const EdgeInsets.all(8.0),
                        width: 100,
                        height: 40,
                        child: const Align(
                            alignment: Alignment.center,
                            child: CustomText(text: "Cancel"))),
                  ),
                  OutlinedButton(
                    onPressed: () async {
                      coursesController.setDisplayAddedTopicForm(false);
                      List<PrerequisiteItem> checkedItem =
                          checkboxPrerequisiteItems
                              .where((element) => element.isChecked == true)
                              .toList();
                      await coursesController.setPrerequisiteForNode(
                          checkedItem,
                          topicSelected == null ? '' : topicSelected!);
                    },
                    child: Container(
                        padding: const EdgeInsets.all(8.0),
                        width: 100,
                        height: 40,
                        child: const Align(
                            alignment: Alignment.center,
                            child: CustomText(text: "Submit"))),
                  )
                ],
              )
            ],
          ),
        ),
      );
    } else {
      return Container();
    }
  }
}
