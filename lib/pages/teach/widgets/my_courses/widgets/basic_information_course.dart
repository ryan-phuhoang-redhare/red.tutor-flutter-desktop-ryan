import 'dart:convert';
import 'dart:typed_data';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/constants/style.dart';
import 'package:redtutor_flutter_webapp/models/course/course.dart';
import 'package:redtutor_flutter_webapp/services/authentication_service.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';
// ignore: avoid_web_libraries_in_flutter
import 'dart:html' as html;

import 'package:redtutor_flutter_webapp/widgets/popup_untils.dart';

import '../../../../../models/mediaRecourse.dart';

class BasicInformationCourse extends StatefulWidget {
  final Function(String courseAccessValue)? updateCourseAccessValue;
  const BasicInformationCourse(
      {super.key, required this.updateCourseAccessValue});

  @override
  State<BasicInformationCourse> createState() => _BasicInformationCourseState();
}

class _BasicInformationCourseState extends State<BasicInformationCourse> {
  final TextEditingController _title = TextEditingController();
  final TextEditingController _description = TextEditingController();
  final TextEditingController _price = TextEditingController();

  String difficulty = "Select One";
  String subjectLanguage = "Select One";
  String instructLanguage = "Select One";
  String theme = "Select One";

  bool _isBold = false;
  bool _isUnderlined = false;
  bool _isItalic = false;
  int courseAccessOption = 1;
  List<int> uint8Image = [];
  String base64Data = '';
  String imageUrl = 'https://i.ibb.co/q1Kmvw2/default-image.png';
  @override
  void initState() {
    loadCourseSelected();
    super.initState();
  }

  @override
  void dispose() {
    _title.dispose();
    _description.dispose();
    _price.dispose();
    super.dispose();
  }

  void loadCourseSelected() async {
    if (coursesController.updateCourseStatus.value == true) {
      Course? retrievedCourseSelected =
          await coursesController.loadCourseSelectedFromSharedPreferences();
      _title.text = retrievedCourseSelected!.name;
      _description.text = retrievedCourseSelected.description;
      _price.text = retrievedCourseSelected.unlockCost.toString();
      setState(() {
        difficulty = retrievedCourseSelected.difficulty;
        subjectLanguage = retrievedCourseSelected.subjectLanguage;
        instructLanguage = retrievedCourseSelected.instructionLanguage[0];
        theme = retrievedCourseSelected.theme;
        imageUrl = retrievedCourseSelected.mediaResource.url;
      });
    } else {
      _title.text = "";
      _description.text = "";
      _price.text = "";
      setState(() {
        difficulty = "Select One";
        subjectLanguage = "Select One";
        instructLanguage = "Select One";
        theme = "Select One";
      });
    }
  }

  void setCourseAccessOption(int value) {
    setState(() {
      courseAccessOption = value;
    });
    widget.updateCourseAccessValue?.call(value.toString());
  }

  void uploadImage() {
    final html.FileUploadInputElement input = html.FileUploadInputElement()
      ..accept = 'image/*'; // Limit file types to images

    input
        .click(); // Simulate a click on the input element to open the file picker

    input.onChange.listen((e) {
      final html.File file = input.files!.first;
      final html.FileReader reader = html.FileReader();

      reader.onLoadEnd.listen((e) {
        // Handle the uploaded image, e.g., send it to a server or display it

        Uint8List data = reader.result as Uint8List;
        String imageBase64Data = base64Encode(data);
        setState(() {
          uint8Image = data;
          base64Data = imageBase64Data;
        });
      });

      reader.readAsArrayBuffer(file);
    });
  }

  Future<Course> getCourseRequest() async {
    Course? courseSelected =
        await coursesController.loadCourseSelectedFromSharedPreferences();
    String userId = await AuthenticationService.getUserId() as String;
    double unlockCost = double.tryParse(_price.text)!;
    List<String> instructionLanguage = [];
    instructionLanguage.add(instructLanguage);

    Course course = Course(
        id: courseSelected != null ? courseSelected.id : '',
        name: _title.text,
        isPremium: false,
        unlockCost: unlockCost,
        subjectLanguage: subjectLanguage,
        instructionLanguage: instructionLanguage,
        theme: theme,
        difficulty: difficulty,
        description: _description.text,
        mediaResource: MediaResource(id: '', mediaType: '', url: ''),
        imageBase64Data: base64Data,
        ratingsCount: 0,
        averageRating: 0,
        creatorId: userId,
        createdAt: DateTime.now().millisecondsSinceEpoch,
        status: '',
        lastEdited: 0);
    return course;
  }

  void saveDraft() async {
    Course courseRequest = await getCourseRequest();
    if (coursesController.updateCourseStatus.value == true) {
      try {
        coursesController.updateCourse(courseRequest);
        showPopupStatus(context, 'success');
        coursesController.changeAddNewStatusTo(false);
      } catch (e) {
        showPopupStatus(context, 'error');
      }
    } else {
      try {
        coursesController.createCourse(courseRequest);
        showPopupStatus(context, 'success');
        coursesController.changeAddNewStatusTo(false);
      } catch (e) {
        showPopupStatus(context, 'error');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
            flex: 4,
            child: Container(
              margin: const EdgeInsets.only(right: 50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // Course Title
                  Container(
                      margin: const EdgeInsets.only(top: 10, bottom: 10),
                      child: Column(
                        children: [
                          const Align(
                              alignment: Alignment.topLeft,
                              child: CustomText(
                                text: "Course Title",
                                weight: FontWeight.bold,
                              )),
                          const SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            controller: _title,
                            decoration: const InputDecoration(
                              hintText: 'Course Title Text...',
                              border: OutlineInputBorder(),
                            ),
                          ),
                        ],
                      )),
                  const SizedBox(height: 30),
                  // Course Description
                  Column(
                    children: [
                      const Align(
                          alignment: Alignment.topLeft,
                          child: CustomText(
                            text: "Course Description",
                            weight: FontWeight.bold,
                          )),
                      const SizedBox(
                        height: 10,
                      ),
                      Align(
                          alignment: Alignment.topLeft, child: _buildToolbar()),
                      Stack(
                        alignment: Alignment.topRight,
                        children: [
                          TextFormField(
                            maxLines: 4,
                            controller: _description,
                            decoration: const InputDecoration(
                              hintText: 'Course Description Text...',
                              border: OutlineInputBorder(),
                            ),
                          ),
                          Tooltip(
                            message: "This is a instruction",
                            child: IconButton(
                              hoverColor: Colors.transparent,
                              icon: const Icon(Icons.info),
                              onPressed: () {},
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                  const SizedBox(height: 30),
                  // Price
                  Container(
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 150,
                          child: CustomText(
                            text: "Price",
                            weight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(width: 150),
                        Container(
                          margin: const EdgeInsets.only(right: 50),
                          width: 150,
                          child: TextFormField(
                            controller: _price,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                            ),
                          ),
                        ),
                        const SizedBox(width: 10),
                        Checkbox(
                          value: true,
                          onChanged: (bool? newValue) {
                            setState(() {
                              // _isChecked = newValue!;
                            });
                          },
                        ),
                        const SizedBox(width: 10),
                        const CustomText(text: "Subcribers Only"),
                        const SizedBox(width: 10),
                        Tooltip(
                          message: "This is a instruction",
                          child: IconButton(
                            hoverColor: Colors.transparent,
                            icon: const Icon(Icons.info),
                            onPressed: () {},
                          ),
                        )
                      ],
                    ),
                  ),
                  const SizedBox(height: 30),
                  // Course Image
                  Container(
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    child: Row(
                      children: [
                        const CustomText(
                          text: "Course Image",
                          weight: FontWeight.bold,
                        ),
                        const SizedBox(width: 200),
                        Container(
                          margin: const EdgeInsets.only(right: 50),
                          width: 200,
                          height: 200,
                          child: uint8Image.isEmpty
                              ? Image.network(imageUrl)
                              : Image.memory(Uint8List.fromList(uint8Image)),
                        ),
                        const SizedBox(width: 10),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ElevatedButton(
                              onPressed: () {
                                // Add your button action here
                                uploadImage();
                              },
                              child: const CustomText(text: 'Choose'),
                            ),
                            const SizedBox(height: 10),
                            const CustomText(
                              text: "1280 x 720 pixels",
                              size: 14,
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 30),
                  Container(
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          width: 150,
                          child: CustomText(
                            text: "Who can view this course?",
                            weight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(width: 150),
                        SizedBox(
                          width: 800,
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                RadioListTile(
                                  title: const CustomText(
                                      text:
                                          'Anyone - Allow anyone to see this course'),
                                  value: 1,
                                  groupValue: courseAccessOption,
                                  onChanged: (value) {
                                    setCourseAccessOption(value!);
                                  },
                                ),
                                RadioListTile(
                                  title: const CustomText(
                                      text:
                                          "Only people who follow me - Only allow Followers to see this Course"),
                                  value: 2,
                                  groupValue: courseAccessOption,
                                  onChanged: (value) {
                                    setCourseAccessOption(value!);
                                  },
                                ),
                                RadioListTile(
                                  title: const CustomText(
                                      text:
                                          "Only Followers with a password - Protect this Course with a password"),
                                  value: 3,
                                  groupValue: courseAccessOption,
                                  onChanged: (value) {
                                    setCourseAccessOption(value!);
                                  },
                                ),
                                if (courseAccessOption == 3)
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    margin: const EdgeInsets.only(left: 70),
                                    child: Stack(
                                      alignment: Alignment.centerRight,
                                      children: [
                                        SizedBox(
                                          width: 300,
                                          child: TextFormField(
                                            decoration: const InputDecoration(
                                              hintText: 'Input password here',
                                              border: OutlineInputBorder(),
                                            ),
                                          ),
                                        ),
                                        Tooltip(
                                          message: "This is a instruction",
                                          child: IconButton(
                                            hoverColor: Colors.transparent,
                                            icon: const Icon(Icons.info),
                                            onPressed: () {},
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                              ]),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )),
        Expanded(
            flex: 1,
            child: Column(
              children: [
                Row(
                  children: [
                    CustomText(
                      text: coursesController.updateCourseStatus.value
                          ? "Status: Published"
                          : "Status: Draft",
                      weight: FontWeight.bold,
                    ),
                    Expanded(child: Container()),
                    Tooltip(
                      message: "This is a instruction",
                      child: IconButton(
                        hoverColor: Colors.transparent,
                        icon: const Icon(Icons.info),
                        onPressed: () {},
                      ),
                    )
                  ],
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 150,
                      height: 50,
                      child: OutlinedButton(
                        onPressed: () {
                          // Add your button action here
                          saveDraft();
                        },
                        child: const CustomText(text: 'Save Draft'),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    SizedBox(
                      width: 120,
                      height: 50,
                      child: OutlinedButton(
                        onPressed: () {
                          // Add your button action here
                          saveDraft();
                        },
                        child: const CustomText(text: 'Confirm'),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 40,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Difficulty
                    const Align(
                        alignment: Alignment.topLeft,
                        child: CustomText(
                          text: 'Difficulty:',
                          weight: FontWeight.bold,
                        )),
                    DropdownButton<String>(
                      value: difficulty,
                      isExpanded: true,
                      onChanged: (String? newValue) {
                        setState(() {
                          difficulty = newValue!;
                        });
                      },
                      items: <String>[
                        'Select One',
                        'Beginner',
                        'Intermediate',
                        'Advance'
                      ]
                          .map<DropdownMenuItem<String>>(
                            (String value) => DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            ),
                          )
                          .toList(),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    // Subjet Language
                    const Align(
                        alignment: Alignment.topLeft,
                        child: CustomText(
                          text: 'Subject Language:',
                          weight: FontWeight.bold,
                        )),
                    DropdownButton<String>(
                      value: subjectLanguage,
                      isExpanded: true,
                      onChanged: (String? newValue) {
                        setState(() {
                          subjectLanguage = newValue!;
                        });
                      },
                      items: <String>[
                        'Select One',
                        'English (S)',
                        'Chinese (S)',
                        'Others (S)'
                      ]
                          .map<DropdownMenuItem<String>>(
                            (String value) => DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            ),
                          )
                          .toList(),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    // Instruct Language
                    const Align(
                        alignment: Alignment.topLeft,
                        child: CustomText(
                          text: 'Instruct Language:',
                          weight: FontWeight.bold,
                        )),
                    DropdownButton<String>(
                      value: instructLanguage,
                      isExpanded: true,
                      onChanged: (String? newValue) {
                        setState(() {
                          instructLanguage = newValue!;
                        });
                      },
                      items: <String>[
                        'Select One',
                        'English (I)',
                        'Chinese (I)',
                        'Others (I)'
                      ]
                          .map<DropdownMenuItem<String>>(
                            (String value) => DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            ),
                          )
                          .toList(),
                    ),
                    const SizedBox(
                      height: 20,
                    ),

                    // Theme
                    const Align(
                        alignment: Alignment.topLeft,
                        child: CustomText(
                          text: 'Theme:',
                          weight: FontWeight.bold,
                        )),
                    DropdownButton<String>(
                      value: theme,
                      isExpanded: true,
                      onChanged: (String? newValue) {
                        setState(() {
                          theme = newValue!;
                        });
                      },
                      items: <String>[
                        'Select One',
                        'Linguistics',
                        'Travel',
                        'Math',
                        'Coaching'
                      ]
                          .map<DropdownMenuItem<String>>(
                            (String value) => DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            ),
                          )
                          .toList(),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                // Ratings
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const CustomText(text: "Ratings", weight: FontWeight.bold),
                    const SizedBox(width: 50),
                    const Icon(Icons.star, color: Colors.yellow),
                    const CustomText(text: "--- (0)"),
                    Expanded(child: Container()),
                    OutlinedButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey[200])),
                      onPressed: null,
                      child: const CustomText(text: "View"),
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const CustomText(
                        text: "New Alert Report", weight: FontWeight.bold),
                    const SizedBox(width: 50),
                    const CustomText(text: "---"),
                    Expanded(child: Container()),
                    OutlinedButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey[200])),
                      onPressed: null,
                      child: const CustomText(text: "View"),
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                DottedBorder(
                  borderType: BorderType.Rect,
                  color: Colors.black,
                  strokeWidth: 2,
                  dashPattern: const [5, 5],
                  child: SizedBox(
                    width: 400,
                    height: 400,
                    child: ListView(
                      children: const [
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: CustomText(
                            text: "Action needed",
                            weight: FontWeight.bold,
                          ),
                        ),
                        ListTile(
                          leading: Icon(Icons.circle, size: 20),
                          title: Text(
                              "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."),
                        ),
                        ListTile(
                          leading: Icon(Icons.circle, size: 20),
                          title: Text('Item 2'),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            )),
      ],
    );
  }

  Widget _buildToolbar() {
    return Container(
      padding: EdgeInsets.all(8.0),
      color: light,
      width: 200,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
            hoverColor: Colors.transparent,
            icon: const Icon(Icons.format_bold),
            onPressed: () {
              setState(() {
                _isBold = !_isBold;
              });
            },
            color: _isBold ? Colors.blue : Colors.black,
          ),
          IconButton(
            hoverColor: Colors.transparent,
            icon: const Icon(Icons.format_underlined),
            onPressed: () {
              setState(() {
                _isUnderlined = !_isUnderlined;
              });
            },
            color: _isUnderlined ? Colors.blue : Colors.black,
          ),
          IconButton(
            hoverColor: Colors.transparent,
            icon: const Icon(Icons.format_italic),
            onPressed: () {
              setState(() {
                _isItalic = !_isItalic;
              });
            },
            color: _isItalic ? Colors.blue : Colors.black,
          ),
        ],
      ),
    );
  }
}
