import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:flutter/material.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/models/request/login_request.dart';
import 'package:redtutor_flutter_webapp/pages/books/widgets/favourite_tab.dart';
import 'package:redtutor_flutter_webapp/pages/books/widgets/library_tab.dart';
import 'package:redtutor_flutter_webapp/pages/books/widgets/my_shelf_tab.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

class BooksPage extends StatefulWidget {
  const BooksPage({super.key});

  @override
  State<BooksPage> createState() => _BooksPageState();
}

class _BooksPageState extends State<BooksPage> {
  void resetState() {
    ebooksController.changeAddNewStatusTo(false);
  }

  @override
  void initState() {
    authController.getToken(LoginRequest("admin.redtutor@gmail.com", "123456"));
    print('books.dart:16');
    ebooksController.fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //return const EbooksTable();
    return Container(
      padding: const EdgeInsets.only(top: 48),
      child: DefaultTabController(
        length: 3,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              color: const Color(0xFF079992),
              width: double.infinity,
              height: 48,
              child: Row(
                children: [
                  const SizedBox(
                    width: 32,
                  ),
                  ButtonsTabBar(
                    duration: 0,
                    backgroundColor: Colors.transparent,
                    unselectedBackgroundColor: Colors.transparent,
                    borderWidth: 1,
                    borderColor: Colors.white,
                    unselectedBorderColor: Colors.transparent,
                    onTap: (p0) {
                      resetState();
                    },
                    tabs: [
                      Tab(
                        child: Container(
                          margin: const EdgeInsets.all(0),
                          width: 150,
                          child: const Center(
                            child: CustomText(
                              text: "Library",
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Tab(
                        child: Container(
                          margin: const EdgeInsets.all(0),
                          width: 150,
                          child: const Center(
                            child: CustomText(
                              text: "My Shelf",
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Tab(
                        child: Container(
                          margin: const EdgeInsets.all(0),
                          width: 150,
                          child: const Center(
                            child: CustomText(
                              text: "Favourites",
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            //const SizedBox(height: 16),
            const Expanded(
              child: TabBarView(
                children: <Widget>[LibraryTab(), MyShelfTab(), FavouriteTab()],
                //children: <Widget>[LibraryTab()],
                //children: <Widget>[LibraryTab(), LibraryTab(), LibraryTab()],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
