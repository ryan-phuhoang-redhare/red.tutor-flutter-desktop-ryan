// import 'package:flutter/material.dart';
// import 'package:red_tutor_flutter_app/app/di.dart';
// import 'package:red_tutor_flutter_app/presentation/ebook_player/ebook_player.dart';
// import 'package:red_tutor_flutter_app/presentation/main_layout/main_layout_viewmodel.dart';
// import 'package:red_tutor_flutter_app/presentation/resources/route_manager.dart';

// import '../../common/button.dart';

// class StartReadingButton extends StatelessWidget {
//   const StartReadingButton({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return Positioned(
//       bottom: 0.0,
//       left: 0.0,
//       right: 0.0,
//       child: Container(
//         height: kToolbarHeight + 20.0,
//         color: Colors.transparent,
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             Button(
//               onPressed: () {
//                 instance<MainLayoutViewmodel>().pushScreen(EbookPlayerScreen());
//               },
//               label: "Start Reading",
//               iconData: Icons.book,
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
