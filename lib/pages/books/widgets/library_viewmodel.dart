// import 'dart:async';

// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:red_tutor_flutter_app/data/states/selected_ebook_state.dart';

// import '../../domain/models/ebook.dart';
// import '../../domain/usecase/fetch_ebooks_usecase.dart';
// import '../../domain/usecase/logout_usecase.dart';
// import '../base/base_viewmodel.dart';
// import '../common/state_renderer.dart';
// import '../common/state_renderer_impl.dart';
// import '../resources/route_manager.dart';

// class LibraryViewModel extends BaseViewModel {
//   final FetchEbookUseCase _fetchEbookUseCase;
//   final LogoutUseCase _logoutUseCase;

//   LibraryViewModel(this._logoutUseCase, this._fetchEbookUseCase);

//   @override
//   void start() {
//     super.start();
//     fetchEbookList();
//   }

//   @override
//   void dispose() {
//     _ebookListStreamController.close();
//     super.dispose();
//   }

//   // List ebook
//   final StreamController<List<Ebook>?> _ebookListStreamController =
//       StreamController.broadcast();
//   Sink<List<Ebook>?> get ebookListSink => _ebookListStreamController.sink;
//   Stream<List<Ebook>?> get outputEbookList => _ebookListStreamController.stream;

//   Future<void> fetchEbookList() async {
//     inputState.add(
//         LoadingState(stateRendererType: StateRendererType.POPUP_LOADING_STATE));
//     final fetchEbookResult = await _fetchEbookUseCase.execute();

//     fetchEbookResult.fold(
//       (failure) {
//         inputState.add(ErrorState(
//             StateRendererType.FULL_SCREEN_ERROR_STATE, failure.message));
//         ebookListSink.add([]);
//       },
//       (data) {
//         ebookListSink.add(data);
//         refreshUIContent();
//       },
//     );
//   }

//   logOut(BuildContext context) {
//     _logoutUseCase.execute().then((_) {
//       Navigator.pushNamed(context, Routes.loginRoute);
//     });
//   }

//   double getImgHeight(BuildContext context) {
//     final Orientation orientation = MediaQuery.of(context).orientation;
//     return orientation == Orientation.portrait ? 240.0 : 480.0;
//   }

//   void onEbookTap(BuildContext context, Ebook ebook) {
//     final ebookState = context.read<SelectedEbookState>();
//     // _cache.(ebook);
//     ebookState.set(ebook);
//     Navigator.pushNamed(context, Routes.ebookDetail);
//   }
// }
