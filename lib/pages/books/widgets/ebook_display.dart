// import 'dart:developer';

// import 'package:flutter/material.dart';

// import '../../../app/constants.dart';
// import '../../../app/di.dart';
// import '../../../domain/models/ebook.dart';
// import '../../../models/book/ebook.dart';
// import '../../resources/assets_manager.dart';
// import '../../resources/values_manager.dart';
// import '../library_viewmodel.dart';

// class EBookDisplay extends StatefulWidget {
//   const EBookDisplay({super.key, required this.ebook});
//   final Ebook ebook;

//   // String get ebookCoverImgUrl => "https://i.ibb.co/VpVzYky/62f1d5aef297.png";
//   String get ebookCoverImgUrl =>
//       "https://images.wallpaperscraft.com/image/single/book_reading_inspiration_113238_1600x900.jpg";
//   @override
//   State<EBookDisplay> createState() => _EBookDisplayState();
// }

// class _EBookDisplayState extends State<EBookDisplay> {
//   final _viewModel = instance<LibraryViewModel>();
//   final double padding = 20;

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         _viewModel.onEbookTap(context, widget.ebook);
//       },
//       child: Container(
//         padding: EdgeInsets.all(padding),
//         color: const Color.fromARGB(255, 243, 245, 248),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Image.network(
//               widget.ebookCoverImgUrl,
//               height: _viewModel.getImgHeight(context),
//               width: MediaQuery.of(context).size.width - padding * 2,
//               errorBuilder: (context, error, stackTrace) {
//                 return Image.asset(ImageAssets.notFound);
//               },
//             ),
//             Container(
//               padding: const EdgeInsets.symmetric(vertical: 5),
//               child: Row(
//                 children: [
//                   const Icon(
//                     Icons.book,
//                     size: AppSize.s28,
//                   ),
//                   Text(
//                     widget.ebook.ebookTitle,
//                     style: Theme.of(context).textTheme.displayLarge,
//                   ),
//                 ],
//               ),
//             ),
//             Container(
//               padding: const EdgeInsets.symmetric(vertical: 5),
//               child: Row(
//                 children: [
//                   const Icon(Icons.person, size: AppSize.s28),
//                   Text(
//                     widget.ebook.authors.join(", "),
//                     style: Theme.of(context).textTheme.titleLarge,
//                   ),
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
