import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/models/book/ebook.dart';

class EbookFilterManager extends GetxController {
  static EbookFilterManager instance = Get.find();

  String ebookTitle = '';
  String difficulty = 'None';
  String language1 = 'None';
  String language2 = 'None';
  String mainTheme = 'None';
  String sortType = 'None';

  void setEbookTitleFilter(String ebookTitle) {
    this.ebookTitle = ebookTitle;
  }

  void setEbookDifficultyFilter(String difficulty) {
    this.difficulty = difficulty;
  }

  void setEbookLanguage1Filter(String language1) {
    this.language1 = language1;
    print('Filter language 1: $language1');
  }

  void setEbookLanguage2Filter(String language2) {
    this.language2 = language2;
    print('Filter language 2: $language2');
  }

  setEbookSortType(String sortType) {
    this.sortType = sortType;
  }

  void resetFilter() {
    ebookTitle = '';
    difficulty = 'None';
    language1 = 'None';
    language2 = 'None';
    mainTheme = 'None';
    sortType = 'None';
  }

  List<Ebook> sortEbookListBy(List<Ebook> listEbook, String sortType) {
    if (sortType == 'Title Ascending') {
      listEbook.sort(((a, b) => a.ebookTitle.compareTo(b.ebookTitle)));
    } else if (sortType == 'Title Descending') {
      listEbook.sort(((a, b) => b.ebookTitle.compareTo(a.ebookTitle)));
    } else if (sortType == 'Price Ascending') {
      listEbook.sort(((a, b) => a.price.compareTo(b.price)));
    } else if (sortType == 'Price Descending') {
      listEbook.sort(((a, b) => b.price.compareTo(a.price)));
    }
    return listEbook;
  }

  List<Ebook> getFilteredSortedEbooks() {
    List<Ebook> listFilteredSortedEbook = [];
    List<Ebook> listFilteredEbook = [];
    if (ebooksController.ebooksObs.isNotEmpty) {
      if (language1 != 'None' && language2 != 'None') {
        List<Ebook> listFilteredEbookBothLanguages =
            ebooksController.ebooksObs.where((ebook) {
          final bool doesEbookTitleMatch =
              ebookTitle == '' ? true : ebook.ebookTitle.contains(ebookTitle);
          final bool doesCourseDifficultyMatch = difficulty == 'None'
              ? true
              : ebook.description.contains(difficulty);
          bool doEbookLanguagesMatch = ebook.languages.contains(language1) &&
              ebook.languages.contains(language2);

          final bool doesMainThemeMatch =
              mainTheme == 'None' ? true : ebook.genres.contains(mainTheme);

          return doesEbookTitleMatch &&
              doesCourseDifficultyMatch &&
              doEbookLanguagesMatch &&
              doesMainThemeMatch;
        }).toList();
        listFilteredEbookBothLanguages =
            sortEbookListBy(listFilteredEbookBothLanguages, this.sortType);

        List<Ebook> listFilteredEbookOnlyLanguage1 =
            ebooksController.ebooksObs.where((ebook) {
          final bool doesEbookTitleMatch =
              ebookTitle == '' ? true : ebook.ebookTitle.contains(ebookTitle);
          final bool doesCourseDifficultyMatch = difficulty == 'None'
              ? true
              : ebook.description.contains(difficulty);
          bool doEbookLanguagesMatch = ebook.languages.contains(language1) &&
              !ebook.languages.contains(language2);

          final bool doesMainThemeMatch =
              mainTheme == 'None' ? true : ebook.genres.contains(mainTheme);

          return doesEbookTitleMatch &&
              doesCourseDifficultyMatch &&
              doEbookLanguagesMatch &&
              doesMainThemeMatch;
        }).toList();
        listFilteredEbookOnlyLanguage1 =
            sortEbookListBy(listFilteredEbookOnlyLanguage1, this.sortType);

        List<Ebook> listFilteredEbookOnlyLanguage2 =
            ebooksController.ebooksObs.where((ebook) {
          final bool doesEbookTitleMatch =
              ebookTitle == '' ? true : ebook.ebookTitle.contains(ebookTitle);
          final bool doesCourseDifficultyMatch = difficulty == 'None'
              ? true
              : ebook.description.contains(difficulty);
          bool doEbookLanguagesMatch = !ebook.languages.contains(language1) &&
              ebook.languages.contains(language2);

          final bool doesMainThemeMatch =
              mainTheme == 'None' ? true : ebook.genres.contains(mainTheme);

          return doesEbookTitleMatch &&
              doesCourseDifficultyMatch &&
              doEbookLanguagesMatch &&
              doesMainThemeMatch;
        }).toList();
        listFilteredEbookOnlyLanguage2 =
            sortEbookListBy(listFilteredEbookOnlyLanguage2, this.sortType);

        listFilteredEbook.addAll(listFilteredEbookBothLanguages);
        listFilteredEbook.addAll(listFilteredEbookOnlyLanguage1);
        listFilteredEbook.addAll(listFilteredEbookOnlyLanguage2);

        listFilteredSortedEbook = listFilteredEbook;
      } else {
        listFilteredEbook = ebooksController.ebooksObs.where((ebook) {
          final bool doesEbookTitleMatch =
              ebookTitle == '' ? true : ebook.ebookTitle.contains(ebookTitle);
          final bool doesCourseDifficultyMatch = difficulty == 'None'
              ? true
              : ebook.description.contains(difficulty);

          bool doEbookLanguagesMatch = false;
          if (language1 == 'None' && language2 == 'None') {
            doEbookLanguagesMatch = true;
          } else if (language1 != 'None' && language2 == 'None') {
            doEbookLanguagesMatch = ebook.languages.contains(language1);
          } else if (language1 == 'None' && language2 != 'None') {
            doEbookLanguagesMatch = ebook.languages.contains(language2);
          }
          // else if (language1 != 'None' && language2 != 'None') {
          //   doEbookLanguagesMatch = ebook.languages.contains(language1) ||
          //       ebook.languages.contains(language2);
          // }
          final bool doesMainThemeMatch =
              mainTheme == 'None' ? true : ebook.genres.contains(mainTheme);

          return doesEbookTitleMatch &&
              doesCourseDifficultyMatch &&
              doEbookLanguagesMatch &&
              doesMainThemeMatch;
        }).toList();
        listFilteredSortedEbook =
            sortEbookListBy(listFilteredEbook, this.sortType);
      }
    }
    return listFilteredSortedEbook;
  }
}
