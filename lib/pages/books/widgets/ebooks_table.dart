import 'package:flutter/material.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/models/book/ebook.dart';
import 'package:redtutor_flutter_webapp/pages/books/widgets/ebook_search_filter_bar.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

class EbooksTable extends StatefulWidget {
  //const EbooksTable({super.key});
  final ValueChanged<bool> updateIsTableView;
  const EbooksTable({super.key, required this.updateIsTableView});

  @override
  State<EbooksTable> createState() => _EbooksTableState();
}

class _EbooksTableState extends State<EbooksTable> {
  // Future<void> saveSelectedEbook(int index) async {
  //   var ebookSelected = ebooksController.ebooksObs[index];
  //   print('table 16');
  //   await ebooksController.selectedEbook(ebookSelected);
  // }

  List<Ebook> listEbookToShow = ebooksController.listEbookFiltered.isEmpty
      ? ebooksController.ebooksObs
      : ebooksController.listEbookFiltered;

  void saveSelectedEbook(int index) {
    ebooksController.selectedEbookIndex = index;
    //ebooksController.selectedEbook = ebooksController.ebooksObs[index];
    ebooksController.selectedEbook = listEbookToShow[index];
  }

  void updateListEbookToShow() {
    listEbookToShow = ebooksController.listEbookFiltered;
  }

  void refresh() {
    setState(() {
      updateListEbookToShow();
    });
  }

  @override
  void initState() {
    ebooksController.fetchData();
    //updateListEbookToShow();
    listEbookToShow = ebooksController.listEbookFiltered.isEmpty
        ? ebooksController.ebooksObs
        : ebooksController.listEbookFiltered;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 64,
          child: EbookSearchFilterBar(notifyParent: refresh),
        ),
        Expanded(
          child: GridView.count(
            shrinkWrap: true,
            crossAxisCount: 3,
            childAspectRatio: 1.0,
            padding: const EdgeInsets.only(
              left: 32,
              right: 32,
              top: 16,
              bottom: 16,
            ),
            mainAxisSpacing: 32,
            crossAxisSpacing: 32,
            children: List.generate(
              listEbookToShow.length,
              (index) {
                return InkWell(
                  onTap: () async {
                    saveSelectedEbook(index);
                    widget.updateIsTableView(false);
                  },
                  child: Container(
                    width: 130,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white,
                    ),
                    child: Center(
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.all(16),
                            child: const Image(
                                image: AssetImage(
                                    'assets/images/book_coffee.jpg')),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(left: 16),
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: CustomText(
                                    text: listEbookToShow[index].ebookTitle,
                                    weight: FontWeight.bold,
                                    color: const Color(0xFF3F1762),
                                  ))),
                          Padding(
                            padding: const EdgeInsets.only(left: 16),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: CustomText(
                                text: listEbookToShow[index].authors.join(', '),
                                weight: FontWeight.normal,
                                color: const Color(0xFF3F1762),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
