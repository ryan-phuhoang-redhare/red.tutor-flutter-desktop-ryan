import 'package:flutter/material.dart';
import 'package:redtutor_flutter_webapp/pages/books/widgets/ebook_details.dart';
import 'package:redtutor_flutter_webapp/pages/books/widgets/ebooks_table.dart';

class LibraryTab extends StatefulWidget {
  const LibraryTab({super.key});

  @override
  State<LibraryTab> createState() => _LibraryTabState();
}

class _LibraryTabState extends State<LibraryTab> {
  @override
  void initState() {
    super.initState();
  }

  bool _isTablePage = true;

  void _update(bool isTablePage) {
    setState(() => _isTablePage = isTablePage);
  }

  @override
  Widget build(BuildContext context) {
    // return const Expanded(
    //   child: EbooksTable(),
    // );
    return Expanded(
      child: AnimatedCrossFade(
        duration: const Duration(seconds: 0),
        firstChild: EbooksTable(updateIsTableView: _update),
        secondChild: EbookDetails(update: _update),
        crossFadeState:
            _isTablePage ? CrossFadeState.showFirst : CrossFadeState.showSecond,
      ),
    );
  }
}
