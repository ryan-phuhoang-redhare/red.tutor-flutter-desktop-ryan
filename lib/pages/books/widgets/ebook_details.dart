import 'package:flutter/material.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';

class EbookDetails extends StatefulWidget {
  final ValueChanged<bool> update;
  const EbookDetails({super.key, required this.update});

  @override
  State<EbookDetails> createState() => _EbookDetailsState();
}

class _EbookDetailsState extends State<EbookDetails> {
  //final _viewModel = instance<EbookDetailsViewModel>();
  //Ebook? selectedEbook;

  @override
  void initState() {
    print('details 18');
    //loadSelectedEbook();
    super.initState();
  }

  // void loadSelectedEbook() async {
  //   selectedEbook =
  //       ebooksController.ebooksObs[ebooksController.selectedEbookIndex];
  //   print('Ebook Index: ${ebooksController.selectedEbookIndex}');
  // }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: double.infinity,
        child: Row(children: [
          Expanded(
            flex: 7,
            child: Column(
              children: [
                Container(
                  //width: MediaQuery.of(context).size.width * 0.65,
                  padding: const EdgeInsets.all(0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(4),
                    ),
                    padding: const EdgeInsets.all(8),
                    child: Row(
                      children: [
                        Expanded(
                            flex: 9,
                            child: Text(
                              ebooksController.selectedEbook!.ebookTitle,
                              overflow: TextOverflow.visible,
                              style: const TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                        SizedBox(
                          //color: Colors.red,
                          height: 50,
                          width: 50,
                          child: InkWell(
                            onTap: () {
                              //Back to ebooks table
                              widget.update(true);
                            },
                            child: const Icon(
                              Icons.arrow_circle_left,
                              color: Color(0xFF079992),
                              size: 50,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  // decoration: BoxDecoration(
                  //   color: Colors.white,
                  //   borderRadius: BorderRadius.circular(4),
                  // ),
                  padding: const EdgeInsets.only(top: 8, bottom: 8),
                  //width: MediaQuery.of(context).size.width * 0.5,
                  width: double.infinity,
                  //height: MediaQuery.of(context).size.width * 0.5 / 16 * 9,
                  child: Image(
                      alignment: Alignment.centerLeft,
                      width: MediaQuery.of(context).size.width * 0.5,
                      height: MediaQuery.of(context).size.width * 0.5 / 16 * 9,
                      image: const AssetImage('assets/images/book_coffee.jpg')),
                ),
                Container(
                    padding: const EdgeInsets.all(0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Container(
                        padding: const EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      'Authors: ${ebooksController.selectedEbook!.authors.join(', ')}',
                                      // textAlign: TextAlign.left
                                    )),
                                Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      'Publisher: ${ebooksController.selectedEbook!.publisher.publisherName}',
                                      // textAlign: TextAlign.left
                                    ))
                              ],
                            )
                          ],
                        ))),
                // Container(
                //   padding: const EdgeInsets.all(8),
                //   alignment: Alignment.centerLeft,
                //   child: Container(
                //     decoration: BoxDecoration(
                //       color: Color.fromARGB(255, 218, 194, 133),
                //       borderRadius: BorderRadius.circular(20),
                //     ),
                //     padding: EdgeInsets.all(10),
                //     child: Text(
                //       selectedEbook!.authors.join(', '),
                //       style: const TextStyle(
                //         fontWeight: FontWeight.bold,
                //       ),
                //     ),
                //   ),
                // ),
                // Container(
                //   padding: const EdgeInsets.all(5),
                //   alignment: Alignment.centerLeft,
                //   child: const Text(
                //     "Description:",
                //     textScaleFactor: 1.1,
                //     style: TextStyle(
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                // ),
                // Container(
                //   padding: const EdgeInsets.all(8),
                //   alignment: Alignment.centerLeft,
                //   child: Text(
                //     ebooksController.ebooksObs[0].description,
                //   ),
                // ),
                // Container(
                //   padding: const EdgeInsets.all(5),
                //   alignment: Alignment.centerLeft,
                //   child: const Text(
                //     "Book Information:",
                //     textScaleFactor: 1.1,
                //     style: TextStyle(
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                // ),
                // Container(
                //   padding: const EdgeInsets.all(5),
                //   child: Column(
                //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //     children: [
                //       Row(
                //         children: [
                //           Expanded(
                //             flex: 3,
                //             child: Text(
                //               "Publisher",
                //               textAlign: TextAlign.right,
                //             ),
                //           ),
                //           Expanded(
                //             flex: 1,
                //             child: Text(
                //               ":\t",
                //               textAlign: TextAlign.center,
                //             ),
                //           ),
                //           Expanded(
                //             flex: 6,
                //             child: Row(
                //               children: [
                //                 Container(
                //                   decoration: BoxDecoration(
                //                     color: Color.fromARGB(255, 218, 194, 133),
                //                     borderRadius: BorderRadius.circular(20),
                //                   ),
                //                   padding: EdgeInsets.all(10),
                //                   child: Text(
                //                     ebooksController
                //                         .ebooksObs[0].publisher.publisherName,
                //                     style: TextStyle(
                //                         fontWeight: FontWeight.bold),
                //                   ),
                //                 ),
                //               ],
                //             ),
                //           )
                //         ],
                //       ),
                //       Container(
                //         height: 10,
                //         color: Colors.transparent,
                //       ),
                //       // FlexBox(
                //       //   flex: [3, 1, 6],
                //       //   crossAxisAlignment: CrossAxisAlignment.center,
                //       //   isHorizontal: true,
                //       //   children: [
                //       //     const Text(
                //       //       "Language",
                //       //       textAlign: TextAlign.right,
                //       //     ),
                //       //     const Text(
                //       //       ":",
                //       //       textAlign: TextAlign.center,
                //       //     ),
                //       //     Row(
                //       //       children: [
                //       //         Container(
                //       //           decoration: BoxDecoration(
                //       //             color: Color.fromARGB(255, 218, 194, 133),
                //       //             borderRadius: BorderRadius.circular(20),
                //       //           ),
                //       //           padding: EdgeInsets.all(10),
                //       //           child: Text(
                //       //             "English",
                //       //             style: TextStyle(fontWeight: FontWeight.bold),
                //       //           ),
                //       //         ),
                //       //       ],
                //       //     ),
                //       //   ],
                //       // ),
                //       // Container(
                //       //   height: 10,
                //       //   color: Colors.transparent,
                //       // ),
                //       // FlexBox(
                //       //   crossAxisAlignment: CrossAxisAlignment.center,
                //       //   isHorizontal: true,
                //       //   flex: const [3, 1, 6],
                //       //   children: [
                //       //     const Text(
                //       //       "Learning level",
                //       //       textAlign: TextAlign.right,
                //       //     ),
                //       //     const Text(
                //       //       ":",
                //       //       textAlign: TextAlign.center,
                //       //     ),
                //       //     Row(
                //       //       children: [
                //       //         Container(
                //       //           decoration: BoxDecoration(
                //       //             color: Color.fromARGB(255, 218, 194, 133),
                //       //             borderRadius: BorderRadius.circular(20),
                //       //           ),
                //       //           padding: EdgeInsets.all(10),
                //       //           child: Text(
                //       //             "Elementary",
                //       //             style: TextStyle(fontWeight: FontWeight.bold),
                //       //           ),
                //       //         ),
                //       //       ],
                //       //     ),
                //       //   ],
                //       // ),
                //       // Container(
                //       //   height: 10,
                //       //   color: Colors.transparent,
                //       // ),
                //       // FlexBox(
                //       //   flex: [3, 1, 6],
                //       //   crossAxisAlignment: CrossAxisAlignment.center,
                //       //   isHorizontal: true,
                //       //   children: [
                //       //     Text(
                //       //       "Release Date",
                //       //       textAlign: TextAlign.right,
                //       //     ),
                //       //     Text(
                //       //       ":",
                //       //       textAlign: TextAlign.center,
                //       //     ),
                //       //     Row(
                //       //       children: [
                //       //         Container(
                //       //           decoration: BoxDecoration(
                //       //             color: ColorManager.white,
                //       //             borderRadius: BorderRadius.circular(20),
                //       //           ),
                //       //           padding: EdgeInsets.all(10),
                //       //           child: Text(
                //       //             "Nov 3 2021",
                //       //             style: TextStyle(fontWeight: FontWeight.bold),
                //       //           ),
                //       //         ),
                //       //       ],
                //       //     ),
                //       //   ],
                //       // ),
                //     ],
                //   ),
                // ),
                // Container(
                //   padding: const EdgeInsets.all(5),
                //   alignment: Alignment.centerLeft,
                //   child: const Text(
                //     "Tags:",
                //     textScaleFactor: 1.1,
                //     style: TextStyle(
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                // ),
                // Container(
                //   padding: const EdgeInsets.all(8),
                //   height: 50,
                //   child: Row(
                //     mainAxisAlignment: MainAxisAlignment.start,
                //     children: [
                //       Container(
                //         decoration: BoxDecoration(
                //           color: Color.fromARGB(255, 218, 194, 133),
                //           borderRadius: BorderRadius.circular(20),
                //         ),
                //         padding: EdgeInsets.all(10),
                //         child: Text(
                //           "Children's book",
                //           style: TextStyle(
                //             fontWeight: FontWeight.bold,
                //           ),
                //         ),
                //       )
                //     ],
                //   ),
                // ),
                // Container(
                //   padding: const EdgeInsets.all(5),
                //   alignment: Alignment.centerLeft,
                //   child: const Text(
                //     "Ratings:",
                //     textScaleFactor: 1.1,
                //     style: TextStyle(
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                // ),
                // Container(
                //   padding: const EdgeInsets.all(5),
                //   alignment: Alignment.center,
                //   child: FlexBox(
                //     flex: const [1, 1, 1, 1, 1],
                //     isHorizontal: true,
                //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //     children: [
                //       EbookStar(onPressed: () {}, isActive: true),
                //       EbookStar(onPressed: () {}, isActive: true),
                //       EbookStar(onPressed: () {}, isActive: true),
                //       EbookStar(onPressed: () {}, isActive: true),
                //       EbookStar(onPressed: () {}, isActive: false),
                //     ],
                //   ),
                // ),
              ],
            ),
          ),
          const SizedBox(
            width: 16,
          ),
          Expanded(
            flex: 2,
            child: Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.only(
                      top: 16, left: 16, right: 16, bottom: 16),
                  //height: MediaQuery.of(context).size.height * 0.4,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: Column(
                    children: [
                      OutlinedButton.icon(
                        style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4),
                                    side:
                                        const BorderSide(color: Colors.grey))),
                            minimumSize: const MaterialStatePropertyAll(
                                Size.fromHeight(50))),
                        onPressed: () {},
                        icon: const Icon(
                          Icons.diamond_sharp,
                          color: Colors.black,
                        ),
                        label: const Text('999'),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      OutlinedButton.icon(
                        style: ButtonStyle(
                          side: MaterialStateProperty.all(
                            const BorderSide(
                              color: Color(0xFF079992),
                            ),
                          ),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(4),
                              // side: const BorderSide(
                              //   color: Color(0xFF079992),
                              // ),
                            ),
                          ),
                          minimumSize: const MaterialStatePropertyAll(
                              Size.fromHeight(60)),
                          backgroundColor: const MaterialStatePropertyAll(
                            Color(0xFF079992),
                          ),
                        ),
                        onPressed: () {},
                        icon: const Icon(
                          Icons.cases_sharp,
                          color: Colors.white,
                        ),
                        label: const Text(
                          'Acquire',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      OutlinedButton.icon(
                        style: ButtonStyle(
                            side: MaterialStateProperty.all(
                              const BorderSide(
                                color: Color(0xFF079992),
                              ),
                            ),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(4),
                              ),
                            ),
                            minimumSize: const MaterialStatePropertyAll(
                                Size.fromHeight(50))),
                        onPressed: () {},
                        icon: const Icon(
                          Icons.favorite_outline,
                          color: Color(0xFF079992),
                        ),
                        label: const Text(
                          'Add to Favourite',
                          style: TextStyle(
                            color: Color(0xFF079992),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      OutlinedButton.icon(
                        style: ButtonStyle(
                            side: MaterialStateProperty.all(
                              const BorderSide(
                                color: Color(0xFF079992),
                              ),
                            ),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(4),
                              ),
                            ),
                            minimumSize: const MaterialStatePropertyAll(
                                Size.fromHeight(50))),
                        onPressed: () {},
                        icon: const Icon(
                          Icons.remove_circle_outline,
                          color: Color(0xFF079992),
                        ),
                        label: const Text(
                          'Not Interested',
                          style: TextStyle(
                            color: Color(0xFF079992),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      OutlinedButton.icon(
                        style: ButtonStyle(
                            side: MaterialStateProperty.all(
                              const BorderSide(
                                color: Color(0xFF079992),
                              ),
                            ),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(4),
                              ),
                            ),
                            minimumSize: const MaterialStatePropertyAll(
                                Size.fromHeight(50))),
                        onPressed: () {},
                        icon: const Icon(
                          Icons.flag_outlined,
                          color: Color(0xFF079992),
                        ),
                        label: const Text(
                          'Report',
                          style: TextStyle(
                            color: Color(0xFF079992),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ]));
  }
}
