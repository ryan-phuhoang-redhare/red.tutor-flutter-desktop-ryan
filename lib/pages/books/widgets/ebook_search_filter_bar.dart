import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/models/book/ebook.dart';
import 'ebook_filter_manager.dart';

class EbookSearchFilterBar extends StatefulWidget {
  final Function() notifyParent;
  const EbookSearchFilterBar({super.key, required this.notifyParent});
  //CourseScreenViewModel get _viewModel => instance<CourseScreenViewModel>();

  // InputBorder _getSearchInputBorder() {
  //   return const OutlineInputBorder(
  //     borderRadius: BorderRadius.all(
  //       Radius.circular(8),
  //     ),
  //     borderSide: BorderSide(
  //       color: Colors.transparent,
  //     ),
  //   );
  // }

  @override
  State<StatefulWidget> createState() => EbookSearchFilterBarState();
}

class EbookSearchFilterBarState extends State<EbookSearchFilterBar> {
  //final _viewModel = instance<EbookDetailsViewModel>();
  //Ebook? selectedEbook;

  EbookFilterManager ebookFilter = Get.put(EbookFilterManager());
  List<String> listDifficulty = [];
  List<String> listLanguage = [];
  List<String> listMainTheme = [];

  String hintDropdownDifficulty = 'Difficulty';
  String hintDropdownLanguage1 = 'Language 1';
  String hintDropdownLanguage2 = 'Language 2';
  String hintDropdownMainTheme = 'Main Theme';
  String hintDropdownSortBy = 'Sorting By';

  void updateListEbookLanguage() {
    List<String> listAllEbookLanguage = ['None'];
    List<Ebook> listAllEbook = ebooksController.ebooksObs;
    if (listAllEbook.isNotEmpty) {
      var languageSets = listAllEbook.map((ebook) => ebook.languages);
      languageSets.forEach((set) {
        set.forEach((language) {
          listAllEbookLanguage.add(language);
        });
      });
      print(listAllEbookLanguage);
    }
    listLanguage = listAllEbookLanguage.toSet().toList();
  }

  void updateFilteredEbookList() async {
    ebooksController.listEbookFiltered = ebookFilter.getFilteredSortedEbooks();
    print(ebooksController.listEbookFiltered);
    widget.notifyParent();
  }

  @override
  void initState() {
    updateListEbookLanguage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //final courseListData = context.read<CourseListState>();
    return Container(
      height: 64,
      color: const Color(0xFFA8E0D4),
      alignment: Alignment.centerLeft,
      child: Row(
        children: [
          const SizedBox(
            width: 32,
          ),
          SizedBox(
            height: 40,
            width: 256,
            child: Stack(
              alignment: Alignment.centerRight,
              children: [
                SearchBar(
                  onChanged: (text) {
                    ebookFilter.setEbookTitleFilter(text);
                  },
                  shadowColor: MaterialStateProperty.all(Colors.transparent),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                      side: const BorderSide(
                        color: Colors.transparent,
                      ),
                    ),
                  ),
                  hintText: 'Search',
                ),
                Container(
                  padding: const EdgeInsets.symmetric(
                      //horizontal: AppPadding.p4,
                      ),
                  decoration: const BoxDecoration(
                    color: Color(0xFF079992),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(0.0), // Right top corner radius
                      bottomLeft:
                          Radius.circular(0.0), // Left bottom corner radius
                      topRight: Radius.circular(8.0), // Right top corner radius
                      bottomRight:
                          Radius.circular(8.0), // Right bottom corner radius
                    ),
                  ),
                  height: 40,
                  child: InkWell(
                    onTap: updateFilteredEbookList,
                    child: const Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
          ),
          const SizedBox(
            width: 16,
          ),
          Container(
            height: 40,
            width: 184,
            padding: const EdgeInsets.only(left: 8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.purple,
            ),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                dropdownColor: Colors.purple,
                items: <String>['None'].map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                    onTap: () => setState(() {
                      value == 'None'
                          ? hintDropdownDifficulty = 'Difficulty'
                          : hintDropdownDifficulty = value;
                    }),
                  );
                }).toList(),
                hint: SizedBox(
                  child: Text(
                    hintDropdownDifficulty,
                    style: const TextStyle(color: Colors.black),
                    textAlign: TextAlign.left,
                  ),
                ),
                onChanged: (String? value) => setState(() {}),
              ),
            ),
          ),
          const SizedBox(
            width: 8,
          ),
          Container(
            height: 40,
            width: 184,
            padding: const EdgeInsets.only(left: 8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.orange,
            ),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                dropdownColor: Colors.orange,
                items: listLanguage.map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                    onTap: () => setState(() {
                      value == 'None'
                          ? hintDropdownLanguage1 = 'Language 1'
                          : hintDropdownLanguage1 = value;
                      ebookFilter.setEbookLanguage1Filter(value);
                      updateFilteredEbookList();
                    }),
                  );
                }).toList(),
                hint: SizedBox(
                  width: 150,
                  child: Text(
                    hintDropdownLanguage1,
                    style: const TextStyle(color: Colors.black),
                    textAlign: TextAlign.left,
                  ),
                ),
                onChanged: (String? value) => setState(
                  () {
                    //updateFilteredEbookList;
                  },
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 8,
          ),
          Container(
            height: 40,
            width: 184,
            padding: const EdgeInsets.only(left: 8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.red,
            ),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                dropdownColor: Colors.red,
                items: listLanguage.map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                    onTap: () => setState(() {
                      value == 'None'
                          ? hintDropdownLanguage2 = 'Language 2'
                          : hintDropdownLanguage2 = value;
                      ebookFilter.setEbookLanguage2Filter(value);
                      updateFilteredEbookList();
                    }),
                  );
                }).toList(),
                hint: SizedBox(
                  width: 150,
                  child: Text(
                    hintDropdownLanguage2,
                    style: const TextStyle(color: Colors.black),
                    textAlign: TextAlign.left,
                  ),
                ),
                onChanged: (String? value) => setState(
                  () {
                    //updateFilteredEbookList;
                  },
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 8,
          ),
          Container(
            height: 40,
            width: 184,
            padding: const EdgeInsets.only(left: 8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.yellow,
            ),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                dropdownColor: Colors.yellow,
                items: <String>['None'].map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                    onTap: () => setState(() {
                      value == 'None'
                          ? hintDropdownMainTheme = 'Main Theme'
                          : hintDropdownMainTheme = value;
                    }),
                  );
                }).toList(),
                hint: SizedBox(
                  width: 150,
                  child: Text(
                    hintDropdownMainTheme,
                    style: const TextStyle(color: Colors.black),
                    textAlign: TextAlign.left,
                  ),
                ),
                onChanged: (String? value) => setState(() {}),
              ),
            ),
          ),
          const SizedBox(
            width: 8,
          ),
          Container(
            height: 40,
            width: 184,
            padding: const EdgeInsets.only(left: 8),
            decoration: BoxDecoration(
              border: Border.all(color: const Color(0xFF079992), width: 1.0),
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
            ),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                dropdownColor: Colors.white,
                items: <String>[
                  'None',
                  'Title Ascending',
                  'Title Descending',
                  'Price Ascending',
                  'Price Descending'
                ].map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(
                      value,
                      style: const TextStyle(color: Color(0xFF079992)),
                    ),
                    onTap: () => setState(() {
                      value == 'None'
                          ? hintDropdownSortBy = 'Sorting By'
                          : hintDropdownSortBy = value;
                      ebookFilter.setEbookSortType(value);
                      updateFilteredEbookList();
                    }),
                  );
                }).toList(),
                hint: SizedBox(
                  width: 150,
                  child: Text(
                    hintDropdownSortBy,
                    style: const TextStyle(color: Color(0xFF079992)),
                    textAlign: TextAlign.left,
                  ),
                ),
                onChanged: (String? value) => setState(() {}),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
