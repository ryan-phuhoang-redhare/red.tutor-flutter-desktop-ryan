import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/pages/activities/widgets/my_bots/widgets/add_new_bot.dart';
import 'package:redtutor_flutter_webapp/pages/activities/widgets/my_bots/widgets/bots_table.dart';

class MyBotsPage extends StatefulWidget {
  const MyBotsPage({super.key});

  @override
  State<MyBotsPage> createState() => _MyBotsPageState();
}

class _MyBotsPageState extends State<MyBotsPage> {
  @override
  Widget build(BuildContext context) {
    return Obx(() => Expanded(
          child: ListView(children: [
            if (activitiesController.addNewStatus.value == false)
              const BotsTablePage()
            else
              const AddNewBotPage()
          ]),
        ));
  }
}
