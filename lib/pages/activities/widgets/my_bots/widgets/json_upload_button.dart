import 'dart:convert';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';

class JsonUploadButton extends StatefulWidget {
  final String buttonName;
  const JsonUploadButton({super.key, required this.buttonName});

  @override
  State<JsonUploadButton> createState() => _JsonUploadButtonState();
}

class _JsonUploadButtonState extends State<JsonUploadButton> {
  String _selectedFileName = "";

  Future<void> _pickFile() async {
    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.custom,
        allowedExtensions: ['json'],
      );

      if (result != null) {
        // Handle the selected PDF file paths
        for (var file in result.files) {
          String jsonContent = utf8.decode(file.bytes!);
          activitiesController.botJson.value = jsonContent;
        }
        setState(() {
          _selectedFileName = result.files[0].name;
        });
      }
    } catch (e) {
      print("Error picking files: $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            ElevatedButton(
              onPressed: _pickFile,
              child: Text(widget.buttonName),
            ),
          ],
        ),
        const SizedBox(height: 20),
        if (_selectedFileName != "")
          Row(
            children: [
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(_selectedFileName),
                    IconButton(
                      icon: const Icon(Icons.delete),
                      onPressed: () {
                        print("Delete $_selectedFileName");
                        setState(() {
                          _selectedFileName = "";
                        });
                      },
                    ),
                  ],
                )
              ])
            ],
          ),
      ],
    );
  }
}
