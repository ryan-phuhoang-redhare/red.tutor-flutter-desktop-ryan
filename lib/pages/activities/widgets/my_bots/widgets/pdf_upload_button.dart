import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:dio/dio.dart' as dio;
import 'package:http_parser/http_parser.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

class PdfUploadButton extends StatefulWidget {
  const PdfUploadButton({super.key});

  @override
  State<PdfUploadButton> createState() => _PdfUploadButtonState();
}

class _PdfUploadButtonState extends State<PdfUploadButton> {
  final TextEditingController _collectionNameTextController =
      TextEditingController();
  List<String> _selectedFileNames = [];

  Future<void> _pickFiles() async {
    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.custom,
        allowedExtensions: ['pdf'],
      );

      if (result != null) {
        List<dio.MultipartFile> files = [];
        // Handle the selected PDF file paths
        for (var file in result.files) {
          // Convert each FilePickerResult file to a dio.MultipartFile
          files.add(
            dio.MultipartFile.fromBytes(
              file.bytes!,
              filename: file.name,
              contentType: MediaType('application', 'pdf'),
            ),
          );
          // Convert bytes to base64
          String base64String = base64Encode(file.bytes!);
          activitiesController.botDocument.value = base64String;
        }

        setState(() {
          _selectedFileNames = result.files.map((file) => file.name).toList();
        });
      }
    } catch (e) {
      print("Error picking files: $e");
    }
  }

  @override
  void dispose() {
    _collectionNameTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            ElevatedButton(
              onPressed: _pickFiles,
              child: const Text("Upload Document"),
            ),
            SizedBox(
              width: 60,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const CustomText(
                  text: "Collection Name",
                  weight: FontWeight.bold,
                ),
                SizedBox(
                  width: 300,
                  child: TextFormField(
                    controller: _collectionNameTextController,
                    onChanged: (text) {
                      // Update character count
                    },
                    decoration: const InputDecoration(
                      hintText: 'Collection Name Text...',
                      border: OutlineInputBorder(),
                    ),
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(
                          100), // Limit to 100 characters
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
        Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _selectedFileNames.map((fileName) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(fileName),
                    IconButton(
                      icon: const Icon(Icons.delete),
                      onPressed: () {
                        print("Delete $fileName");
                        setState(() {
                          _selectedFileNames.remove(fileName);
                        });
                      },
                    ),
                  ],
                );
              }).toList(),
            ),
          ],
        ),
      ],
    );
  }
}
