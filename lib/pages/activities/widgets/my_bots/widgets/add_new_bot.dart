import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/models/request/activity/bot_request.dart';
import 'package:redtutor_flutter_webapp/pages/activities/widgets/my_bots/widgets/json_upload_button.dart';
import 'package:redtutor_flutter_webapp/pages/activities/widgets/my_bots/widgets/pdf_upload_button.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';
import 'package:redtutor_flutter_webapp/widgets/popup_untils.dart';

class AddNewBotPage extends StatefulWidget {
  const AddNewBotPage({super.key});

  @override
  State<AddNewBotPage> createState() => _AddNewBotPageState();
}

class _AddNewBotPageState extends State<AddNewBotPage> {
  final TextEditingController _botNameTextController = TextEditingController();
  // List of items for the dropdown
  List<String> items = ['Please Select One', 'Default', 'Test'];
  bool _isClear = false;
  // Variable to store the selected item
  String botTemplateSeleted = 'Please Select One';
  @override
  void dispose() {
    _botNameTextController.dispose();
    super.dispose();
  }

  createBot() async {
    setState(() {
      _isClear = true;
      _botNameTextController.clear();
      botTemplateSeleted = 'Please Select One';
    });
    showPopupStatus(context, 'success');

    // BotRequest botRequest = BotRequest(
    //     botName: _botNameTextController.text,
    //     botTemplate: botTemplateSeleted,
    //     botJson: activitiesController.botJson.value,
    //     botDocument: activitiesController.botDocument.value);
    // await activitiesController.createBot(botRequest);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Bot Name
                    const CustomText(
                      text: "AI Name",
                      weight: FontWeight.bold,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      controller: _botNameTextController,
                      onChanged: (text) {
                        // Update character count
                      },
                      decoration: const InputDecoration(
                        hintText: 'AI Name Text...',
                        border: OutlineInputBorder(),
                      ),
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(
                            100), // Limit to 100 characters
                      ],
                    )
                  ],
                ),
              ),
              Expanded(
                child: Container(),
              )
            ]),
        const SizedBox(
          height: 20,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const CustomText(
              text: "AI Template",
              weight: FontWeight.bold,
            ),
            DropdownButton<String>(
              value: botTemplateSeleted,
              onChanged: (String? newValue) {
                setState(() {
                  botTemplateSeleted = newValue!;
                });
              },
              items: items.map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        const JsonUploadButton(buttonName: "Upload AI Prompt Json"),
        const SizedBox(
          height: 20,
        ),
        const PdfUploadButton(),
        const SizedBox(
          height: 20,
        ),
        const JsonUploadButton(buttonName: "Upload Bot Command Json"),
        const SizedBox(
          height: 20,
        ),
        const Divider(color: Colors.grey, thickness: 2),
        const SizedBox(
          height: 20,
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              OutlinedButton(
                onPressed: () {
                  activitiesController.changeAddNewStatusTo(false);
                  activitiesController.changeUpdateStatusTo(false);
                },
                child: Container(
                    padding: const EdgeInsets.all(8.0),
                    width: 100,
                    height: 40,
                    child: const Align(
                        alignment: Alignment.center,
                        child: CustomText(text: "Back"))),
              ),
              const SizedBox(
                width: 20,
              ),
              OutlinedButton(
                onPressed: () {
                  createBot();
                },
                child: Container(
                    padding: const EdgeInsets.all(8.0),
                    width: 100,
                    height: 40,
                    child: const Align(
                        alignment: Alignment.center,
                        child: CustomText(text: "Save Bot"))),
              )
            ]),
      ],
    );
  }
}
