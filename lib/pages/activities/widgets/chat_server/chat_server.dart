import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/pages/activities/widgets/chat_server/widgets/add_new_chat_server.dart';
import 'package:redtutor_flutter_webapp/pages/activities/widgets/chat_server/widgets/chat_server_table.dart';

class MyActivitiesPage extends StatefulWidget {
  const MyActivitiesPage({super.key});

  @override
  State<MyActivitiesPage> createState() => _MyActivitiesPageState();
}

class _MyActivitiesPageState extends State<MyActivitiesPage> {
  @override
  Widget build(BuildContext context) {
    return Obx(() => Expanded(
          child: ListView(children: [
            if (chatServerController.addNewStatus.value == false)
              const ChatServerTablePage()
            else
              const AddNewChatServerPage()
          ]),
        ));
  }
}
