import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/constants/style.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

class ChatServerTablePage extends StatefulWidget {
  const ChatServerTablePage({super.key});

  @override
  State<ChatServerTablePage> createState() => _ChatServerTablePageState();
}

class _ChatServerTablePageState extends State<ChatServerTablePage> {
  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: active.withOpacity(.4), width: .5),
            boxShadow: [
              BoxShadow(
                  offset: const Offset(0, 6),
                  color: lightGrey.withOpacity(.1),
                  blurRadius: 12)
            ],
            borderRadius: BorderRadius.circular(8),
          ),
          padding: const EdgeInsets.all(16),
          margin: const EdgeInsets.only(bottom: 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  const SizedBox(
                    width: 10,
                  ),
                  SizedBox(
                    width: 150,
                    height: 50,
                    child: OutlinedButton.icon(
                        onPressed: () {
                          chatServerController.changeAddNewStatusTo(true);
                        },
                        icon: Icon(
                          Icons.add,
                          color: dark,
                        ),
                        label: const CustomText(text: "Add New")),
                  ),
                  Expanded(child: Container()),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              // 56: dataRowHeight
              // 7: number of data each page
              // 40: headingRowHeight
              SizedBox(
                height: (100 * 7) + 40,
                child: DataTable2(
                  columnSpacing: 12,
                  dataRowHeight: 100,
                  headingRowHeight: 40,
                  horizontalMargin: 12,
                  minWidth: 600,
                  showCheckboxColumn: false,
                  columns: const [
                    DataColumn2(
                        label: CustomText(
                          text: "Chat Server Name",
                          weight: FontWeight.bold,
                        ),
                        size: ColumnSize.L),
                    DataColumn2(
                      label: Row(
                        children: [
                          CustomText(
                              text: 'Last Edited', weight: FontWeight.bold),
                          // IconButton(
                          //   hoverColor: Colors.transparent,
                          //   icon: const Icon(Icons.arrow_upward),
                          //   onPressed: () {},
                          // )
                        ],
                      ),
                      size: ColumnSize.M,
                    ),
                    DataColumn2(
                      label: CustomText(
                        text: 'Number of Room',
                        weight: FontWeight.bold,
                      ),
                      size: ColumnSize.S,
                    ),
                    DataColumn2(
                      label: CustomText(text: ''),
                      size: ColumnSize.M,
                    ),
                  ],
                  rows: List<DataRow>.generate(
                      activitiesController.activitiesObs.length, (index) {
                    return DataRow(
                      cells: [
                        // Chat Server Name
                        const DataCell(CustomText(text: "-----")),
                        // Last Edited
                        const DataCell(CustomText(text: "-----")),
                        // the number of room
                        const DataCell(CustomText(text: "-----")),
                        // Action button
                        DataCell(Row(
                          children: [
                            IconButton(
                              hoverColor: Colors.transparent,
                              icon: const Icon(Icons.delete),
                              onPressed: () {},
                            ),
                          ],
                        )),
                      ],
                      onSelectChanged: (isSelected) {
                        // Handle row click action here
                      },
                    );
                  }),
                ),
              ),
            ],
          ),
        ));
  }
}
