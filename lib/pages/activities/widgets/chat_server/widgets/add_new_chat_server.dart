import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';
import 'package:redtutor_flutter_webapp/widgets/popup_untils.dart';

class AddNewChatServerPage extends StatefulWidget {
  const AddNewChatServerPage({super.key});

  @override
  State<AddNewChatServerPage> createState() => _AddNewChatServerPageState();
}

class _AddNewChatServerPageState extends State<AddNewChatServerPage> {
  final TextEditingController _chatServerNameTextController =
      TextEditingController();
  final TextEditingController _chatServerIdTextController =
      TextEditingController();
  String selectedServerType = 'Please Select One';
  List<String> serverTypeItems = ['Please Select One', 'Discord', 'Telegram'];

  List<String> AIItems = [
    'Please Select One',
    'Summarizeai',
    'Faqai',
    'Coacheeai',
    'FalicitatorAI'
  ];

  final List<Room> _rooms = [
    Room(
        roomIdController: TextEditingController(),
        passcodeController: TextEditingController())
  ];

  final List<AI> _AIs = [AI(AIName: 'Please Select One')];

  @override
  void dispose() {
    _chatServerNameTextController.dispose();
    _chatServerIdTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Bot Name
                    const CustomText(
                      text: "Chat Server Name",
                      weight: FontWeight.bold,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      controller: _chatServerNameTextController,
                      onChanged: (text) {
                        // Update character count
                      },
                      decoration: const InputDecoration(
                        hintText: 'Chat Server Text...',
                        border: OutlineInputBorder(),
                      ),
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(
                            100), // Limit to 100 characters
                      ],
                    )
                  ],
                ),
              ),
              Expanded(
                child: Container(),
              )
            ]),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const CustomText(
                  text: "Server Type",
                  weight: FontWeight.bold,
                ),
                Row(
                  children: [
                    DropdownButton<String>(
                      value: selectedServerType,
                      items: serverTypeItems.map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          selectedServerType = value!;
                        });
                      },
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              width: 60,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const CustomText(
                  text: "Server Id",
                  weight: FontWeight.bold,
                ),
                SizedBox(
                  width: 300,
                  child: TextFormField(
                    controller: _chatServerIdTextController,
                    onChanged: (text) {
                      // Update character count
                    },
                    decoration: const InputDecoration(
                      hintText: 'Server Id Text...',
                      border: OutlineInputBorder(),
                    ),
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(
                          100), // Limit to 100 characters
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
        const Divider(
          height: 20,
          thickness: 2,
          color: Colors.black,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const CustomText(
              text: "Room",
              weight: FontWeight.bold,
            ),
            ListView.builder(
                shrinkWrap: true,
                itemCount: _rooms.length,
                itemBuilder: (BuildContext context, int index) {
                  return _roomWidget(index);
                })
          ],
        ),
        const Divider(
          height: 20,
          thickness: 2,
          color: Colors.black,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const CustomText(
              text: "List AI",
              weight: FontWeight.bold,
            ),
            ListView.builder(
                shrinkWrap: true,
                itemCount: _AIs.length,
                itemBuilder: (BuildContext context, int index) {
                  return _AIWidget(index);
                })
          ],
        ),
        const Divider(
          height: 20,
          thickness: 2,
          color: Colors.black,
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              OutlinedButton(
                onPressed: () {
                  chatServerController.changeAddNewStatusTo(false);
                },
                child: Container(
                    padding: const EdgeInsets.all(8.0),
                    width: 100,
                    height: 40,
                    child: const Align(
                        alignment: Alignment.center,
                        child: CustomText(text: "Back"))),
              ),
              const SizedBox(
                width: 20,
              ),
              OutlinedButton(
                onPressed: () {
                  showPopupStatus(context, 'success');
                },
                child: Container(
                    padding: const EdgeInsets.all(8.0),
                    width: 100,
                    height: 40,
                    child: const Align(
                        alignment: Alignment.center,
                        child: CustomText(text: "Submit"))),
              ),
            ])
      ],
    );
  }

  Widget _roomWidget(int index) {
    return Container(
      margin: const EdgeInsets.only(top: 20, bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // RoomId
          const CustomText(
            text: "RoomId: ",
          ),
          const SizedBox(
            width: 10,
          ),
          SizedBox(
            width: 300,
            child: TextFormField(
              controller: _rooms[index].roomIdController,
              onChanged: (text) {
                // Update character count
              },
              decoration: const InputDecoration(
                hintText: 'Room Id Text...',
                border: OutlineInputBorder(),
              ),
              inputFormatters: [
                LengthLimitingTextInputFormatter(
                    100), // Limit to 100 characters
              ],
            ),
          ),
          const SizedBox(
            width: 50,
          ),
          // Passcode
          const CustomText(
            text: "Passcode: ",
          ),
          const SizedBox(
            width: 10,
          ),
          SizedBox(
            width: 300,
            child: TextFormField(
              controller: _rooms[index].passcodeController,
              onChanged: (text) {
                // Update character count
              },
              decoration: const InputDecoration(
                hintText: 'Passcode Text...',
                border: OutlineInputBorder(),
              ),
              inputFormatters: [
                LengthLimitingTextInputFormatter(
                    100), // Limit to 100 characters
              ],
            ),
          ),
          const SizedBox(
            width: 60,
          ),

          if (_rooms.length > 1)
            Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Colors.grey,
                  width: 1.0,
                ),
              ),
              child: IconButton(
                icon: const Icon(Icons.delete),
                onPressed: () {
                  setState(() {
                    _rooms.removeAt(index);
                  });
                },
              ),
            ),
          const SizedBox(
            width: 20,
          ),

          if (index == _rooms.length - 1)
            Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Colors.grey,
                  width: 1.0,
                ),
              ),
              child: IconButton(
                icon: const Icon(Icons.add),
                onPressed: () {
                  setState(() {
                    _rooms.add(Room(
                        roomIdController: TextEditingController(),
                        passcodeController: TextEditingController()));
                  });
                },
              ),
            ),
        ],
      ),
    );
  }

  Widget _AIWidget(int index) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const CustomText(
            text: "AI Name: ",
          ),
          const SizedBox(
            width: 10,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DropdownButton<String>(
                value: _AIs[index].AIName,
                items: AIItems.map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (value) {
                  List<String> AINames =
                      _AIs.map((item) => item.AIName).toList();
                  if (!AINames.contains(value)) {
                    setState(() {
                      _AIs[index].AIName = value!;
                    });
                  }
                },
              ),
            ],
          ),
          const SizedBox(
            width: 60,
          ),
          if (_AIs.length > 1)
            Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Colors.grey,
                  width: 1.0,
                ),
              ),
              child: IconButton(
                icon: const Icon(Icons.delete),
                onPressed: () {
                  setState(() {
                    _AIs.removeAt(index);
                  });
                },
              ),
            ),
          const SizedBox(
            width: 20,
          ),
          if (index == _AIs.length - 1)
            Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Colors.grey,
                  width: 1.0,
                ),
              ),
              child: IconButton(
                icon: const Icon(Icons.add),
                onPressed: () {
                  if (_AIs.length < 3) {
                    setState(() {
                      _AIs.add(AI(AIName: "Please Select One"));
                    });
                  }
                },
              ),
            ),
        ],
      ),
    );
  }
}

class Room {
  TextEditingController roomIdController = TextEditingController(text: '');
  TextEditingController passcodeController = TextEditingController(text: '');
  Room({
    required this.roomIdController,
    required this.passcodeController,
  });
}

class AI {
  String AIName;
  AI({
    required this.AIName,
  });
}
