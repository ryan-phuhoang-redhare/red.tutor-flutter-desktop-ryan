import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/models/request/login_request.dart';
import 'package:redtutor_flutter_webapp/pages/activities/widgets/chat_server/chat_server.dart';
import 'package:redtutor_flutter_webapp/pages/activities/widgets/my_bots/bots.dart';
import 'package:redtutor_flutter_webapp/pages/activities/widgets/my_rooms/rooms.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

class Activities extends StatefulWidget {
  const Activities({super.key});

  @override
  State<Activities> createState() => _ActivitiesState();
}

class _ActivitiesState extends State<Activities> {
  void _resetState(var indexTab) {}
  @override
  void initState() {
    authController.getToken(LoginRequest("admin.redtutor@gmail.com", "123456"));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(50),
      child: DefaultTabController(
        length: 2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ButtonsTabBar(
              backgroundColor: Colors.lightGreen[400],
              unselectedBackgroundColor: Colors.grey[350],
              unselectedLabelStyle: const TextStyle(color: Colors.black),
              labelStyle: const TextStyle(
                  color: Colors.white, fontWeight: FontWeight.bold),
              buttonMargin: const EdgeInsets.only(right: 50),
              onTap: (p0) {
                // resetState(p0);
              },
              tabs: [
                Tab(
                    child: Container(
                        margin: const EdgeInsets.all(10),
                        width: 150,
                        child: const Center(
                            child: CustomText(text: "Chat Server")))),
                // Tab(
                //     child: Container(
                //         margin: const EdgeInsets.all(10),
                //         width: 150,
                //         child: const Center(
                //             child: CustomText(text: "Chat Room")))),
                Tab(
                    child: Container(
                        margin: const EdgeInsets.all(10),
                        width: 150,
                        child: const Center(child: CustomText(text: "AI Bot"))))
              ],
            ),
            const Expanded(
              child: TabBarView(
                children: <Widget>[
                  MyActivitiesPage(),
                  // MyRoomsPage(),
                  MyBotsPage(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
