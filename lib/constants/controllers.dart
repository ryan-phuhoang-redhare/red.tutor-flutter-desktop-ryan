import 'package:redtutor_flutter_webapp/controllers/activities_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/auth_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/chatServer_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/courses_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/exercises_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/menu_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/navigation_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/topics_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/worksheets_controller.dart';

import '../controllers/ebooks_controller.dart';

MenuController menuController = MenuController.instance;
NavigationController navigationController = NavigationController.instance;
CoursesController coursesController = CoursesController.instance;
TopicsController topicsController = TopicsController.instance;
ExercisesController exercisesController = ExercisesController.instance;
WorksheetsController worksheetsController = WorksheetsController.instance;
ActivitiesController activitiesController = ActivitiesController.instance;
AuthController authController = AuthController.instance;
ChatServerController chatServerController = ChatServerController.instance;
EbooksController ebooksController = EbooksController.instance;
