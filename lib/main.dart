import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:redtutor_flutter_webapp/api/api_client.dart';
import 'package:redtutor_flutter_webapp/controllers/activities_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/auth_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/chatServer_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/courses_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/ebooks_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/exercises_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/menu_controller.dart'
    as menu_controller;
import 'package:redtutor_flutter_webapp/controllers/navigation_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/topics_controller.dart';
import 'package:redtutor_flutter_webapp/controllers/worksheets_controller.dart';
import 'package:redtutor_flutter_webapp/layout.dart';
import 'package:redtutor_flutter_webapp/pages/404/error.dart';
import 'package:redtutor_flutter_webapp/pages/authentication/authentication.dart';
import 'package:redtutor_flutter_webapp/routing/routes.dart';
import 'package:url_strategy/url_strategy.dart';

void main() {
  Get.put(menu_controller.MenuController());
  Get.put(NavigationController());
  Get.put(CoursesController());
  Get.put(TopicsController());
  Get.put(ExercisesController());
  Get.put(WorksheetsController());
  Get.put(AuthController());
  Get.put(ActivitiesController());
  Get.put(ChatServerController());
  Get.put(EbooksController());
  setPathUrlStrategy();
  ApiClient.setup();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: rootRoute,
      unknownRoute:
          GetPage(name: '/notfound', page: () => const PageNotFound()),
      getPages: [
        // GetPage(
        //     name: rootRoute,
        //     page: () {
        //       return SiteLayout();
        //     }),
        GetPage(
            name: authenticationPageRoute,
            page: () => const AuthenticationPage()),
        GetPage(name: '/notfound', page: () => const PageNotFound())
      ],
      debugShowCheckedModeBanner: false,
      title: "Dashboard",
      theme: ThemeData(
          scaffoldBackgroundColor: const Color(0xFFD1EEE8),
          textTheme: GoogleFonts.mulishTextTheme(Theme.of(context).textTheme)
              .apply(bodyColor: Colors.black),
          pageTransitionsTheme: const PageTransitionsTheme(builders: {
            TargetPlatform.iOS: FadeUpwardsPageTransitionsBuilder(),
            TargetPlatform.android: FadeUpwardsPageTransitionsBuilder()
          }),
          primaryColor: const Color(0xFF079992)),
      home: SiteLayout(),
    );
  }
}
