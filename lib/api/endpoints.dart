class Endpoints {
  static const redtutorApi = '/redTutorApi';
  // Auth
  static const login = '$redtutorApi/redtutorAuth/redtutorLogin';
  // Courses
  static const getAllCourses = '$redtutorApi/subject/getAll';
  static const createCourse = '$redtutorApi/subject/create';
  static const updateCourse = '$redtutorApi/subject/update';
  static const deleteCourse = '$redtutorApi/subject/delete';
  // Learning Level Graph
  static const createGraph = '$redtutorApi/learningLevelGraph/create';
  static const updateGraph = '$redtutorApi/learningLevelGraph/update';
  static const getGraphByCourseId =
      '$redtutorApi/learningLevelGraph/getGraphByCourseId';
  // Topic
  static const getAllTopics = '$redtutorApi/topic/getAll';
  static const createTopic = '$redtutorApi/topic/create';
  static const updateTopic = '$redtutorApi/topic/update';
  static const deleteTopic = '$redtutorApi/topic/delete';
  // Exercise
  static const getAllExercises = '$redtutorApi/exercise/getAll';
  static const getExerciseById = '$redtutorApi/exercise/getById';
  static const createExercise = '$redtutorApi/exercise/create';
  static const updateExercise = '$redtutorApi/exercise/update';
  static const deleteExercise = '$redtutorApi/exercise/delete';
  // Worksheet && Quiz
  static const getAllWorksheets = '$redtutorApi/worksheet/getAll';
  static const createWorksheet = '$redtutorApi/worksheet/create';
  static const updateWorksheet = '$redtutorApi/worksheet/update';
  static const deleteWorksheet = '$redtutorApi/worksheet/delete';
  static const getQuizzesByWorksheetId =
      '$redtutorApi/worksheet/getQuizzesByWorksheetId';
  static const getQuizById = '$redtutorApi/quiz/getQuizById';
  // Activities
  static const createBot = '$redtutorApi/activity/createBot';
  // Ebooks
  static const getAllEbooks = '$redtutorApi/ebook/getAllRawData';
}
