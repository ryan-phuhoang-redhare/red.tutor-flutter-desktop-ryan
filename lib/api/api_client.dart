import 'package:dio/dio.dart';
import 'package:redtutor_flutter_webapp/constants/baseUrl.dart';

class ApiClient {
  static final Dio dio = Dio();

  static void setup() {
    dio.options.baseUrl = BaseUrl.localUrl;
    dio.options.connectTimeout =
        const Duration(milliseconds: 3000); // Adjust timeouts as needed
    dio.options.receiveTimeout = const Duration(milliseconds: 3000);

    // Add authentication interceptors or other global configurations
    dio.interceptors.add(LogInterceptor());
  }
}
