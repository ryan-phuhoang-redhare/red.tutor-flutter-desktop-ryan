import 'package:flutter/material.dart';
import 'package:redtutor_flutter_webapp/pages/activities/activities.dart';
import 'package:redtutor_flutter_webapp/pages/books/books.dart';
import 'package:redtutor_flutter_webapp/pages/overview/overview.dart';
import 'package:redtutor_flutter_webapp/pages/teach/teach.dart';
import 'package:redtutor_flutter_webapp/routing/routes.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case overviewPageRoute:
      return _getPageRoute(const OverViewPage());
    case teachPageRoute:
      return _getPageRoute(const TeachPage());
    case activitiesPageRoute:
      return _getPageRoute(const Activities());
    case booksPageRoute:
      return _getPageRoute(const BooksPage());
    default:
      return _getPageRoute(const OverViewPage());
  }
}

PageRoute _getPageRoute(Widget child) {
  return MaterialPageRoute(builder: (context) => child);
}
