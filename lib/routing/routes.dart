const rootRoute = "/";

const overviewPageDisplayName = "Overview";
const overviewPageRoute = "/overview";

const teachPageDisplayName = "Teach";
const teachPageRoute = "/teach";

const activitiesPageDisplayName = "Activities";
const activitiesPageRoute = "/activities";

const booksPageDisplayName = "Books";
const booksPageRoute = "/books";

const gamesPageDisplayName = "Games";
const gamesPageRoute = "/games";

const reportsPageDisplayName = "Reports";
const reportsPageRoute = "/reports";

const chatsPageDisplayName = "Chats";
const chatsPageRoute = "/chats";

const authenticationPageDisplayName = "Log out";
const authenticationPageRoute = "/auth";

class MenuItem {
  final String name;
  final String route;

  MenuItem(this.name, this.route);
}

List<MenuItem> sideMenuItemRoutes = [
  MenuItem(overviewPageDisplayName, overviewPageRoute),
  MenuItem(teachPageDisplayName, teachPageRoute),
  MenuItem(activitiesPageDisplayName, activitiesPageRoute),
  MenuItem(booksPageDisplayName, booksPageRoute),
  MenuItem(gamesPageDisplayName, gamesPageRoute),
  MenuItem(reportsPageDisplayName, reportsPageRoute),
  MenuItem(chatsPageDisplayName, chatsPageRoute),
  MenuItem(authenticationPageDisplayName, authenticationPageRoute),
];
