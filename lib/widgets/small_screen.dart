import 'package:flutter/material.dart';
import 'package:redtutor_flutter_webapp/helpers/local_navigator.dart';

class SmallScreen extends StatelessWidget {
  const SmallScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return localNavigator();
  }
}
