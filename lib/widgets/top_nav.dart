import 'package:flutter/material.dart';
import 'package:redtutor_flutter_webapp/constants/style.dart';
import 'package:redtutor_flutter_webapp/helpers/responsiveness.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

AppBar topNavigationBar(BuildContext context, GlobalKey<ScaffoldState> key) =>
    AppBar(
        leading: !ResponsiveWidget.isSmallScreen(context)
            ? Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 16),
                    child:
                        Image.asset("assets/icons/redhareLogo.png", width: 28),
                  )
                ],
              )
            : IconButton(
                icon: const Icon(Icons.menu),
                onPressed: () {
                  key.currentState?.openDrawer();
                }),
        elevation: 0,
        title: Row(children: [
          const Visibility(
              child: CustomText(
            text: "Red.Tutor",
            color: Colors.white,
            size: 20,
            weight: FontWeight.bold,
          )),
          Expanded(child: Container()),
          CustomText(text: "999999", color: lightGrey),
          const SizedBox(
            width: 24,
          ),
          OutlinedButton.icon(
            onPressed: () {},
            icon: Icon(Icons.add, color: dark.withOpacity(.7)),
            label: CustomText(text: "999", color: lightGrey), // <-- Text
          ),
          const SizedBox(
            width: 24,
          ),
          // IconButton(
          //     icon: Icon(Icons.settings, color: dark.withOpacity(.7)),
          //     onPressed: () {}),
          Stack(
            children: [
              IconButton(
                  icon: Icon(Icons.notifications, color: dark.withOpacity(.7)),
                  onPressed: () {}),
              Positioned(
                  top: 7,
                  right: 7,
                  child: Container(
                    width: 12,
                    height: 12,
                    padding: const EdgeInsets.all(4),
                    decoration: BoxDecoration(
                        color: active,
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(color: light, width: 2)),
                  ))
            ],
          ),
          // Container(
          //   width: 1,
          //   height: 22,
          //   color: lightGrey,
          // ),
          // const SizedBox(
          //   width: 24,
          // ),
          // CustomText(text: "Santos Enoque", color: lightGrey),
          // const SizedBox(
          //   width: 16,
          // ),
          // Container(
          //   decoration: BoxDecoration(
          //       color: Colors.white, borderRadius: BorderRadius.circular(30)),
          //   child: Container(
          //       padding: const EdgeInsets.all(2),
          //       margin: const EdgeInsets.all(2),
          //       child: CircleAvatar(
          //           backgroundColor: light,
          //           child: Icon(Icons.person_outline, color: dark))),
          // ),
        ]),
        iconTheme: IconThemeData(color: dark),
        backgroundColor: Colors.transparent);
