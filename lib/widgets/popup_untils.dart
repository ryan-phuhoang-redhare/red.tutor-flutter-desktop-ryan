import 'package:flutter/material.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

void showPopupStatus(BuildContext context, String status) {
  String textForm = '';
  if (status == 'success') {
    textForm = 'Form submitted successfully.';
  } else {
    textForm = "Something wrong when submitting the form";
  }
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      behavior: SnackBarBehavior.floating,
      width: 800,
      padding: EdgeInsets.all(20),
      content: CustomText(text: textForm, color: Colors.black),
      duration: const Duration(seconds: 3),
      backgroundColor:
          status == 'success' ? Colors.green[200] : Colors.red[400],
      action: SnackBarAction(
        label: 'Dismiss',
        textColor: Colors.black,
        onPressed: () {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
        },
      ),
    ),
  );
}
