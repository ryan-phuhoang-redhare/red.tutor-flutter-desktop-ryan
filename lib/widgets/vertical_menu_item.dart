import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';

class VerticalMenuItem extends StatelessWidget {
  final String itemName;
  final Function()? onTap;
  const VerticalMenuItem(
      {super.key, required this.itemName, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      onHover: (value) {
        value
            ? menuController.onHover(itemName)
            : menuController.onHover("not hovering");
      },
      child: Obx(() => Container(
          color: menuController.isHovering(itemName)
              ? const Color(0xFF079992)
              : Colors.white,
          child: Row(
            children: [
              Visibility(
                  visible: menuController.isHovering(itemName) ||
                      menuController.isActive(itemName),
                  maintainSize: true,
                  maintainState: true,
                  maintainAnimation: true,
                  child: Container(
                    width: 3,
                    height: 72,
                    color: const Color(0xFF079992),
                  )),
              Expanded(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                      padding: const EdgeInsets.all(16),
                      child: menuController.returnIconFor(itemName)),
                  if (!menuController.isActive(itemName))
                    Flexible(
                        child: CustomText(
                            text: itemName,
                            color: menuController.isHovering(itemName)
                                ? const Color(0xFF079992)
                                : Colors.white))
                  else
                    Flexible(
                        child: CustomText(
                      text: itemName,
                      color: Colors.white,
                      size: 18,
                      weight: FontWeight.bold,
                    ))
                ],
              ))
            ],
          ))),
    );
  }
}
