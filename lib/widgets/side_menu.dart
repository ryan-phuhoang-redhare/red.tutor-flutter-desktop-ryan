import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redtutor_flutter_webapp/constants/controllers.dart';
import 'package:redtutor_flutter_webapp/constants/style.dart';
import 'package:redtutor_flutter_webapp/helpers/responsiveness.dart';
import 'package:redtutor_flutter_webapp/routing/routes.dart';
import 'package:redtutor_flutter_webapp/widgets/custom_text.dart';
import 'package:redtutor_flutter_webapp/widgets/side_menu_item.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({super.key});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(
        color: const Color(0xFF79DBC8),
        child: ListView(
          children: [
            if (ResponsiveWidget.isSmallScreen(context))
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(
                    height: 40,
                  ),
                  Row(
                    children: [
                      SizedBox(width: width / 48),
                      Padding(
                        padding: const EdgeInsets.only(right: 12),
                        child: Image.asset("assets/icons/redhareLogo.png",
                            width: 28),
                      ),
                      const Flexible(
                          child: CustomText(
                              text: "Red.Tutor",
                              size: 20,
                              weight: FontWeight.bold,
                              color: Colors.white)),
                      SizedBox(width: width / 48),
                    ],
                  ),
                  const SizedBox(height: 30),
                ],
              ),
            const Divider(
              color: Colors.white,
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(40)),
                      child: Container(
                          padding: const EdgeInsets.all(2),
                          margin: const EdgeInsets.all(2),
                          child: CircleAvatar(
                            radius: 30,
                            backgroundColor: light,
                            child: Icon(Icons.person_outline,
                                color: dark, size: 30),
                          )),
                    ),
                    const SizedBox(width: 20),
                    const Expanded(
                      child: Column(
                          // mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                CustomText(
                                  text: "Lvl 99",
                                  color: Colors.white,
                                  size: 18,
                                  weight: FontWeight.bold,
                                )
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20)),
                                child: LinearProgressIndicator(
                                  backgroundColor: Colors.orangeAccent,
                                  valueColor:
                                      AlwaysStoppedAnimation(Colors.blue),
                                  minHeight: 15,
                                  value: 0.9,
                                ),
                              ),
                            ),
                          ]),
                    )
                  ],
                )
              ],
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: sideMenuItemRoutes
                  .map((item) => SideMenuItem(
                      itemName: item.name,
                      onTap: () {
                        if (item.route == authenticationPageRoute) {
                          Get.offAllNamed(authenticationPageRoute);
                          menuController.changeActiveItemTo(item.name);
                        }
                        if (!menuController.isActive(item.name)) {
                          menuController.changeActiveItemTo(item.name);
                          if (ResponsiveWidget.isSmallScreen(context)) {
                            Get.back();
                          }
                          navigationController.navigateTo(item.route);
                        }
                      }))
                  .toList(),
            )
          ],
        ));
  }
}
