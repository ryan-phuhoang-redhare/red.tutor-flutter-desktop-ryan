import 'package:redtutor_flutter_webapp/utils/extentions.dart';

class Ebook {
  String ebookTitle;
  String ebookUrl;
  List<String> authors;
  String description;
  int totalPages;
  List<String> genres;
  List<String> languages;
  int price;
  Publisher publisher;
  String learningLevel;
  String ebookStatus;
  EbookAdvertisement ebookAdvertisement;

  Ebook(
      {required this.description,
      required this.authors,
      required this.ebookTitle,
      required this.totalPages,
      required this.ebookUrl,
      required this.genres,
      required this.languages,
      required this.price,
      required this.publisher,
      required this.ebookStatus,
      required this.learningLevel,
      required this.ebookAdvertisement});

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'description': description,
      'authors': authors,
      'ebookTitle': ebookTitle,
      'totalPages': totalPages,
      'ebookUrl': ebookUrl,
      'genres': genres,
      'languages': languages,
      'price': price,
      'publisher': publisher,
      'ebookStatus': ebookStatus,
      'learningLevel': learningLevel,
      'ebookAdvertisement': ebookAdvertisement,
    };
  }

  // Create a factory constructor to create objects from a Map.
  factory Ebook.fromJson(Map<String, dynamic> jsonData) {
    String description = jsonData['description'].toString().orEmpty();
    print('description: $description');

    // List<String> authors = [];
    // print('authors: $authors');

    List<String>? authors = [];
    if (jsonData['authors'] != null) {
      List<dynamic> jsonList = jsonData['authors'];
      for (var element in jsonList) {
        authors.add(element.toString());
      }
    }

    String ebookTitle = jsonData['ebookTitle'].toString().orEmpty();
    print('ebookTitle: $ebookTitle');

    int totalPages = int.parse(jsonData['totalPages'].toString()).orZero();
    print('totalPages: $totalPages');

    String ebookUrl = jsonData['ebookUrl'].toString().orEmpty();
    print('ebookUrl: $ebookUrl');

    List<String> genres = [];
    if (jsonData['genres'] != null) {
      List<dynamic> jsonList = jsonData['genres'];
      for (var element in jsonList) {
        genres.add(element.toString());
      }
    }
    print('genres: $genres');

    List<String> languages = [];
    if (jsonData['languages'] != null) {
      List<dynamic> jsonList = jsonData['languages'];
      for (var element in jsonList) {
        languages.add(element.toString());
      }
    }
    print('languages: $languages');

    int price = int.parse(jsonData['price'].toString()).orZero();
    print('price: $price');

    Publisher publisher = Publisher(' ', ' ');
    if (jsonData['publisher'] != null) {
      dynamic data = jsonData['publisher'];
      publisher.id = data['id'];
      publisher.publisherName = data['publisherName'];
    }
    //jsonData['publisher'].toString().orEmpty() as Publisher;
    print('publisher: $publisher');

    String ebookStatus = jsonData['ebookStatus'].toString().orEmpty();
    print('ebookStatus: $ebookStatus');

    String learningLevel = jsonData['learningLevel'].toString().orEmpty();
    print('learningLevel: $learningLevel');

    EbookAdvertisement ebookAdvertisement = EbookAdvertisement(
        adDisplayPages: [],
        adEngines: [],
        adSkippingType: '',
        advertisingUnlock: false);
    // jsonData['ebookAdvertisement']
    //     .toString()
    //     .orEmpty() as EbookAdvertisement;
    print('ebookAdvertisement: $ebookAdvertisement');

    try {
      return Ebook(
          description: description,
          authors: authors,
          ebookTitle: ebookTitle,
          totalPages: totalPages,
          ebookUrl: ebookUrl,
          genres: genres,
          languages: languages,
          price: price,
          publisher: publisher,
          ebookStatus: ebookStatus,
          learningLevel: learningLevel,
          ebookAdvertisement: ebookAdvertisement);
    } catch (e) {
      return Ebook(
          description: '',
          authors: [],
          ebookTitle: '',
          totalPages: 0,
          ebookUrl: '',
          genres: [],
          languages: [],
          price: 0,
          publisher: Publisher('', ''),
          ebookStatus: '',
          learningLevel: '',
          ebookAdvertisement: EbookAdvertisement(
              adDisplayPages: [],
              adEngines: [],
              adSkippingType: '',
              advertisingUnlock: false));
    }
  }
}

class Publisher {
  String publisherName;
  String id;

  Publisher(this.id, this.publisherName);

  // factory Publisher.fromPublisherResponse(PublisherResponse r) =>
  //     Publisher(r.id.orEmpty(), r.publisherName.orEmpty());
}

class EbookAdvertisement {
  bool advertisingUnlock;
  List<AdEngine> adEngines;
  List<String> adDisplayPages;
  String adSkippingType;

  EbookAdvertisement(
      {required this.adDisplayPages,
      required this.adEngines,
      required this.adSkippingType,
      required this.advertisingUnlock});

  // factory EbookAdvertisement.fromEbookAdResponse(EbookAdResponse r) =>
  //     EbookAdvertisement(
  //       adDisplayPages: r.adDisplayPages.orEmpty(),
  //       adEngines: r.adEngines
  //           .orEmpty()
  //           .map((e) => AdEngine.fromAdEngineResponse(e))
  //           .toList(),
  //       adSkippingType: r.adSkippingType.orEmpty(),
  //       advertisingUnlock: r.advertisingUnlock.orFalse(),
  //     );
}

class AdEngine {
  String adEngineType;
  String apiKey;
  AdColonyZone adcolonyZone;

  AdEngine(
      {required this.adEngineType,
      required this.adcolonyZone,
      required this.apiKey});

  // factory AdEngine.fromAdEngineResponse(AdEngineResponse r) => AdEngine(
  //       adEngineType: r.adEngineType.orEmpty(),
  //       adcolonyZone: AdColonyZone.fromAdColonyZoneResponse(r.adcolonyZone),
  //       apiKey: r.apiKey.orEmpty(),
  //     );
}

class AdColonyZone {
  String zoneName;
  String zoneId;

  AdColonyZone({required this.zoneName, required this.zoneId});

  // factory AdColonyZone.fromAdColonyZoneResponse(AdColonyZoneResponse r) =>
  //     AdColonyZone(
  //       zoneName: r.zoneName.orEmpty(),
  //       zoneId: r.zoneId.orEmpty(),
  //     );
}

class PostMessage {
  String message;

  PostMessage({required this.message});
}
