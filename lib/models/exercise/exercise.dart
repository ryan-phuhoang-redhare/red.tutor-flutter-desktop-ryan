import 'package:redtutor_flutter_webapp/utils/extentions.dart';

class Exercise {
  String id;
  String title;
  //String description;
  String worksheetId;
  String? topicId;
  String noteContent;
  int questionLimit;
  int minimumScore;
  int timerDuration;
  int? lastEdited;

  Exercise(
      {required this.id,
      required this.title,
      //required this.description,
      required this.worksheetId,
      this.topicId,
      required this.noteContent,
      required this.questionLimit,
      required this.minimumScore,
      required this.timerDuration,
      this.lastEdited});

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      //'description': description,
      'worksheetId': worksheetId,
      'topicId': topicId,
      'noteContent': noteContent,
      'questionLimit': questionLimit,
      'minimumScore': minimumScore,
      'timerDuration': timerDuration,
      'lastEdited': lastEdited
    };
  }

  // Create a factory constructor to create objects from a Map.
  factory Exercise.fromJson(Map<String, dynamic> jsonData) {
    String id = jsonData['id'].toString().orEmpty();
    String title = jsonData['title'].toString().orEmpty();
    //String description = jsonData['description'].toString().orEmpty();
    String worksheetId = jsonData['worksheetId'].toString().orEmpty();
    String topicId = jsonData['topicId'].toString().orEmpty();
    String noteContent = jsonData['noteContent'].toString().orEmpty();
    int questionLimit =
        int.parse(jsonData['questionLimit'].toString()).orZero();
    int minimumScore = int.parse(jsonData['minimumScore'].toString()).orZero();
    int timerDuration =
        int.parse(jsonData['timerDuration'].toString()).orZero();
    int lastEdited = jsonData['lastEdited'] == null
        ? 0
        : int.parse(jsonData['lastEdited'].toString()).orZero();

    try {
      return Exercise(
          id: id,
          title: title,
          //description: description,
          worksheetId: worksheetId,
          topicId: topicId,
          noteContent: noteContent,
          questionLimit: questionLimit,
          minimumScore: minimumScore,
          timerDuration: timerDuration,
          lastEdited: lastEdited);
    } catch (e) {
      return Exercise(
          id: '',
          title: '',
          //description: '',
          worksheetId: '',
          topicId: '',
          noteContent: '',
          questionLimit: 0,
          minimumScore: 0,
          timerDuration: 0,
          lastEdited: 0);
    }
  }
}
