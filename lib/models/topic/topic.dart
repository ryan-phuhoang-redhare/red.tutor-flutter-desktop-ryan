import 'package:redtutor_flutter_webapp/models/mediaRecourse.dart';
import 'package:redtutor_flutter_webapp/utils/extentions.dart';

class Topic {
  String id;
  String subjectId;
  String title;
  String description;
  int masteryReward;
  int maxRepeatCount;
  int coinReward;
  int unlockCost;
  List<String> assignedExercises;
  MediaResource mediaResource;
  String? imageBase64Data;
  String status;
  int lastEdited;

  Topic({
    required this.id,
    required this.subjectId,
    required this.title,
    required this.description,
    required this.masteryReward,
    required this.maxRepeatCount,
    required this.coinReward,
    required this.unlockCost,
    required this.assignedExercises,
    required this.mediaResource,
    this.imageBase64Data,
    required this.status,
    required this.lastEdited,
  });

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'subjectId': subjectId,
      'title': title,
      'description': description,
      'masteryReward': masteryReward,
      'maxRepeatCount': maxRepeatCount,
      'coinReward': coinReward,
      'unlockCost': unlockCost,
      'assignedExercises': assignedExercises,
      'mediaResource': mediaResource,
      'imageBase64Data': imageBase64Data,
      'status': status,
      'lastEdited': lastEdited
    };
  }

  // Create a factory constructor to create objects from a Map.
  factory Topic.fromJson(Map<String, dynamic> jsonData) {
    Map<String, dynamic> mediaResourceJson = jsonData['mediaResource'];
    MediaResource mediaResource = MediaResource.fromJson(mediaResourceJson);
    String id = jsonData['id'].toString().orEmpty();
    String subjectId = jsonData['subjectId'].toString().orEmpty();
    String title = jsonData['title'].toString().orEmpty();
    String description = jsonData['description'].toString().orEmpty();
    String status = jsonData['status'].toString().orEmpty();
    int masteryReward =
        int.tryParse(jsonData['masteryReward'].toString()).orZero();
    int maxRepeatCount =
        int.tryParse(jsonData['maxRepeatCount'].toString()).orZero();
    int coinReward = int.tryParse(jsonData['coinReward'].toString()).orZero();
    int unlockCost = int.tryParse(jsonData['unlockCost'].toString()).orZero();
    int lastEdited = int.tryParse(jsonData['lastEdited'].toString()).orZero();
    List<String>? assignedExercises = [];
    if (jsonData['assignedExercises'] != null) {
      List<dynamic> jsonList = jsonData['assignedExercises'];
      for (var element in jsonList) {
        assignedExercises.add(element.toString());
      }
    }
    try {
      return Topic(
        id: id,
        subjectId: subjectId,
        title: title,
        description: description,
        masteryReward: masteryReward,
        maxRepeatCount: maxRepeatCount,
        coinReward: coinReward,
        unlockCost: unlockCost,
        assignedExercises: assignedExercises,
        mediaResource: mediaResource,
        status: status,
        lastEdited: lastEdited,
      );
    } catch (e) {
      return Topic(
        id: '',
        subjectId: '',
        title: '',
        description: '',
        masteryReward: 0,
        maxRepeatCount: 0,
        coinReward: 0,
        unlockCost: 0,
        assignedExercises: [],
        mediaResource: MediaResource(id: '', mediaType: '', url: ''),
        status: "",
        lastEdited: 0,
      );
    }
  }
}
