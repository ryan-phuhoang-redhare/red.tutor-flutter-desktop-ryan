class OptionRequest {
  int index;
  String description;
  bool isChecked;

  OptionRequest(
      {required this.index,
      required this.description,
      required this.isChecked});

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {'index': index, 'description': description, 'isChecked': isChecked};
  }
}
