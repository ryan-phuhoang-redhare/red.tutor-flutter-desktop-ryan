class QuizRequest {
  // Create newId when user edit quiz
  String? id;
  String? quizNewId;
  String question;
  String type;
  List<dynamic> options;
  bool isShuffleAnswer;
  dynamic rightAnswer;
  String explanation;
  String hint;
  String creatorId;
  int index;

  QuizRequest(
      {required this.question,
      required this.type,
      required this.options,
      required this.rightAnswer,
      required this.explanation,
      required this.hint,
      required this.creatorId,
      required this.index,
      required this.isShuffleAnswer,
      this.id,
      this.quizNewId});

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'quizNewId': quizNewId,
      'question': question,
      'type': type,
      'options': options,
      'rightAnswer': rightAnswer,
      'explanation': explanation,
      'hint': hint,
      'index': index,
      'isShuffleAnswer': isShuffleAnswer,
      'creatorId': creatorId,
    };
  }
}
