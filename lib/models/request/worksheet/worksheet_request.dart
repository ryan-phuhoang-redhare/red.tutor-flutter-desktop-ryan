import 'package:redtutor_flutter_webapp/models/request/worksheet/quiz_request.dart';

class WorksheetRequest {
  String? id;
  String title;
  bool isShuffleEnabled;
  List<QuizRequest> addNewQuizRequests;

  WorksheetRequest(
      {this.id,
      required this.title,
      required this.isShuffleEnabled,
      required this.addNewQuizRequests});

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'isShuffleEnabled': isShuffleEnabled,
      'addNewQuizRequests': addNewQuizRequests
    };
  }
}
