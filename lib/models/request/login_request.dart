import 'package:redtutor_flutter_webapp/models/account.dart';

class LoginRequest extends Account {
  final String typeLogin = "Local";
  LoginRequest(String email, String password) : super(email, password);
}
