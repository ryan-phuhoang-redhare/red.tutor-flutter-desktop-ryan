class BotRequest {
  String botName;
  String botTemplate;
  String botJson;
  String botDocument;

  BotRequest(
      {required this.botName,
      required this.botTemplate,
      required this.botJson,
      required this.botDocument});

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'botName': botName,
      'botTemplate': botTemplate,
      'botJson': botJson,
      'botDocument': botDocument
    };
  }
}
