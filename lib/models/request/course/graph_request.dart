class AddNewGraphRequest {
  String name;
  String subjectId;
  String creator;
  List<GraphRowRequest> graphRowRequests;

  AddNewGraphRequest(
      {required this.name,
      required this.subjectId,
      required this.creator,
      required this.graphRowRequests});

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'subjectId': subjectId,
      'creator': creator,
      'graphRowRequests':
          graphRowRequests.map((rowRequest) => rowRequest.toJson()).toList()
    };
  }
}

class UpdateGraphRequest {
  String id;
  String? name;
  List<GraphRowRequest> graphRowRequests;
  List<String> deletedRows;
  UpdateGraphRequest(
      {required this.id,
      this.name,
      required this.graphRowRequests,
      required this.deletedRows});

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name ?? '',
      'graphRowRequests':
          graphRowRequests.map((rowRequest) => rowRequest.toJson()).toList(),
      'deletedRows': deletedRows
    };
  }
}

class GraphRowRequest {
  String id;
  int levelNode;
  String childId;
  String parentId;
  String creator;
  List<GraphNodeRequest> nodeRequests;
  Map<String, List<String>> nodePrerequisites;

  GraphRowRequest(
      {required this.id,
      required this.levelNode,
      required this.childId,
      required this.parentId,
      required this.creator,
      required this.nodeRequests,
      required this.nodePrerequisites});

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'levelNode': levelNode,
      'childId': childId,
      'parentId': parentId,
      'creator': creator,
      'nodePrerequisites': nodePrerequisites,
      'nodeRequests':
          nodeRequests.map((nodeRequest) => nodeRequest.toJson()).toList()
    };
  }
}

class GraphNodeRequest {
  String? nodeId;
  String position;
  String? topicId;
  List<String>? prerequisites;

  GraphNodeRequest(
      {this.nodeId, required this.position, this.topicId, this.prerequisites});

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'nodeId': nodeId,
      'position': position,
      'topicId': topicId,
      'prerequisites': prerequisites
    };
  }
}
