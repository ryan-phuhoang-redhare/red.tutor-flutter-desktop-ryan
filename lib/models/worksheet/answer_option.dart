import 'package:flutter/widgets.dart';
import 'package:redtutor_flutter_webapp/utils/extentions.dart';

class AnswerOption {
  bool isRightAnswer = false;
  int? quizIndex;
  int index;
  String answerDescription = '';
  TextEditingController textController = TextEditingController(text: '');
  Text characterAnswerText = const Text('Remaining characters: 100/100');
  AnswerOption(
      {this.quizIndex,
      required this.index,
      required this.answerDescription,
      required this.isRightAnswer});

  // Create a factory constructor to create objects from a Map.
  factory AnswerOption.fromJson(Map<String, dynamic> jsonData) {
    int index = int.tryParse(jsonData['index'].toString()).orZero();
    String description = jsonData['description'].toString().orEmpty();
    bool isChecked =
        // ignore: sdk_version_since
        bool.tryParse(jsonData['isChecked'].toString()).orFalse();
    try {
      return AnswerOption(
        index: index,
        answerDescription: description,
        isRightAnswer: isChecked,
      );
    } catch (e) {
      return AnswerOption(
        index: -1,
        answerDescription: '',
        isRightAnswer: false,
      );
    }
  }
}
