import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:redtutor_flutter_webapp/models/worksheet/answer_option.dart';
import 'package:redtutor_flutter_webapp/utils/extentions.dart';

class Quiz {
  String? id;
  int index;
  String questionDescription;
  List<dynamic> options;
  String explanation = '';
  String hint = '';
  String type = "MULTIPLE_CHOICE";
  bool isShuffleAnswer = false;
  bool showHintQuestionText = true;
  dynamic rightAnswer;

  Text characterQuestionDescriptionText =
      const Text('Remaining characters: 100/100');
  Text characterExplanationText = const Text('Remaining characters: 100/100');
  Text characterHintText = const Text('Remaining characters: 100/100');
  TextEditingController hintController = TextEditingController(text: '');
  TextEditingController explanationController = TextEditingController(text: '');
  QuillController quillEditorController = QuillController.basic();

  Quiz(
      {required this.index,
      required this.questionDescription,
      required this.options,
      required this.rightAnswer,
      required this.hint,
      required this.explanation,
      required this.type,
      required this.isShuffleAnswer,
      this.id});

  // Create a factory constructor to create objects from a Map.
  factory Quiz.fromJson(Map<String, dynamic> jsonData) {
    String id = jsonData['id'].toString().orEmpty();
    String questionDescription = jsonData['question'].toString().orEmpty();
    int index = int.tryParse(jsonData['index'].toString()).orZero();
    String hint = jsonData['hint'].toString().orEmpty();
    String explanation = jsonData['explanation'].toString().orEmpty();
    String type = jsonData['type'].toString().orEmpty();
    bool isShuffleAnswer =
        // ignore: sdk_version_since
        bool.tryParse(jsonData['shuffleAnswer'].toString()).orFalse();
    dynamic rightAnswer = jsonData['rightAnswer'].toString();

    List<dynamic> optionsRetrieved = jsonData['options'];
    List<AnswerOption> options = [];
    optionsRetrieved.forEach((element) {
      AnswerOption answerOption = AnswerOption.fromJson(element);
      if (answerOption.answerDescription != "") {
        answerOption.textController.text = answerOption.answerDescription;
        final currentLength = answerOption.answerDescription.length;
        final remaining = 100 - currentLength;
        answerOption.characterAnswerText = Text(
          'Remaining characters: ${remaining}/100',
          // ignore: prefer_is_empty
          style: TextStyle(color: remaining < 0 ? Colors.red : Colors.grey),
        );
      }
      options.add(answerOption);
    });
    try {
      Quiz quizRes = Quiz(
          id: id,
          index: index,
          questionDescription: questionDescription,
          options: options,
          rightAnswer: rightAnswer,
          hint: hint,
          explanation: explanation,
          type: type,
          isShuffleAnswer: isShuffleAnswer);
      return quizRes;
    } catch (e) {
      return Quiz(
          id: '',
          index: -1,
          questionDescription: '',
          hint: '',
          explanation: '',
          type: 'MULTIPLE_CHOICE',
          isShuffleAnswer: false,
          options: [
            AnswerOption(index: 0, answerDescription: '', isRightAnswer: false)
          ],
          rightAnswer: -1);
    }
  }
}
