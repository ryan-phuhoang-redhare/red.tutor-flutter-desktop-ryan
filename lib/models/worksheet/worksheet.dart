import 'package:redtutor_flutter_webapp/utils/extentions.dart';

class Worksheet {
  String? id;
  String title;
  bool isShuffleEnabled;
  int createdDate;
  List<String> quizzes;
  int lastEdited;
  String status;

  Worksheet(
      {this.id,
      required this.title,
      required this.isShuffleEnabled,
      required this.createdDate,
      required this.quizzes,
      required this.lastEdited,
      required this.status});

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'isShuffleEnabled': isShuffleEnabled,
      'createdDate': createdDate,
      'quizzes': quizzes,
      'lastEdited': lastEdited,
      'status': status
    };
  }

  // Create a factory constructor to create objects from a Map.
  factory Worksheet.fromJson(Map<String, dynamic> jsonData) {
    String id = jsonData['id'].toString().orEmpty();
    String title = jsonData['title'].toString().orEmpty();
    String status = jsonData['status'].toString().orEmpty();
    bool isShuffleEnabled =
        // ignore: sdk_version_since
        bool.tryParse(jsonData['isShuffleEnabled'].toString()).orFalse();
    int createdDate = int.tryParse(jsonData['createdDate'].toString()).orZero();
    int lastEdited = int.tryParse(jsonData['lastEdited'].toString()).orZero();
    List<String>? quizzes = [];
    if (jsonData['quizzes'] != null) {
      List<dynamic> jsonList = jsonData['quizzes'];
      for (var element in jsonList) {
        quizzes.add(element.toString());
      }
    }
    try {
      return Worksheet(
          id: id,
          title: title,
          isShuffleEnabled: isShuffleEnabled,
          createdDate: createdDate,
          quizzes: quizzes,
          lastEdited: lastEdited,
          status: status);
    } catch (e) {
      return Worksheet(
          id: '',
          title: '',
          isShuffleEnabled: false,
          createdDate: 0,
          quizzes: [],
          lastEdited: 0,
          status: "");
    }
  }
}
