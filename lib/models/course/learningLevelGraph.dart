import 'package:redtutor_flutter_webapp/models/course/graphRow.dart';

class LearningLevelGraph {
  String? id;
  List<GraphRow> rows;

  LearningLevelGraph({this.id, required this.rows});

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {'rows': rows.map((row) => row.toJson()).toList()};
  }

  // Create a factory constructor to create objects from a Map.
  factory LearningLevelGraph.fromJson(Map<String, dynamic> jsonData) {
    String graphId = jsonData["graphId"].toString();
    List<GraphRow> rows = [];
    List<dynamic> rowsJson = jsonData["rows"];
    rowsJson.forEach((element) {
      GraphRow row = GraphRow.fromJson(element);
      rows.add(row);
    });
    try {
      return LearningLevelGraph(id: graphId, rows: rows);
    } catch (e) {
      return LearningLevelGraph(rows: []);
    }
  }
}
