class PrerequisiteItem {
  String nodePosition;
  String text;
  bool isChecked;
  PrerequisiteItem(
      {required this.nodePosition,
      required this.text,
      required this.isChecked});
}
