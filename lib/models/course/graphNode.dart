import 'package:redtutor_flutter_webapp/utils/extentions.dart';

class GraphNode {
  String id;
  String position;
  String? topicId;
  String? topicTitle;
  dynamic topicImage;
  List<String> prerequisites;

  GraphNode({
    required this.id,
    required this.position,
    this.topicTitle,
    this.topicId,
    dynamic topicImage,
    required this.prerequisites,
  });

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'position': position,
      'topicId': topicId,
      'topicTitle': topicTitle,
      'topicImage': topicImage,
      'prerequisites': prerequisites,
    };
  }

  // Create a factory constructor to create objects from a Map.
  factory GraphNode.fromJson(Map<String, dynamic> jsonData) {
    String id = jsonData['nodeId'].toString().orEmpty();
    String position = jsonData['position'].toString().orEmpty();
    String topicId = jsonData['topicId'].toString().orEmpty();
    String topicTitle = jsonData['topicTitle'].toString().orEmpty();
    dynamic topicImage = jsonData['topicImage'].toString().orEmpty();
    List<String>? prerequisites = [];
    if (jsonData['prerequisites'] != null) {
      List<dynamic> jsonList = jsonData['prerequisites'];
      for (var element in jsonList) {
        prerequisites.add(element.toString());
      }
    }

    return GraphNode(
        id: id,
        position: position,
        topicId: topicId,
        topicTitle: topicTitle,
        topicImage: topicImage,
        prerequisites: prerequisites);
  }
}
