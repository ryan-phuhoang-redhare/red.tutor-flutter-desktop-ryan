import 'package:redtutor_flutter_webapp/models/course/graphNode.dart';
import 'package:redtutor_flutter_webapp/utils/extentions.dart';

class GraphRow {
  String id;
  int levelNode;
  String childId;
  String parentId;
  List<GraphNode> nodes;

  GraphRow(
      {required this.id,
      required this.levelNode,
      required this.childId,
      required this.parentId,
      required this.nodes});

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'levelNode': levelNode,
      'childId': childId,
      'parentId': parentId,
      'nodes': nodes
    };
  }

  // Create a factory constructor to create objects from a Map.
  factory GraphRow.fromJson(Map<String, dynamic> jsonData) {
    String id = jsonData['rowId'].toString().orEmpty();
    int levelNode = int.tryParse(jsonData['levelNode'].toString()).orZero();
    String childId = jsonData['childId'].toString().orEmpty();
    String parentId = jsonData['parentId'].toString().orEmpty();
    List<GraphNode> nodes = [];
    List<dynamic> nodesJson = jsonData['nodes'];
    nodes = nodesJson.map((json) => GraphNode.fromJson(json)).toList();
    return GraphRow(
        id: id,
        levelNode: levelNode,
        childId: childId,
        parentId: parentId,
        nodes: nodes);
  }
}
