import 'package:redtutor_flutter_webapp/models/mediaRecourse.dart';
import 'package:redtutor_flutter_webapp/utils/extentions.dart';

class Course {
  String id;
  String name;
  double unlockCost;
  String subjectLanguage;
  List<String> instructionLanguage;
  String difficulty;
  String theme;
  String description;
  int ratingsCount;
  double averageRating;
  bool isPremium;
  String? imageBase64Data;
  MediaResource mediaResource;
  String creatorId;
  int createdAt;
  String status;
  int lastEdited;

  Course(
      {required this.id,
      required this.name,
      required this.unlockCost,
      required this.subjectLanguage,
      required this.instructionLanguage,
      required this.difficulty,
      required this.theme,
      required this.description,
      required this.ratingsCount,
      required this.averageRating,
      required this.isPremium,
      required this.mediaResource,
      this.imageBase64Data,
      required this.creatorId,
      required this.createdAt,
      required this.status,
      required this.lastEdited});
  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'isPremium': isPremium,
      'unlockCost': unlockCost,
      'subjectLanguage': subjectLanguage,
      'instructionLanguage': instructionLanguage,
      'difficulty': difficulty,
      'theme': theme,
      'description': description,
      'ratingsCount': ratingsCount,
      'averageRating': averageRating,
      'imageBase64Data': imageBase64Data,
      'mediaResource': mediaResource,
      'creatorId': creatorId,
      'createdAt': createdAt
    };
  }

  // Create a factory constructor to create objects from a Map.
  factory Course.fromJson(Map<String, dynamic> jsonData) {
    Map<String, dynamic> mediaResourceJson = jsonData['mediaResource'];
    MediaResource mediaResource = MediaResource.fromJson(mediaResourceJson);
    String id = jsonData['id'].toString().orEmpty();
    String name = jsonData['name'].toString().orEmpty();
    String status = jsonData['status'].toString().orEmpty();

    double unlockCost =
        double.tryParse(jsonData['unlockCost'].toString()).orZero();
    String subjectLanguage = jsonData['subjectLanguage'].toString().orEmpty();
    List<String>? instructionLanguage = [];
    if (jsonData['instructionLanguage'] != null) {
      List<dynamic> jsonList = jsonData['instructionLanguage'];
      for (var element in jsonList) {
        instructionLanguage.add(element.toString());
      }
    }

    String difficulty = jsonData['difficulty'].toString().orEmpty();
    String theme = jsonData['theme'].toString().orEmpty();
    String description = jsonData['description'].toString().orEmpty();
    int ratingsCount =
        int.tryParse(jsonData['ratingsCount'].toString()).orZero();
    int lastEdited = int.tryParse(jsonData['lastEdited'].toString()).orZero();
    double averageRating =
        double.tryParse(jsonData['averageRating'].toString()).orZero();

    bool premium =
        // ignore: sdk_version_since
        bool.tryParse(jsonData['premium'].toString()).orFalse();
    String creatorId = jsonData['creatorId'].toString().orEmpty();
    int createdAt = int.tryParse(jsonData['createdAt'].toString()).orZero();
    try {
      return Course(
          id: id,
          name: name,
          unlockCost: unlockCost,
          subjectLanguage: subjectLanguage,
          instructionLanguage: instructionLanguage,
          difficulty: difficulty,
          theme: theme,
          description: description,
          ratingsCount: ratingsCount,
          averageRating: averageRating,
          isPremium: premium,
          mediaResource: mediaResource,
          creatorId: creatorId,
          status: status,
          lastEdited: lastEdited,
          createdAt: createdAt);
    } catch (e) {
      return Course(
          id: '',
          name: '',
          unlockCost: 0,
          subjectLanguage: '',
          instructionLanguage: [],
          difficulty: '',
          theme: '',
          description: '',
          ratingsCount: 0,
          averageRating: 0,
          isPremium: false,
          mediaResource: MediaResource(id: id, mediaType: '', url: ''),
          creatorId: '',
          status: '',
          lastEdited: 0,
          createdAt: 0);
    }
  }
}
