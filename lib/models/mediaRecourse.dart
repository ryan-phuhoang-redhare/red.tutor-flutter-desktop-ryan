import 'package:redtutor_flutter_webapp/utils/extentions.dart';

class MediaResource {
  String id;
  String mediaType;
  String url;

  MediaResource({
    required this.id,
    required this.mediaType,
    required this.url,
  });

  // Convert the object to a Map that can be easily converted to JSON.
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'mediaType': mediaType,
      'url': url,
    };
  }

  // Create a factory constructor to create objects from a Map.
  factory MediaResource.fromJson(Map<String, dynamic> jsonData) {
    String id = jsonData['id'].toString().orEmpty();
    String mediaType = jsonData['mediaType'].toString().orEmpty();
    String url = jsonData['url'].toString().orEmpty();

    return MediaResource(id: id, mediaType: mediaType, url: url);
  }
}
