// extensions on String
extension NonNullString on String? {
  String orEmpty() {
    if (this == "null") return "";
    return this ?? "";
  }
}

// extensions on Int
extension NonNullInteger on int? {
  int orZero() {
    return this ?? 0;
  }
}

// extensions on Double
extension NonNullDouble on double? {
  double orZero() {
    return this ?? 0.0;
  }
}

extension NonNullList<T> on List<T>? {
  List<T> orEmpty() {
    return this ?? [];
  }
}

extension NonNullBool on bool? {
  bool orFalse() {
    return this ?? false;
  }
}
