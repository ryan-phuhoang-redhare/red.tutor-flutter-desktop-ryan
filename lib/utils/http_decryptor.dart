import 'dart:convert';
import 'dart:developer';

import 'package:encrypt/encrypt.dart' as crypto;

class HttpDecryptor {
  late crypto.Key _cryptKey;
  late crypto.IV _iv;
  late crypto.Encrypter _encrypter;

  HttpDecryptor() {
    _cryptKey = crypto.Key.fromUtf8("RhsRhs01RhsRhs01");
    _iv = crypto.IV.fromLength(0);
    _encrypter = crypto.Encrypter(
      crypto.AES(
        _cryptKey,
        mode: crypto.AESMode.ecb,
        padding: null,
      ),
    );
  }

  // => I don't know why after decrypting, the result is
  // in this format: {wanted_result}\u0003\u0003\u0003
  // or this format: {wanted_result}\x0F\x0F\x0F\x0F
  // Hence, i have to resort to replaceAll whenever the end of string is not '}'
  String _clean(String data) {
    final lastChar = data[data.length - 1];
    if (lastChar == "}") return data;
    return data.replaceAll(lastChar, "");
  }

  /// Decrypt the data from remote APIs
  ///
  /// Throws [DecryptException] on error codes
  T decrypt<T>(String data) {
    try {
      // Add padding to the input string if needed
      final paddingLength = data.length % 4;
      final paddedData =
          paddingLength > 0 ? data + ('=' * (4 - paddingLength)) : data;
      // decrypt
      final decrypted = _encrypter.decrypt64(paddedData, iv: _iv);
      final cleanedData = _clean(decrypted);

      return (jsonDecode(cleanedData))["payload"];
    } catch (e) {
      log("[DECRYPT ERROR] ${e.toString()}");
      throw Error();
    }
  }
}
